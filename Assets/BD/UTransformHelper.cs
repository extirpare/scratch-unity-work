﻿using UnityEngine;
using System;
using System.Linq;

// This holds friend functions (or whatever the term is for ReturnType MyFunc(this ClassType) functions)
// for Transforms, GameObjects, and Components.

namespace BD
{
	public static class UTransformHelper
	{
		public static Transform BDFindChild(this Transform transform, string name) { return transform.BDFindChild(name, false); }
		public static Transform BDFindChild(this Transform transform, string name, bool recurse) { return transform.BDGetChildByName(name, recurse); }
		public static Transform BDGetChildByName(this Transform transform, string name) { return transform.BDGetChildByName(name, false); }
		public static Transform BDGetChildByName(this Transform transform, string name, bool recurse)
		{
			Transform childTransform = transform.Find(name);
			if (childTransform != null) return childTransform;

			if (recurse)
			{
				foreach (Transform child in transform)
				{
					Transform foundTransform = child.BDGetChildByName(name, true);
					if (foundTransform != null) return foundTransform;
				}
			}

			return null;
		}

		public static Transform BDGetOrCreateChildByName(this Transform transform, string name) { return transform.BDGetOrCreateChildByName(name, false); }
		public static Transform BDGetOrCreateChildByName(this Transform transform, string name, bool recurse)
		{
			Transform ret = transform.BDGetChildByName(name, recurse);
			if (ret == null)
			{
				GameObject newObject = new GameObject(name);
				newObject.transform.parent = transform;
				return newObject.transform;
			}
			return ret;
		}

		public static void BDDeleteAllChildren(this Transform transform)
		{
			transform.BDDeleteAllChildren(true);
		}

		public static void BDDeleteAllChildren(this Transform transform, bool immediate)
		{
			if (immediate)
				for (int i = transform.childCount - 1; i >= 0; --i)
					MonoBehaviour.DestroyImmediate(transform.GetChild(i).gameObject);
			else
				for (int i = transform.childCount - 1; i >= 0; --i)
					MonoBehaviour.Destroy(transform.GetChild(i).gameObject);
		}

		public static void BDDeleteAllChildrenWhere(this Transform transform, Func<GameObject, bool> functor)
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				if (functor(transform.GetChild(i).gameObject) == true)
				MonoBehaviour.DestroyImmediate(transform.GetChild(i).gameObject);
			}
		}

		public static void BDShowAllChildren(this Transform transform) { transform.BDSetActiveAllChildren(true, false); }
		public static void BDHideAllChildren(this Transform transform) { transform.BDSetActiveAllChildren(false, false); }
		public static void BDSetActiveAllChildren(this Transform transform, bool active) { transform.BDSetActiveAllChildren(active, false); }
		public static void BDSetActiveAllChildren(this Transform transform, bool active, bool recurse)
		{
			for (int i = 0; i < transform.childCount; ++i)
			{
				GameObject obj = transform.GetChild(i).gameObject;
				if (obj.activeSelf != active)
					transform.GetChild(i).gameObject.SetActive(active);
				if (recurse)
					transform.GetChild(i).BDSetActiveAllChildren(active, true);
			}
		}
		

		public static T GetOrAddComponent<T>(this GameObject gameObject)
			where T : Component
		{
			T existingComponent = gameObject.GetComponent<T>();
			if (existingComponent == null)
				return gameObject.AddComponent<T>();
			return existingComponent;
		}

		public static bool BDHasComponent<T>(this GameObject gameObject)
		{
			return gameObject.GetComponent<T>() != null;
		}

		public static bool BDIsDestroyed(this Component component)
		{
             // UnityEngine overloads the == opeator for the GameObject type to return null when the object has been destroyed.
			 // However, the object still exists, just hasn't been cleaned up yet.
             return component == null && !ReferenceEquals(component, null);
		}
		
		public static bool BDHasInterface<T>(this GameObject gameObject) where T : class { return gameObject.BDGetInterface<T>() != null; }
		public static T BDGetInterface<T>(this GameObject gameObject)
			where T : class
		{
			if (!typeof(T).IsInterface)
			{
				Debug.LogError(typeof(T).ToString() + ": is not an actual interface!");
				return null;
			}
			return gameObject.GetComponents<T>().OfType<T>().FirstOrDefault();
		}

		public static T BDFindChildWithComponent<T>(this GameObject gameObject, string name) where T : Component { return gameObject.BDFindChildWithComponent<T>(name, false); }
		public static T BDFindChildWithComponent<T>(this GameObject gameObject, string name, bool recurse)
			where T : Component
		{
			//is it worth making this look for more children with the same name if the first one doesn't have the component you want?
			Transform namedChild = gameObject.transform.BDFindChild(name, recurse);
			if (namedChild != null) return namedChild.GetComponent<T>();
			return null;
		}

		public static T BDGetOrCreateComponent<T>(this GameObject gameObject)
			where T : Component
		{
			if (gameObject.GetComponent<T>() != null)
				return gameObject.GetComponent<T>();

			T component = gameObject.AddComponent<T>();
			return component;
		}

		public static T BDGetOrCreateChildWithComponent<T>(this GameObject gameObject, string name)
			where T : Component
		{
			Transform namedChildTransform = gameObject.transform.BDFindChild(name);
			GameObject namedChild = (namedChildTransform != null) ? namedChildTransform.gameObject : new GameObject(name);


			namedChild.transform.parent = gameObject.transform;
			if (namedChild.GetComponent<T>() != null)
				return namedChild.GetComponent<T>();

			T component = namedChild.AddComponent<T>();
			return component;
		}

		public static T BDGetComponentInSelfOrChildren<T>(this GameObject gameObject) where T : Component { return gameObject.BDGetComponentInSelfOrChildren<T>(false); }
		public static T BDGetComponentInSelfOrChildren<T>(this GameObject gameObject, bool recurse)
			where T : Component
		{
			T selfComponent = gameObject.GetComponent<T>();
			if (selfComponent != null) return selfComponent;
			T childComponent = gameObject.GetComponentInChildren<T>();
			if (childComponent != null) return childComponent;
			return null;
		}
	}
}