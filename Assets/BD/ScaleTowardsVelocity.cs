﻿using BD;
using UnityEngine;
 
[ExecuteInEditMode]
public class ScaleTowardsVelocity : MonoBehaviour
{
	public eAxis LookAxis;
	public eAxis ScaleAxis;
	public float ScaleStrength = 1f;
	BD.Vector3 m_prevPosition;
	
 
	void Start()
	{
		m_prevPosition = transform.position;
	}

	void LateUpdate()
	{
		BD.Vector3 delta = (BD.Vector3)transform.position - m_prevPosition;
		if (delta.Length() > BD.Maths.Epsilon)
			transform.localRotation = Quaternion.LookRotation(BD.Vector3.AlongAxis(LookAxis), delta);

		float onAxis = 1 + (delta.Length() * ScaleStrength);
		float offAxis = Mathf.Sqrt(1 / onAxis); //preserves volume
		foreach (eAxis axis in EnumHelper.GetValuesIn<eAxis>())
			transform.localScale = new BD.Vector3(transform.localScale).SetAxis(axis, axis == ScaleAxis ? onAxis : offAxis);
 
		m_prevPosition = transform.position;
	}
}