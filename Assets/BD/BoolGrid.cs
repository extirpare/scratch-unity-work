﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace BD
{
	//These get a much nicer editor display
	[Serializable]
	public class BoolGrid : Grid2D<bool>, ISerializationCallbackReceiver
	{
		[SerializeField] int m_serializedWidth = 1;
		[SerializeField] int m_serializedHeight = 1;
		[SerializeField] List<bool> m_serializedVals = new List<bool>();
		public BoolGrid() : base() { }
		public BoolGrid(int width, int height) : base(width, height) { }
		public BoolGrid(IntCoord dims) : base(dims) { }

		public BoolGrid(IntCoord trueLoc, IntCoord dims) : base(dims) { this[trueLoc] = true; }
		public BoolGrid(List<IntCoord> trueLocs, IntCoord dims) : base(dims) { SetTrue(trueLocs); }
		public BoolGrid(List<IntCoord> trueLocs, int width, int height) : base(width, height) { SetTrue(trueLocs); }

		public void OnBeforeSerialize()
		{
			m_serializedWidth = this.Dimensions.X;
			m_serializedHeight = this.Dimensions.Y;

			m_serializedVals.Clear();
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
				m_serializedVals.Add(this[iter]); 
		}

		public void OnAfterDeserialize()
		{
			SetDims(new IntCoord(m_serializedWidth, m_serializedHeight)); 

			int i = 0;
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
			{
				this[iter] = i >= m_serializedVals.Count ? false : m_serializedVals[i];
				++i;
			}
		}

		public void SetTrue(List<IntCoord> trueLocs)
		{
			foreach (IntCoord trueLoc in trueLocs)
				if (trueLoc.FitsWithin(Dimensions))
					this[trueLoc] = true;
		}

		public List<IntCoord> TrueLocs()
		{
			List<IntCoord> ret = new List<IntCoord>();
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
				if (this[iter])
					ret.Add(iter);

			return ret;
		}

		public void ApplyOr(BoolGrid other)
		{
			Debug.Assert(this.Dimensions == other.Dimensions);
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
				this[iter] = this[iter] || other[iter];
		}

		public void ApplyAnd(BoolGrid other)
		{
			Debug.Assert(this.Dimensions == other.Dimensions);
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
				this[iter] = this[iter] && other[iter];
		}

		public void ActOnTrueCoords(Action<IntCoord> action)
		{
			ActOnIndicesWhere((loc, val) => val == true, (loc, val) => action(loc));
		}
	}
}
