﻿using System.Collections.Generic;

namespace BD
{
	public static class Algo
	{
		public static T TheOneThatIsnt<T>(T left, T right, T val)
			where T : class
		{
			return object.ReferenceEquals(left, val) ? right : left;
		}

		public static void Swap<T>(ref T left, ref T right)
		{
			T tmp = left;
			left = right;
			right = tmp;
		}

		//note bitIndex == the power of 2 that bit represents
		public static bool IsBitSet(uint val, int bitIndex)
		{
			return (val & 1 << bitIndex) != 0;
		}

		public static uint SetBit(uint val, int bitIndex, bool bitSetting)
		{
			uint ret;
			if (bitSetting)
				ret = val | (uint)1 << bitIndex;
			else
				ret = val & ~((uint)1 << bitIndex);
			return ret;
		}

		public static int CountOneBits(uint val)
		{
			int count = 0;
			for (int i = 0; i < 32; ++i)
				if (IsBitSet(val, i)) ++count;
			return count;
		}

		public static bool InBounds(int val, int lengthOfArray)
		{
			return val >= 0 && val < lengthOfArray;
		}

		public static T[] ResizeAndCopyArray<T>(T[] baseArr, int newLen)
		{
			T[] ret = new T[newLen];
			for (int i = 0; i < Maths.Min(baseArr.Length, newLen); ++i)
				ret[i] = baseArr[i];

			return ret;
		}

		public static T FindOnEnumerator<T>(System.Collections.Generic.IEnumerator<T> iter, System.Func<T, bool> checkFunc)
		{
			while (iter.MoveNext())
				if (checkFunc(iter.Current))
					return iter.Current;
					
			return default(T);
		}
	}
}
