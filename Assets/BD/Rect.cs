﻿using System;
using System.Xml;

//
// A 2D axis-aligned box.
//

namespace BD
{
	[Xmlable("Rect")]
	[Serializable] //Unity might use this to serialize but mostly we use it to expose to inspector
	public struct Rect : IXmlable
	{
		public float X;
		public float Y;
		public float Width;
		public float Height;

		public float Left { get { return X; } set { X = value; } }
		public float Right { get { return X + Width; } set { Width = value - X; } }
		public float Top { get { return Y; } set { Y = value; } }
		public float Bottom { get { return Y + Height; } set { Height = value - Y; } }
		public float Length { get { return Width; } set { Width = value; } }

		public Vector2 XY { get { return TopLeft; } set { TopLeft = value; } }
		public Vector2 WidthHeight { get { return Size; } set { Size = value; } }

		public Vector2 TopLeft { get { return new Vector2(Left, Top); } set { Left = value.X; Top = value.Y; } }
		public Vector2 TopRight { get { return new Vector2(Right, Top); } set { Right = value.X; Top = value.Y; } }
		public Vector2 BottomLeft { get { return new Vector2(Left, Bottom); } set { Left = value.X; Bottom = value.Y; } }
		public Vector2 BottomRight { get { return new Vector2(Right, Bottom); } set { Right = value.X; Bottom = value.Y; } }
		public Vector2 Size { get { return new Vector2(Width, Height); } set { Width = value.X; Height = value.Y; } }
		public Vector2 Center { get { return new Vector2(X + Width / 2, Y + Height / 2); } }

		public Rect(Rect other) { X = other.X; Y = other.Y; Width = other.Width; Height = other.Height; }
		public Rect(float width, float height)
		{
			this.X = 0;
			this.Y = 0;
			this.Width = width;
			this.Height = height;
		}

		public Rect(float x, float y, float width, float height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		public Rect(Vector2 size)
		{
			this.X = 0;
			this.Y = 0;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public Rect(Vector2 topLeft, Vector2 size)
		{
			this.X = topLeft.X;
			this.Y = topLeft.Y;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public Rect(Vector2 topLeft, float width, float height)
		{
			this.X = topLeft.X;
			this.Y = topLeft.Y;
			this.Width = width;
			this.Height = height;
		}

		public Rect(float x, float y, Vector2 size)
		{
			this.X = x;
			this.Y = y;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public static Rect Zero { get { return new Rect(0, 0, 0, 0); } }
		public static Rect None { get { return new Rect(Vector2.Zero, Vector2.None); } }
		public static Rect ZeroToOne { get { return new Rect(Vector2.Zero, Vector2.One); } }

		public static Rect CenterAndRadius(BD.Vector2 center, float radius) { return new Rect(center - new Vector2(radius), new Vector2(2 * radius)); }

		public static implicit operator Rect(UnityEngine.Rect r) { return new Rect(r.x, r.y, r.width, r.height); }
		public static implicit operator UnityEngine.Rect(Rect r) { return new UnityEngine.Rect(r.X, r.Y, r.Width, r.Height); } 


		public static Rect operator +(Rect rect, DirDims dirDims) { return rect.Expand(dirDims); }
		public static Rect operator -(Rect rect, DirDims dirDims) { return rect.Contract(dirDims); }

		public static bool operator ==(Rect lhs, Rect rhs)
		{
			return Maths.NearEqual(lhs.X, rhs.X)
				&& Maths.NearEqual(lhs.Y, rhs.Y)
				&& Maths.NearEqual(lhs.Width, rhs.Width)
				&& Maths.NearEqual(lhs.Height, rhs.Height);
		}
		public static bool operator !=(Rect lhs, Rect rhs) { return !(lhs == rhs); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString() { return "[" + Left + ", " + Top + "] size: [" + Width + ", " + Height + "]"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y).BDWriteAttr("Width", Width).BDWriteAttr("Height", Height); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref X).BDReadAttr("Y", ref Y).BDReadAttr("Width", ref Width).BDReadAttr("Height", ref Height); }

		public Line2 LeftLine() { return new Line2(TopLeft, BottomLeft); }
		public Line2 TopLine() { return new Line2(TopLeft, TopRight); }
		public Line2 RightLine() { return new Line2(TopRight, BottomRight); }
		public Line2 BottomLine() { return new Line2(BottomLeft, BottomRight); }

		public Rect AtX(float x)
		{
			Rect ret = new Rect(this);
			ret.X = x;
			return ret;
		}

		public Rect AtY(float y)
		{
			Rect ret = new Rect(this);
			ret.Y = y;
			return ret;
		}

		public Rect AtWidth(float width)
		{
			Rect ret = new Rect(this);
			ret.Width = width;
			return ret;
		}

		public Rect AtHeight(float height)
		{
			Rect ret = new Rect(this);
			ret.Height = height;
			return ret;
		}

		public Rect SetTopLeft(Vector2 topLeft) { return this.AtX(topLeft.X).AtY(topLeft.Y); }
		public Rect SetSize(Vector2 size) { return this.AtWidth(size.X).AtHeight(size.Y); }
		public Rect SetBottomRight(Vector2 botRight) { return this.SetSize(botRight - this.TopLeft); }

		//If you want to cut off the first and last quarter of a rect, use HorizontalSlice(0.25, 0.75)
		public Rect HorizontalSlice(float startRatio, float endRatio)
		{
			return this.AtWidth(this.Width * (endRatio - startRatio)).ScrollRight(this.Width * startRatio);
		}
		public Rect VerticalSlice(float startRatio, float endRatio)
		{
			return this.AtHeight(this.Height * (endRatio - startRatio)).ScrollDown(this.Height * startRatio);
		}

		public Rect AddX(float x) { return AtX(this.X + x); }
		public Rect AddY(float y) { return AtY(this.Y + y); }
		public Rect AddWidth(float width) { return AtWidth(this.Width + width); }
		public Rect AddHeight(float height) { return AtHeight(this.Height + height); }

		public Rect ToOrigin() { return new Rect(Size); }

		public bool Contains(Vector2 point) { return Contains(point, true); }
		public bool Contains(Vector2 point, bool acceptEdges)
		{
			if (acceptEdges)
			{
				return point.X >= Left
					&& point.Y >= Top
					&& point.X <= Right
					&& point.Y <= Bottom;
			}
			else
			{
				return point.X > Left
					&& point.Y > Top
					&& point.X < Right
					&& point.Y < Bottom;
			}
		}

		public bool ContainsWithinMargin(Vector2 point, DirDims margin)
		{
			Rect shrunk = this.Contract(margin);
			return shrunk.Contains(point);
		}

		//"Contains" a rect == rect is entirely inside this
		public bool Contains(Rect rect) { return Contains(rect, true); }
		public bool Contains(Rect rect, bool acceptEdges)
		{
			if (acceptEdges)
			{
				return rect.Left >= Left
					&& rect.Top >= Top
					&& rect.Right <= Right
					&& rect.Bottom <= Bottom;
			}
			else
			{
				return rect.Left > Left
					&& rect.Top > Top
					&& rect.Right < Right
					&& rect.Bottom < Bottom;
			}
		}

		//"Overlaps" a rect == any part of rect is inside this
		public bool Overlaps(Rect other) { return Overlaps(other, true); }
		public bool Overlaps(Rect other, bool acceptEdges)
		{
			if (acceptEdges)
			{
				if (other.Left > this.Right
					|| other.Top > this.Bottom
					|| other.Right < this.Left
					|| other.Bottom < this.Top)
				{
					return false;
				}
				return true;
			}
			else
			{
				if (other.Left >= this.Right
					|| other.Top >= this.Bottom
					|| other.Right <= this.Left
					|| other.Bottom <= this.Top)
				{
					return false;
				}
				return true;
			}
		}

		public Rect Contract(float amt) { return this.ContractHoriz(amt).ContractVert(amt); }
		public Rect ContractHoriz(float amt)
		{
			Rect ret = new Rect(this);
			ret.X += amt;
			ret.Width -= 2*amt;

			return ret;
		}

		public Rect ContractVert(float amt)
		{
			Rect ret = new Rect(this);
			ret.Y += amt;
			ret.Height -= 2*amt;

			return ret;
		}

		public Rect Contract(DirDims dirDims)
		{
			Rect ret = new Rect(this);
			ret.X += dirDims.Left;
			ret.Y += dirDims.Top;
			ret.Width -= dirDims.Width;
			ret.Height -= dirDims.Height;

			return ret;
		}

		public Rect Expand(float amt) { return ExpandHoriz(amt).ExpandVert(amt); }
		public Rect Expand(DirDims amt) { return Contract(-amt); }
		public Rect ExpandHoriz(float amt) { return ContractHoriz(-amt); }
		public Rect ExpandVert(float amt) { return ContractVert(-amt); }

		public Rect AtMinHeight(float minHeight)
		{
			Rect ret = new Rect(this);
			ret.Height = Math.Max(Height, minHeight);
			return ret;
		}

		public Rect AtMaxHeight(float maxHeight)
		{
			Rect ret = new Rect(this);
			ret.Height = Math.Min(Height, maxHeight);
			return ret;
		}

		public Rect AtMinWidth(float minWidth)
		{
			Rect ret = new Rect(this);
			ret.Width = Math.Max(Width, minWidth);
			return ret;
		}

		public Rect AtMaxWidth(float maxWidth)
		{
			Rect ret = new Rect(this);
			ret.Width = Math.Min(Width, maxWidth);
			return ret;
		}

		public Rect AtMinSize(Vector2 size) { return this.AtMinWidth(size.X).AtMinHeight(size.Y); }
		public Rect AtMaxSize(Vector2 size) { return this.AtMaxWidth(size.X).AtMaxHeight(size.Y); }

		//
		// Scroll[Dir] functions preserve width/height
		//
		public Rect ScrollLeft() { return ScrollLeft(Width); }
		public Rect ScrollLeft(float amt) { return ScrollRight(-amt); }
		public Rect ScrollRight() { return ScrollRight(Width); }
		public Rect ScrollRight(float amt)
		{
			Rect ret = new Rect(this);
			ret.X += amt;
			return ret;
		}

		public Rect ScrollUp() { return ScrollUp(Height); }
		public Rect ScrollUp(float amt) { return ScrollDown(-amt); }
		public Rect ScrollDown() { return ScrollDown(Height); }
		public Rect ScrollDown(float amt)
		{
			Rect ret = new Rect(this);
			ret.Y += amt;
			return ret;
		}

		public Rect Scroll(Vector2 amt) { return this.ScrollRight(amt.X).ScrollDown(amt.Y); }

		//
		// Move[Side][Dir] functions don't preserve width/height
		//
		public Rect MoveTopUp(float amt) { return MoveTopDown(-amt); }
		public Rect MoveTopDown(float amt)
		{
			Rect ret = new Rect(this);
			ret.Y += amt;
			ret.Height = Math.Max(0f, ret.Height - amt);
			return ret;
		}

		public Rect MoveLeftLeft(float amt) { return MoveLeftRight(-amt); }
		public Rect MoveLeftRight(float amt)
		{
			Rect ret = new Rect(this);
			ret.X += amt;
			ret.Width = Math.Max(0f, ret.Width - amt);
			return ret;
		}

		//Also allows translation
		public bool IsUniformScaleOf(Rect other)
		{
			Vector2 ratio = this.Size / other.Size;
			return Maths.NearEqual(ratio.X, ratio.Y);
		}

		public Rect AtScale(float scale)
		{
			Rect ret = new Rect(this);
			ret.Width = scale * Width;
			ret.Height = scale * Height;
			return ret;
		}

		//translates this to fit inside bounds, if needed.
		//if this is bigger than bounds, will fit such that top left == bounds.TopLeft
		public Rect TranslateToFitInside(Rect bounds)
		{
			if (bounds.Contains(this))
				return new Rect(this);

			Rect ret = new Rect(this);

			if (ret.Bottom > bounds.Bottom)
				ret = ret.ScrollUp(ret.Bottom - bounds.Bottom);
			if (ret.Right > bounds.Right)
				ret = ret.ScrollLeft(ret.Right - bounds.Right);
			if (ret.Top < bounds.Top)
				ret = ret.ScrollDown(bounds.Top - ret.Top);
			if (ret.Left < bounds.Left)
				ret = ret.ScrollRight(bounds.Left - ret.Left);

			return ret;
		}

		public Rect ChopToFit(Rect fitAgainst)
		{
			Rect ret = new Rect(this);
			ret.Top = Math.Max(Top, fitAgainst.Top);
			ret.Left = Math.Max(Left, fitAgainst.Left);
			ret.Bottom = Math.Min(Bottom, fitAgainst.Bottom);
			ret.Right = Math.Min(Right, fitAgainst.Right);
			return ret;
		}

		enum eLocation
		{
			SmallerNumber,
			InsideBounds,
			BiggerNumber
		}

		public Vector2 ClosestPoint(Vector2 pt)
		{
			eLocation horizLoc, vertLoc;
			if (pt.X < this.Left) horizLoc = eLocation.SmallerNumber; 
			else if (pt.X > this.Right) horizLoc = eLocation.BiggerNumber; 
			else horizLoc = eLocation.InsideBounds;

			if (pt.Y < this.Top) vertLoc = eLocation.SmallerNumber; 
			else if (pt.Y > this.Bottom) vertLoc = eLocation.BiggerNumber; 
			else vertLoc = eLocation.InsideBounds;

			switch (horizLoc)
			{
				case eLocation.SmallerNumber: //to the left
					switch (vertLoc)
					{
						case eLocation.SmallerNumber:
							return this.TopLeft;
						case eLocation.InsideBounds:
							return new Vector2(this.Left, pt.Y);
						case eLocation.BiggerNumber:
							return this.BottomLeft;
					}
					throw new Exception();
				case eLocation.InsideBounds:
					switch (vertLoc)
					{
						case eLocation.SmallerNumber:
							return new Vector2(pt.X, this.Top);
						case eLocation.InsideBounds:
							return pt; //it's inside us lol
						case eLocation.BiggerNumber:
							return new Vector2(pt.X, this.Bottom);
					}
					throw new Exception();
				case eLocation.BiggerNumber: //to the right
					switch (vertLoc)
					{
						case eLocation.SmallerNumber:
							return this.TopRight;
						case eLocation.InsideBounds:
							return new Vector2(this.Right, pt.Y);
						case eLocation.BiggerNumber:
							return this.BottomRight;
					}
					throw new Exception();
				default:
					throw new Exception();
			}
		}

		public Vector2? LineIntersection(Line2 line)
		{
			Line2 right = new Line2(TopRight, BottomRight);
			Line2 up = new Line2(TopLeft, TopRight);
			Line2 left = new Line2(TopLeft, BottomLeft);
			Line2 down = new Line2(BottomLeft, BottomRight);

			if (right.Intersects(line))
				return right.IntersectionPt(line);
			if (up.Intersects(line))
				return up.IntersectionPt(line);
			if (left.Intersects(line))
				return left.IntersectionPt(line);
			if (down.Intersects(line))
				return down.IntersectionPt(line);

			return null;
		}

		//[0,0] is bottom left and [1,1] is top right
		public Vector2 AtNormalizedLoc(float x, float y)
		{
			return AtNormalizedLoc(new BD.Vector2(x, y));
		}

		public Vector2 AtNormalizedLoc(BD.Vector2 loc)
		{
			return new Vector2(Left + loc.X * Width, Bottom - loc.Y * Height);
		}
	}
}

