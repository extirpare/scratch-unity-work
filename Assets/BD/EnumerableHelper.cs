﻿using System;
using System.Collections;

namespace BD
{
	public static class EnumerableHelper
	{
		//returns the first value in enumerable that satisfies predicate, else nullptr
		public static object BDTryAndGet(this IEnumerable enumerable, Func<object, bool> predicate)
		{
			if (enumerable == null || predicate == null)
				throw new ArgumentNullException();

			foreach (object iterObj in enumerable)
				if (predicate(iterObj)) return iterObj;

			return null;
		}
	}
}
