﻿using UnityEngine;

//ImageProcessControls are just to be used in editor. Stick this on your SpriteRenderer GameObject, use them, then delete this.

public class ImageProcessControls : MonoBehaviour
{
	[System.Serializable]
	public struct NameAndCount
	{
		public string Name;
		public int Count;
	}

	public NameAndCount[] NamesAndCounts;
	public string SpritesheetName;

	void Start()
	{
		Debug.Assert(false, "ImageProcessControls shouldn't be on your class during runtime!");
	}
}
