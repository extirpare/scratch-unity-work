﻿using System;
using System.Xml;

using UnityEngine;

namespace BD
{
	//RGB triad, expected values 0-1 float or 0-255 byte
	[Serializable]
	[Xmlable("Color")]
	public struct Color : IXmlable
	{
		public float R;
		public float G;
		public float B;
		public float A;

		public Color(float val) { R = val; G = val; B = val; A = 1; }
		public Color(float val, float alpha) { R = val; G = val; B = val; A = alpha; }
		public Color(float r, float g, float b) { R = r; G = g; B = b; A = 1; }
		public Color(float r, float g, float b, float a) { R = r; G = g; B = b; A = a; }
		public Color(int r, int g, int b) { UnityEngine.Debug.Assert(Maths.ValidByte(r) && Maths.ValidByte(g) && Maths.ValidByte(b)); 
											R = (float)r/255; G = (float)g/255; B = (float)b/255; A = 1; }
		public Color(int r, int g, int b, int a) { UnityEngine.Debug.Assert(Maths.ValidByte(r) && Maths.ValidByte(g) && Maths.ValidByte(b) && Maths.ValidByte(a)); 
											R = (float)r/255; G = (float)g/255; B = (float)b/255;  A = (float)a/255; }
		public Color(UnityEngine.Color c) { R = c.r; G = c.g; B = c.b; A = c.a; }
		public static implicit operator Color(UnityEngine.Color c) { return new Color(c); }
		public static implicit operator UnityEngine.Color(Color c) { return new UnityEngine.Color(c.R, c.G, c.B, c.A); }

		public static Color operator *(Color a, float val) { return new Color(a.R * val, a.G * val, a.B * val, a.A); }
		public static Color operator *(float val, Color a) { return a * val; }

		public static bool operator !=(Color lhs, Color rhs) { return !(lhs == rhs); }
		public static bool operator ==(Color lhs, Color rhs) { return Maths.NearEqual(lhs.R, rhs.R) 
			&& Maths.NearEqual(lhs.G, rhs.G) && Maths.NearEqual(lhs.B, rhs.B) && Maths.NearEqual(lhs.A, rhs.A); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public static Color ZeroAlpha { get { 		return new Color(1.0f, 1.0f, 1.0f, 0.0f); } }

		public static Color White { get { 			return new Color(1.0f, 1.0f, 1.0f); } }
		public static Color VeryLightGray { get { 	return new Color(0.84f, 0.84f, 0.84f); } }
		public static Color LightGray { get { 		return new Color(0.64f, 0.64f, 0.64f); } }
		public static Color Gray { get { 			return new Color(0.44f, 0.44f, 0.44f); } }
		public static Color DarkGray { get { 		return new Color(0.34f, 0.34f, 0.34f); } }
		public static Color Black { get { 			return new Color(0.0f, 0.0f, 0.0f); } }

		public static Color LightRed { get { 		return new Color(0xff, 0x4d, 0x42); } }
		public static Color Red { get { 			return new Color(0xff, 0x32, 0x2b); } }
		public static Color DarkRed { get { 		return new Color(0x82, 0x14, 0x0f); } }

		public static Color Orange { get { 			return new Color(0xff, 0xb5, 0x31); } }
		public static Color Yellow { get { 			return new Color(0xcc, 0xbc, 0x1a); } }
		public static Color LightYellow { get { 	return new Color(0xdd, 0xcd, 0x5a); } }

		public static Color VeryLightGreen { get { 	return new Color(0x7a, 0xff, 0xa5); } }
		public static Color LightGreen { get { 		return new Color(0x5a, 0xff, 0x75); } }
		public static Color Green { get { 			return new Color(0x25, 0xc7, 0x3f); } }
		public static Color DarkGreen { get { 		return new Color(0x05, 0x33, 0x0c); } }

		public static Color LightBlue { get { 		return new Color(0x67, 0x78, 0xff); } }
		public static Color Blue { get { 			return new Color(0x4e, 0x65, 0xff); } }
		public static Color DarkBlue { get { 		return new Color(0x15, 0x20, 0x66); } }

		public static Color LightPurple { get { 	return new Color(0x98, 0x72, 0xa2); } }
		public static Color Purple { get { 			return new Color(0x6c, 0x2a, 0xcc); } }
		public static Color Magenta { get { 		return new Color(1.0f, 0.0f, 1.0f); } }

		public static Color Brown { get { 			return new Color(0x99, 0x32, 0x00); } }


		public static Color Lerp(Color zeroColor, Color oneColor, float dist)
		{
			float oneMinusDist = 1 - dist;
			return new Color(
				oneMinusDist * zeroColor.R + dist * oneColor.R,
				oneMinusDist * zeroColor.G + dist * oneColor.G,
				oneMinusDist * zeroColor.B + dist * oneColor.B,
				oneMinusDist * zeroColor.A + dist * oneColor.A);
		}

		public Color ApplyGamma()
		{
			Color ret = new Color();
			ret.R = (float)Math.Pow(R, 2.2f);
			ret.G = (float)Math.Pow(G, 2.2f);
			ret.B = (float)Math.Pow(B, 2.2f);
			return ret;
		}

		public Color UndoGamma()
		{
			Color ret = new Color();
			ret.R = (float)Math.Pow(R, 1/2.2f);
			ret.G = (float)Math.Pow(G, 1/2.2f);
			ret.B = (float)Math.Pow(B, 1/2.2f);
			return ret;
		}

		public float Value()
		{
			//this isn't exact, don't be such a nerd
			return 0.3f*R + 0.6f*G + 0.1f*B;
		}

		public Color AsGrayscale() { return new Color(Value(), Value(), Value(), this.A); }

		public Color AtAlpha(float alpha) { Color ret = this; ret.A = alpha; return ret; }

		public string AsHexTuple()
		{
			byte rByte = (byte)(R*255);
			byte gByte = (byte)(G*255);
			byte bByte = (byte)(B*255);
			byte aByte = (byte)(A*255);
			return rByte.ToString("X2") + gByte.ToString("X2") + bByte.ToString("X2") + aByte.ToString("X2");
		}

		public override string ToString() { return "R: " + R.ToString("F2") + " G: " + G.ToString("F2") + " B: " + B.ToString("F2") + " A: " + A.ToString("F2"); }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("R", R).BDWriteAttr("G", G).BDWriteAttr("B", B).BDWriteAttr("A", A); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("R", ref R).BDReadAttr("G", ref G).BDReadAttr("B", ref B).BDReadAttr("A", ref A); }
	}
}
