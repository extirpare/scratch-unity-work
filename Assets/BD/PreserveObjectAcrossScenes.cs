﻿using UnityEngine;

public class PreserveObjectAcrossScenes : MonoBehaviour 
{
	void Awake() 
	{
		DontDestroyOnLoad(this.gameObject);
	}
}
