﻿using UnityEngine;
using BD;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class AlphaPulseSprite : MonoBehaviour 
{
	[ShowInInspector, ReadOnly] float m_startTime;
	public List<SpriteRenderer> SpriteRenderers = new List<SpriteRenderer>();
	public List<UnityEngine.UI.Image> UiImages = new List<UnityEngine.UI.Image>();

	public float PulseTime;
	public AnimationCurve NormalizedCurve;

	void OnEnable()
	{
		m_startTime = Time.time;
		Update();
	}

	void Update() 
	{
		float currentTime = Time.time - m_startTime;
		float remainder = BD.Maths.ToCountAndRemainder(currentTime, PulseTime).Second;
		float normalizedTime = remainder / PulseTime;
		float alpha = NormalizedCurve.Evaluate(normalizedTime);

		foreach (var uiImage in UiImages)
			uiImage.color = new BD.Color(uiImage.color).AtAlpha(alpha);
		foreach (var spriteRenderer in SpriteRenderers)
			spriteRenderer.color = new BD.Color(spriteRenderer.color).AtAlpha(alpha);
	}
}
