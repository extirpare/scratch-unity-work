﻿namespace BD
{
	public static class Physics
	{
		enum eSide
		{
			Left,
			Right,
			OnTop
		}
		
		//http://stackoverflow.com/questions/1119627/how-to-test-if-a-point-is-inside-of-a-convex-polygon-in-2d-integer-coordinates
		//The point's inside the convex hull if it lies on the same direction of all line segments
		public static bool IsInsideConvexHull(BD.Vector2 point, BD.Vector2[] orderedConvexHullPts, bool connectLastToFirst)
		{
			if (orderedConvexHullPts.Length < 2)
				return false;

			float firstCross = (orderedConvexHullPts[1] - orderedConvexHullPts[0]).Cross(point - orderedConvexHullPts[0]);
			eSide desiredSide = (firstCross > BD.Maths.Epsilon) ? eSide.Right : (firstCross < -BD.Maths.Epsilon) ? eSide.Left : eSide.OnTop;

			if (desiredSide == eSide.OnTop) return false;

			for (int i = 1; i < orderedConvexHullPts.Length - 1; ++i)
			{
				float thisCross = (orderedConvexHullPts[i+1] - orderedConvexHullPts[i]).Cross(point - orderedConvexHullPts[i]);
				eSide thisSide = (thisCross > BD.Maths.Epsilon) ? eSide.Right : (thisCross < -BD.Maths.Epsilon) ? eSide.Left : eSide.OnTop;

				if (thisSide != desiredSide) return false;
			}

			if (connectLastToFirst)
			{
				float thisCross = (orderedConvexHullPts[0] - orderedConvexHullPts[orderedConvexHullPts.Length - 1]).Cross(point - orderedConvexHullPts[orderedConvexHullPts.Length - 1]);
				eSide thisSide = (thisCross > BD.Maths.Epsilon) ? eSide.Right : (thisCross < -BD.Maths.Epsilon) ? eSide.Left : eSide.OnTop;

				if (thisSide != desiredSide) return false;
			}

			return true;
		}
	}
}
