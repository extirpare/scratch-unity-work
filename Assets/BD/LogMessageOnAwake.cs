﻿using UnityEngine;

public class LogMessageOnAwake : MonoBehaviour 
{
	void Awake()
	{
		Debug.Log("I'm " + this.gameObject.name + " and I'm getting Awake()d!");
	}
}
