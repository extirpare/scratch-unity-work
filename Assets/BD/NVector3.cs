﻿using System;
using System.Xml;

//NVector3 is for, like, actual-definition-of-vector (normalized). Use them for directions.

namespace BD
{
	[Xmlable("NVector3")]
	public struct NVector3
	{
		public float X { get { return m_x; } }
		public float Y { get { return m_y; } }
		public float Z { get { return m_z; } }

		private float m_x;
		private float m_y;
		private float m_z;

		public NVector3(float x, float y, float z) { m_x = x; m_y = y; m_z = z; AssertNormalized(); }
		public static implicit operator NVector3(UnityEngine.Vector2 v) { NVector3 ret = new NVector3(v.x, v.y, 0); ret.AssertNormalized(); return ret; }
		public static implicit operator NVector3(UnityEngine.Vector3 v) { NVector3 ret = new NVector3(v.x, v.y, v.z); ret.AssertNormalized(); return ret; }
		public static implicit operator NVector3(BD.Vector2 v) { NVector3 ret = new NVector3(v.X, v.Y, 0); ret.AssertNormalized(); return ret; }
		public static implicit operator NVector3(BD.Vector3 v) { NVector3 ret = new NVector3(v.X, v.Y, v.Z); ret.AssertNormalized(); return ret; }
		public static implicit operator UnityEngine.Vector2(NVector3 v) { return new UnityEngine.Vector2(v.X, v.Y); }
		public static implicit operator UnityEngine.Vector3(NVector3 v) { return new UnityEngine.Vector3(v.X, v.Y, v.Z); }
		public static implicit operator BD.Vector2(NVector3 v) { return new BD.Vector2(v.X, v.Y); }
		public static implicit operator BD.Vector3(NVector3 v) { return new BD.Vector3(v.X, v.Y, v.Z); }


		public static Vector3 operator *(float val, NVector3 a) { return new Vector3(a.X * val, a.Y * val, a.Z * val); }
		public static Vector3 operator /(float val, NVector3 a) { return new Vector3(a.X / val, a.Y / val, a.Z / val); }
		public static Vector3 operator *(NVector3 a, float val) { return new Vector3(a.X * val, a.Y * val, a.Z * val); }
		public static Vector3 operator /(NVector3 a, float val) { return new Vector3(a.X / val, a.Y / val, a.Z / val); }

		public static NVector3 Zero { get { return new NVector3(0, 0, 0); } }
		public static NVector3 Right { get { return new NVector3(1, 0, 0); } }
		public static NVector3 Left { get { return new NVector3(-1, 0, 0); } }
		public static NVector3 Up { get { return new NVector3(0, 1, 0); } }
		public static NVector3 Down { get { return new NVector3(0, -1, 0); } }
		public static NVector3 Out { get { return new NVector3(0, 0, 1); } }
		public static NVector3 In { get { return new NVector3(0, 0, -1); } }

		//this is the unary operator, like "x = -myVector"
		public static NVector3 operator -(NVector3 val) { return new NVector3(-val.X, -val.Y, -val.Z); }

		public static bool operator ==(NVector3 lhs, NVector3 rhs)
		{
			return BD.Maths.NearEqual(lhs.X, rhs.X)
				&& BD.Maths.NearEqual(lhs.Y, rhs.Y)
				&& BD.Maths.NearEqual(lhs.Z, rhs.Z);
		}

		public static bool operator !=(NVector3 lhs, NVector3 rhs) { return !(lhs == rhs); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString() { return "N<<" + X + ", " + Y + ", " + Z + ">>"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y).BDWriteAttr("Z", Z); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref m_x).BDReadAttr("Y", ref m_y).BDReadAttr("Z", ref m_z); }

		public static implicit operator NVector3(NVector2 v) { return new NVector3(v.X, v.Y, 0); }

		public static bool IsSameLine(NVector3 a, NVector3 b) { return a ==b || a == -b; }

		public void AssertNormalized() { UnityEngine.Debug.Assert( Maths.NearEqual((float)Math.Sqrt(X*X + Y*Y + Z*Z), 1) || Maths.NearEqual(X*X + Y*Y + Z*Z, 0) ); }

		public float Dot(Vector3 other)
		{
			return this.X * other.X + this.Y * other.Y + this.Z * other.Z;
		}

		//Force-normalizes result, deal with it
		public NVector3 Cross(NVector3 other)
		{
			UnityEngine.Debug.Assert(!IsSameLine(this, other));
			Vector3 crossedPt = new Vector3();
			crossedPt.X = this.Y*other.Z - this.Z*other.Y;
			crossedPt.Y = this.Z*other.X - this.X*other.Z;
			crossedPt.Z = this.X*other.Y - this.Y*other.X;
			return crossedPt.Normalized;
		}

		public bool PointsAlong(NVector3 axis)
		{
			return this.Dot(axis) > Maths.Epsilon;
		}

		public bool IsPerpendicularTo(NVector3 other)
		{
			return Maths.NearEqual(this.Dot(other), 0f);
		}

		public NVector3 GetPerpendicularVector()
		{
			if (this == NVector3.Up || this == NVector3.Down)
				return this.Cross(NVector3.Right);
			return this.Cross(NVector3.Up);
		}

		public NVector3 CalcMutuallyPerpWith(NVector3 other)
		{
			if (this == other)
				return GetPerpendicularVector();
			return this.Cross(other);
		}

		public NVector3 RotateAround(NVector3 axis, float degrees)
		{
			throw new System.NotImplementedException();
		}
	}
}
