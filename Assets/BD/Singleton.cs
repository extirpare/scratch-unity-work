﻿//This is a non-Unity singleton, which isn't meant to be used on MonoBehaviours

namespace BD
{
	public class Singleton<T>
		where T : class, new()
	{
		static T k_instance = null;
		public static T Get()
		{
			if (object.ReferenceEquals(k_instance, null))
				k_instance = new T();

			return k_instance;
		}

		public static T Instance { get { return Get(); } }
	}
}
