﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer)), ExecuteInEditMode]
public class SpriteCopier : MonoBehaviour 
{
	SpriteRenderer m_assocSprite;

	public SpriteRenderer ToCopyFrom;
	SpriteReplacer m_toCopyFromSpriteReplacer;

	void Awake()
	{
		m_assocSprite = GetComponent<SpriteRenderer>();
		m_toCopyFromSpriteReplacer = ToCopyFrom.GetComponent<SpriteReplacer>();
	}

	void LateUpdate() 
	{
		m_assocSprite.sprite = ToCopyFrom.sprite;
		if (m_toCopyFromSpriteReplacer != null)
			m_assocSprite.sprite = m_toCopyFromSpriteReplacer.TryAndReplace(m_assocSprite.sprite);
	}
}
