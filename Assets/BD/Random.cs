﻿using System.Collections.Generic;
using UnityEngine;

namespace BD
{
	public static class Random
	{
		static RandImpl k_random = new RandImpl();

		static string[] k_titleWords =
		{
			"Amiable",
			"Beast",
			"Chaucer",
			"Delaware",
			"Ermine",
			"Footnote",
			"Goodness",
			"Hacking",
			"Infinite",
			"Jest",
			"Kremlin",
			"Lossy",
			"Mode",
			"Northern",
			"Opulent",
			"Practice",
			"Quatrain",
			"Recreate",
			"Stanza",
			"Trope",
			"Under",
			"Venerate",
			"Without",
			"X-Ray",
			"Young",
			"Zeppelin"
		};

		static string[] k_maleFirstNames =
		{
			"Andrew",
			"Ben",
			"Charlie",
			"David",
			"Eric",
			"Flynn",
			"Greg",
			"Harry",
			"Jorge",
			"Kevin",
			"Luis",
			"Mike",
			"Nick",
			"Ryan",
			"Tom",
			"Wallace",
			"Zachary"
		};

		static string[] k_lastNames =
		{
			"Apex",
			"Borges",
			"Cantrell",
			"Danielewski",
			"Eggers",
			"Foster",
			"Graywall",
			"Hughson",
			"Johnson",
			"Luis",
			"Mikelsen",
			"Normcore",
			"Opal",
			"Praxis",
			"Redtide",
			"Sanderson",
			"Thompson",
			"Violent",
			"Wallace",
			"Yandere",
			"Zachary"
		};

		public static string Title(int length)
		{
			UnityEngine.Debug.Assert(length > 0);
			return Concatenate(k_titleWords, length);
		}

		public static string MaleName(int length)
		{
			UnityEngine.Debug.Assert(length > 0);
			string firstName = k_maleFirstNames[k_random.Next(k_maleFirstNames.Length)];

			if (length == 1)
				return firstName;
			else
				return firstName + " " + Concatenate(k_lastNames, length - 1);
		}

		static string Concatenate(string[] words, int wordCount)
		{
			UnityEngine.Debug.Assert(wordCount < words.Length);

			List<int> indices = new List<int>();
			string ret = "";
			for (int i = 0; i < wordCount; ++i)
			{
				int index = k_random.Next(words.Length);

				//don't give us a repeat
				while (indices.Contains(index))
					index = k_random.Next(words.Length);

				ret += (i > 0 ? " " : "") + words[index];
				indices.Add(index);
			}

			return ret;
		}

		
		//inclusive of start, not inclusive of end
		public static int Range(int start, int end)
		{
			return k_random.Next(start, end);
		}

		public static float Range(float start, float end)
		{
			float normalized = k_random.NextFloat();
			return start + (end - start) * normalized;
		}

		//this is the Fisher-Yates shuffle.
		//returns, like, [0 4 3 1 6 2 5] if you pass count == 7
		public static int[] ShuffledIndices(int count)
		{
			int[] ret = new int[count];
			for (int i = 0; i < count; ++i) ret[i] = i;
			for (int i = count - 1; i >= 1; --i)
			{
				int swapIndex = k_random.Next(i);
				Algo.Swap(ref ret[swapIndex], ref ret[i]);
			}

			return ret;
		}

		//from http://stackoverflow.com/questions/218060/random-gaussian-variables

		//returns a number randomly picked from a Gaussian distribution with given mean and std dev
		public static float RandomGaussian(float stdDev) { return RandomGaussian(0, stdDev); }
		public static float RandomGaussian(float mean, float stdDev)
		{
			float rand1 = k_random.NextFloat();
			float rand2 = k_random.NextFloat();

			//this is a Box-Muller transform and gives a rand in 0-1 normal distribution!
			float randFromNormal = Mathf.Sqrt(-2f * Mathf.Log(rand1))  //don't worry, rand is 0-1 and log(1) is 0 and log(0.00001) is -5 so stdDev=1 -> 99.999% return <=|5|
								   * Mathf.Sin(2f * Maths.Pi * rand2);

			float ret = mean + stdDev * randFromNormal;
			return ret;
		}

		public static float RandomGaussian(float mean, float stdDevDown, float stdDevUp)
		{
			float rand1 = k_random.NextFloat();
			float rand2 = k_random.NextFloat();

			//anyhow this is a Box-Muller transform and gives a rand in 0-1 normal distribution!
			float randFromNormal = Mathf.Sqrt(-2f * Mathf.Log(rand1))  //don't worry, rand is 0-1 and log(1) is 0 and log(0.00001) is -5
								   * Mathf.Sin(2f * Maths.Pi * rand2);

			float ret = mean + ((randFromNormal < 0) ? stdDevDown : stdDevUp) * randFromNormal;
			return ret;
		}

		public static float RandomGaussian(float mean, float stdDev, float min, float max)
		{
			return BD.Maths.Clamp(RandomGaussian(mean, stdDev), min, max);
		}

		public static float RandomGaussian(float mean, float stdDevDown, float stdDevUp, float min, float max)
		{
			return BD.Maths.Clamp(RandomGaussian(mean, stdDevDown, stdDevUp), min, max);
		}

		// [0,1)
		public static float NextFloat()
		{
			return k_random.NextFloat();
		}

		// [min,max)
		public static float NextFloat(float min, float max)
		{
			return min + (max - min) * NextFloat();
		}

		// [0, INT_MAX)
		public static int NextInt()
		{
			return k_random.NextInt();
		}

		// [0, max)
		public static int NextInt(int max)
		{
			return NextInt(0, max);
		}

		// [min,max)
		public static int NextInt(int min, int max)
		{
			return min + NextInt() % (max - min);
		}

		public static T RandomEnum<T>()
			where T : struct, System.IConvertible //there's no actual enum constraint, this comes close tho
		{
			T[] values = System.Enum.GetValues(typeof(T)) as T[];
			return values[NextInt(0, values.Length)];
		}

		public static BD.Vector2 PointInCircle(float radius)
		{
			float radians = NextFloat() * BD.Maths.TwoPi;
			float distance = NextFloat() * radius;
			float sin = Mathf.Sin(radians);
			float cos = Mathf.Cos(radians);
			return new BD.Vector2(cos * distance, sin * distance);
		}

		public static T FromList<T>(List<T> vals)
		{
			return vals[NextInt(vals.Count)];
		}

		public static T FromArray<T>(T[] vals)
		{
			return vals[NextInt(vals.Length)];
		}
	}
}
