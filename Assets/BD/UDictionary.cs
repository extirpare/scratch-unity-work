﻿using UnityEngine;
using System;
using System.Collections.Generic;

//UDictionary is a System.Collections.Generic.Dictionary that also serializes appropriately

namespace BD
{
	[Serializable]
	public class UDictionary<TKey, TValue> : ISerializationCallbackReceiver
	{
		[SerializeField] List<TKey> m_serializedKeys = new List<TKey>();
		[SerializeField] List<TValue> m_serializedVals  = new List<TValue>();

		Dictionary<TKey, TValue> m_dictionary = new Dictionary<TKey, TValue>();
		public System.Object LockObject = new System.Object();

		public void OnBeforeSerialize()
		{
			m_serializedKeys.Clear();
			m_serializedVals.Clear();

			if (m_dictionary == null)
				m_dictionary = new Dictionary<TKey, TValue>();

			lock (LockObject)
			{
				var iter = m_dictionary.GetEnumerator();
				while (iter.MoveNext())
				{
					m_serializedKeys.Add(iter.Current.Key);
					m_serializedVals.Add(iter.Current.Value);
				}
			}
		}

		public void OnAfterDeserialize()
		{
			if (m_serializedKeys.Count != m_serializedVals.Count)
			{
				Debug.LogWarning("UDictionary deserialize mismatch: " + m_serializedKeys.Count + " keys to "
					+ m_serializedVals.Count + " vals. Resizing vals to keys.");
				while (m_serializedVals.Count < m_serializedKeys.Count)
					m_serializedVals.Add((TValue)Activator.CreateInstance(typeof(TValue)));
				if (m_serializedVals.Count > m_serializedKeys.Count)
					m_serializedVals.RemoveRange(m_serializedKeys.Count, m_serializedVals.Count - m_serializedKeys.Count);
			}

			Debug.Assert(m_serializedKeys.Count == m_serializedVals.Count);

			lock (LockObject)
			{
				m_dictionary = new Dictionary<TKey, TValue>();

				for (int i = 0; i < m_serializedKeys.Count; ++i)
				{
					if (m_serializedKeys[i] == null)
						m_serializedKeys[i] = TypeHelper.CreateNewInstanceOf<TKey>();

					if (m_serializedKeys[i] == null)
					{
						Debug.LogWarning("UDictionary< " + typeof(TKey) + ", " + typeof(TValue) + "> OnAfterDeserialize given null key! Discarding...");
						continue;
					}
					
					if (m_dictionary.ContainsKey(m_serializedKeys[i]))
					{
						TKey newKeyInstance = TypeHelper.CreateNewInstanceOf<TKey>();

						if (!m_dictionary.ContainsKey(newKeyInstance))
							m_dictionary.Add(newKeyInstance, m_serializedVals[i]);
						else
						{
							Debug.LogError("UDictionary deserialize sees many copies of key '" + m_serializedKeys[i] + "'. Please remove an instance of this key.");
							return;
						}
					}
					else
						m_dictionary.Add(m_serializedKeys[i], m_serializedVals[i]);
				}
			}
		}

		public bool ContainsKey(TKey key)
		{
			lock (LockObject)
				return m_dictionary.ContainsKey(key);
		}
		
		public TValue this[TKey key] 
		{
			get {
				lock (LockObject)
					return m_dictionary[key];
			}

			set {
				lock (LockObject)
					m_dictionary[key] = value;
			}
		}

		public Dictionary<TKey, TValue>.KeyCollection Keys
		{ get {
			lock (LockObject)
				return m_dictionary.Keys;
		} }

		public Dictionary<TKey, TValue>.ValueCollection Values
		{ get {
			lock (LockObject)
				return m_dictionary.Values;
		} }

		public void Add(TKey key, TValue value)
		{
			lock (LockObject)
				m_dictionary.Add(key, value);
		}

		public void Remove(TKey key)
		{
			lock (LockObject)
				m_dictionary.Remove(key);
		}

		public void Clear()
		{
			lock (LockObject)
				m_dictionary.Clear();
		}

		public Dictionary<TKey, TValue>.Enumerator GetEnumerator()
		{
			lock (LockObject)
				return m_dictionary.GetEnumerator();
		}
	}
}
