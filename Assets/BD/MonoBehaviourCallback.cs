﻿using BD;
using UnityEngine;
using Sirenix.OdinInspector;

public class MonoBehaviourCallback
{
	public enum ePriority
	{
		VeryEarly,
		Early,
		Normal,
		Late,
		VeryLate
	}

	public delegate void DelegateFunc();

	[ShowInInspector] EnumToVal<ePriority, DelegateFunc> m_delegates = new EnumToVal<ePriority, DelegateFunc>();

	public void Add(DelegateFunc func)
	{
		Add(func, ePriority.Normal);
	}

	public void Add(DelegateFunc func, ePriority priority)
	{
		if (m_delegates[priority] == null)
			m_delegates[priority] = func;
		else
		{
			foreach (DelegateFunc funcToCall in m_delegates[priority].GetInvocationList())
				if (funcToCall.Equals(func))
					return;
			
			m_delegates[priority] += func;
		}
	}

	public void Remove(DelegateFunc func)
	{
		foreach (ePriority priority in EnumHelper.GetValuesIn<ePriority>())
			m_delegates[priority] -= func;
	}

	public void Clear()
	{
		foreach (ePriority priority in EnumHelper.GetValuesIn<ePriority>())
			m_delegates[priority] = null;
	}

	public void Call()
	{
		foreach (ePriority priority in EnumHelper.GetValuesIn<ePriority>())
			if (m_delegates[priority] != null)
				foreach (DelegateFunc funcToCall in m_delegates[priority].GetInvocationList())
					funcToCall();
	}
}

//I don't like this copy/paste, but we can't template T == void, and I want that, so I'm copy-pasting
//http://stackoverflow.com/questions/11318973/void-in-c-sharp-generics
public class MonoBehaviourCallback<T>
{
	public delegate void DelegateFunc(T val);
	public delegate void DelegateFuncNoPass();

	[ShowInInspector] EnumToVal<MonoBehaviourCallback.ePriority, DelegateFunc> m_delegates = new EnumToVal<MonoBehaviourCallback.ePriority, DelegateFunc>();
	[ShowInInspector] EnumToVal<MonoBehaviourCallback.ePriority, DelegateFuncNoPass> m_delegatesNoPass = new EnumToVal<MonoBehaviourCallback.ePriority, DelegateFuncNoPass>();

	public void Add(DelegateFunc func)
	{
		Add(func, MonoBehaviourCallback.ePriority.Normal);
	}

	public void Add(DelegateFuncNoPass func)
	{
		Add(func, MonoBehaviourCallback.ePriority.Normal);
	}

	public void Add(DelegateFunc func, MonoBehaviourCallback.ePriority priority)
	{
		if (m_delegates[priority] == null)
			m_delegates[priority] = func;
		else
		{
			foreach (DelegateFunc funcToCall in m_delegates[priority].GetInvocationList())
				if (funcToCall.Equals(func))
					return;

			m_delegates[priority] += func;
		}
	}

	public void Add(DelegateFuncNoPass func, MonoBehaviourCallback.ePriority priority)
	{
		if (m_delegatesNoPass[priority] == null)
			m_delegatesNoPass[priority] = func;
		else
		{
			foreach (DelegateFuncNoPass funcToCall in m_delegatesNoPass[priority].GetInvocationList())
				if (funcToCall.Equals(func))
					return;
			
			m_delegatesNoPass[priority] += func;
		}
	}

	public void Remove(DelegateFunc func)
	{
		foreach (MonoBehaviourCallback.ePriority priority in EnumHelper.GetValuesIn<MonoBehaviourCallback.ePriority>())
			m_delegates[priority] -= func;
	}

	public void Remove(DelegateFuncNoPass func)
	{
		foreach (MonoBehaviourCallback.ePriority priority in EnumHelper.GetValuesIn<MonoBehaviourCallback.ePriority>())
			m_delegatesNoPass[priority] -= func;
	}

	public void Call(T val)
	{
		foreach (MonoBehaviourCallback.ePriority priority in EnumHelper.GetValuesIn<MonoBehaviourCallback.ePriority>())
		{
			if (m_delegates[priority] != null)
				foreach (DelegateFunc funcToCall in m_delegates[priority].GetInvocationList())
					funcToCall(val);
					
			if (m_delegatesNoPass[priority] != null)
				foreach (DelegateFuncNoPass funcToCall in m_delegatesNoPass[priority].GetInvocationList())
					funcToCall();
		}
	}
}
