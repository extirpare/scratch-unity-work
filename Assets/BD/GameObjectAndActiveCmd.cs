﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

public enum eActiveCmd
{
	//Turn the game object on/off
	Activate,
	Deactivate,
	ToggleActivate,

	//Set the associated CanvasGroup alpha
	ShowCanvas,
	HideCanvas,
	ToggleCanvas
}

[Serializable]
public struct GameObjectAndActiveCmd
{
	[HorizontalGroup(0.8f), HideLabel] public GameObject Object;
	[HorizontalGroup(0.2f), HideLabel] public eActiveCmd Cmd;

	public void Apply()
	{
		if (Object == null) return;
		switch (Cmd)
		{
			case eActiveCmd.Activate:
				Object.SetActive(true);
				break;
			case eActiveCmd.Deactivate:
				Object.SetActive(false);
				break;
			case eActiveCmd.ToggleActivate:
				Object.SetActive(!Object.activeSelf);
				break;
			case eActiveCmd.ShowCanvas:
				Object.GetComponent<CanvasGroup>().alpha = 1;
				break;
			case eActiveCmd.HideCanvas:
				Object.GetComponent<CanvasGroup>().alpha = 0;
				break;
			case eActiveCmd.ToggleCanvas:
				Object.GetComponent<CanvasGroup>().alpha = (Object.GetComponent<CanvasGroup>().alpha > 0) ? 0 : 1;
				break;
			default: throw new System.Exception();
		}
	}

	public void ApplyOpposite()
	{
		switch (Cmd)
		{
			case eActiveCmd.Activate:
				Object.SetActive(false);
				break;
			case eActiveCmd.Deactivate:
				Object.SetActive(true);
				break;
			case eActiveCmd.ToggleActivate:
				Object.SetActive(!Object.activeSelf);
				break;
			case eActiveCmd.ShowCanvas:
				Object.GetComponent<CanvasGroup>().alpha = 0;
				break;
			case eActiveCmd.HideCanvas:
				Object.GetComponent<CanvasGroup>().alpha = 1;
				break;
			case eActiveCmd.ToggleCanvas:
				Object.GetComponent<CanvasGroup>().alpha = (Object.GetComponent<CanvasGroup>().alpha > 0) ? 0 : 1;
				break;
			default: throw new System.Exception();
		}
	}
}
