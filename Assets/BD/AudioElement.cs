﻿using UnityEngine;
using System;

using Sirenix.OdinInspector;
namespace BD
{
	[Serializable]
	public struct AudioElement
	{
		public AudioClip clip;
		public UnityEngine.Audio.AudioMixerGroup mixerGroup;
		[MinValue(0)] public float volume;

		public void ApplyTo(AudioSource audioSource)
		{
			audioSource.clip = clip;
			audioSource.outputAudioMixerGroup = mixerGroup;
			audioSource.volume = volume;
		}

		public static bool operator !=(AudioElement lhs, AudioElement rhs) { return !(lhs == rhs); }
		public static bool operator ==(AudioElement lhs, AudioElement rhs) 
		{ 
			return lhs.clip == rhs.clip
				&& lhs.mixerGroup == rhs.mixerGroup
				&& Maths.NearEqual(lhs.volume, rhs.volume); 
		}
		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }
	}
}
