﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace BD
{
	public static class XmlHelper
	{
		public class XmlAttributeNotFoundException : Exception
		{
			static string GenerateErrorString(XmlElement parentElement, string missingAttribute)
			{
				return "Unable to find required attribute '" + missingAttribute + "' in element '" + parentElement.Name + "' "
					+ " in document " + parentElement.BaseURI; 
			}
			public XmlAttributeNotFoundException() { }
			public XmlAttributeNotFoundException(XmlElement parentElement, string missingAttribute) 
				: base(GenerateErrorString(parentElement, missingAttribute)) { }
		}

		public static XmlDocument WriteToXmlDoc<T>(T val)
			where T : IXmlable
		{
			string nodeName = typeof(T).BDGetAttr<XmlableAttribute>().Name;
			XmlDocument xmlDoc = new XmlDocument();
			XmlElement root = xmlDoc.CreateElement(nodeName);
			xmlDoc.AppendChild(root);

			try
			{
				val.WriteToXml(root);
			}
			catch (Exception e)
			{
				UnityEngine.Debug.LogError(e.Message);
			}

			return xmlDoc;
		}

		public static XmlDocument WriteToXmlDoc<BaseClassType, SubclassAttributeType>(BaseClassType val)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			string baseClassName = typeof(BaseClassType).BDGetAttr<XmlableAttribute>().Name;
			string subclassTypeName = val.GetType().BDGetAttr<SubclassAttributeType>().Name;

			XmlDocument xmlDoc = new XmlDocument();
			XmlElement root = xmlDoc.CreateElement(baseClassName);
			xmlDoc.AppendChild(root);

			try
			{
				root.BDWriteAttr("Type", subclassTypeName);
				val.WriteToXml(root);
			}
			catch (Exception e)
			{
				UnityEngine.Debug.LogError(e.Message);
			}

			return xmlDoc;
		}

		public static XmlDocument WriteToXmlDoc<T>(List<T> val)
			where T : IXmlable
		{
			string nodeName = typeof(T).BDGetAttr<XmlableAttribute>().Name;
			XmlDocument xmlDoc = new XmlDocument();
			XmlElement root = xmlDoc.CreateElement(nodeName);
			xmlDoc.AppendChild(root);

			try
			{
				root.BDWriteCollection(val);
			}
			catch (Exception e)
			{
				UnityEngine.Debug.LogError(e.Message);
			}

			return xmlDoc;
		}

		public static void WriteToFile<T>(T val, string fileURI)
			where T : IXmlable
		{
			XmlDocument xmlDoc = WriteToXmlDoc(val);
			xmlDoc.Save(BD.FileHelper.GetAbsoluteURI(fileURI));
		}

		public static void WriteToFile<T>(List<T> val, string fileURI)
			where T : IXmlable
		{
			XmlDocument xmlDoc = WriteToXmlDoc(val);
			xmlDoc.Save(BD.FileHelper.GetAbsoluteURI(fileURI));
		}

		public static void ReadFromXmlDoc<T>(ref T val, XmlDocument xmlDoc)
			where T : IXmlable
		{
			val.ReadFromXml(xmlDoc.DocumentElement);
		}

		public static void ReadFromXmlDoc<BaseClassType, SubclassAttributeType>(ref BaseClassType val, XmlDocument xmlDoc)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			xmlDoc.DocumentElement.BDReadNode<BaseClassType, SubclassAttributeType>(ref val);
		}

		public static void ReadFromXmlDoc<T>(List<T> val, XmlDocument xmlDoc)
			where T : IXmlable
		{
			xmlDoc.DocumentElement.BDReadCollection(val);
		}

		public static XmlElement ReadFromFile(string fileURI)
		{
			string absoluteURI = FileHelper.GetAbsoluteURI(fileURI);
			if (!File.Exists(absoluteURI))
			{
				UnityEngine.Debug.LogError("XmlHelper ReadFromFile called on '" + fileURI + "' which does not exist!");
				return null;
			}
			FileStream fileStream = File.OpenRead(absoluteURI);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(fileStream);
			fileStream.Close();

			return xmlDoc.DocumentElement;
		}

		public static void ReadFromFile<T>(ref T val, string fileURI)
			where T : IXmlable
		{
			XmlElement rootElement = ReadFromFile(fileURI);
			val.ReadFromXml(rootElement);
		}

		public static void ReadFromFile<T>(List<T> val, string fileURI)
			where T : IXmlable
		{
			XmlElement rootElement = ReadFromFile(fileURI);
			rootElement.BDReadCollection(val);			
		}


		public static bool BDHasAttr(this XmlElement element, string attrName) { return element.Attributes[attrName] != null; }
		public static bool BDHasChild(this XmlElement element, string childName) { return element.BDTryAndGetChild(childName) != null; }
		public static int BDGetChildCount(this XmlElement element, string childName) { return element.BDGetChildren(childName).Count; }

		public static XmlElement BDTryAndGetChild(this XmlElement element, string childName)
		{
			foreach (var childNode in element.ChildNodes)
			{
				if ((childNode as XmlElement).Name == childName)
					return (XmlElement)childNode;
			}
			return null;
		}

		public static XmlElement BDGetChild(this XmlElement element, string childName)
		{
			XmlElement ret = element.BDTryAndGetChild(childName);
			if (ReferenceEquals(ret, null))
				throw new XmlAttributeNotFoundException(element, childName);
			return ret;
		}

		public static List<XmlElement> BDGetChildren(this XmlElement element, string childName)
		{
			List<XmlElement> ret = new List<XmlElement>();

			foreach (var childNode in element.ChildNodes)
			{
				if ((childNode as XmlElement).Name == childName)
					ret.Add((XmlElement)childNode);
			}
			return ret;
		}

		//returns the child node, not this node, unlike everything else
		public static XmlElement BDCreateChild(this XmlElement element, string childName)
		{
			XmlElement childElem = element.OwnerDocument.CreateElement(childName);
			element.AppendChild(childElem);
			return childElem;
		}



		//
		//  __              __     ___      __   ___  __  
		// |  \ |  |  |\/| |__)     |  \ / |__) |__  /__` 
		// |__/ \__/  |  | |__)     |   |  |    |___.__/ 
        //                                    

		public static XmlElement BDWriteAttr<T>(this XmlElement element, string name, T value)
		{
			UnityEngine.Debug.Assert(!element.BDHasAttr(name));
			
			if (value == null)
				if (typeof(T) == typeof(string))
					value = (T)(object)"";
				else
					value = (T)Activator.CreateInstance(typeof(T));
				
			element.SetAttribute(name, value.ToString());
			return element;
		}

		public static XmlElement BDReadAttr<T>(this XmlElement element, string name, ref T value)
		{
			if (!element.BDHasAttr(name))
			{
				throw new XmlAttributeNotFoundException(element, "XMLHelper BDReadAttr<" + typeof(T).ToString() + ">: element has no attr named '" + name + "'");
			}

			if (typeof(T).IsEnum)
				value = (T)Enum.Parse(typeof(T), element.Attributes[name].Value, true);
			else
				value = (T)Convert.ChangeType(element.Attributes[name].Value, typeof(T));

			return element;
		}

		//If value == defaultValue, write it into element. Else, don't write.
		public static XmlElement BDWriteAttrIfNotDefault<T>(this XmlElement element, string name, T value, T defaultValue)
		{
			if (!EqualityComparer<T>.Default.Equals(value, defaultValue))
				return BDWriteAttr(element, name, value);
			
			return element;
		}

		//If we have an attr called 'name', read it into value. Else, write fallbackValue into value.
		public static XmlElement BDTryAndReadAttr<T>(this XmlElement element, string name, ref T value, T fallbackValue)
		{
			if (element.BDHasAttr(name))
				return BDReadAttr(element, name, ref value);
			else
				value = fallbackValue;

			return element;
		}

		public static XmlElement BDWriteInnerText<T>(this XmlElement element, T value)
		{
			element.InnerText = value.ToString();
			return element;
		}

		public static XmlElement BDReadInnerText<T>(this XmlElement element, ref T value)
		{
			value = (T)Convert.ChangeType(element.InnerText, typeof(T));
			return element;
		}

		public static XmlElement BDWriteCollectionSimple<T>(this XmlElement element, string name, ICollection<T> collection)
		{
			element.BDCreateChild(name).BDWriteCollectionSimple(collection);
			return element;
		}

		public static XmlElement BDWriteCollectionSimple<T>(this XmlElement element, ICollection<T> collection)
		{
			foreach (T item in collection)
				element.BDCreateChild("Value").BDWriteInnerText(item);

			return element;
		}

		public static XmlElement BDReadCollectionSimple<T>(this XmlElement element, string name, ICollection<T> collection)
		{
			element.BDGetChild(name).BDReadCollectionSimple(collection);
			return element;
		}

		public static XmlElement BDReadCollectionSimple<T>(this XmlElement element, ICollection<T> collection)
		{
			collection.Clear();

			List<XmlElement> children = element.BDGetChildren("Value");

			foreach (XmlElement child in children)
			{
				T val = (T)Convert.ChangeType(child.InnerText, typeof(T));
				collection.Add(val);
			}

			return element;
		}



		//
		//  __           __        ___                           __        ___ 
		// /__` |  |\/| |__) |    |__     | \_/  |\/| |     /\  |__) |    |__
		// .__/ |  |  | |    |___ |___    | / \  |  | |___ /~~\ |__) |___ |___
		//

		public static XmlElement BDWriteChild<T>(this XmlElement element, T childVal)
			where T : IXmlable
		{
			return element.BDWriteChild(typeof(T).BDGetAttr<XmlableAttribute>().Name, childVal);
		}

		public static XmlElement BDWriteChild<T>(this XmlElement element, string childName, T childVal)
			where T : IXmlable
		{
			XmlElement childNode = element.BDCreateChild(childName);
			childVal.WriteToXml(childNode);
			return childNode;
		}

		public static XmlElement BDReadChild<T>(this XmlElement element, ref T childVal)
			where T : IXmlable
		{
			return element.BDReadChild(typeof(T).BDGetAttr<XmlableAttribute>().Name, ref childVal);
		}

		public static XmlElement BDReadChild<T>(this XmlElement element, string childName, ref T childVal)
			where T : IXmlable
		{
			if (element.BDGetChildCount(childName) != 1)
			{
				UnityEngine.Debug.LogError("XMLHelper BDReadChild<" + typeof(T).ToString() + ">: element has no child named '" + childName + "', skipping");
				return element;
			}

			XmlElement childNode = element.BDGetChild(childName);
			childVal.ReadFromXml(childNode);
			return element;
		}

		public static XmlElement BDWriteChild<BaseClassType, SubclassAttributeType>(this XmlElement element, BaseClassType childVal)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			string baseClassName = typeof(BaseClassType).BDGetAttr<XmlableAttribute>().Name;
			string subclassTypeName = childVal.GetType().BDGetAttr<SubclassAttributeType>().Name;

			XmlElement childNode = element.BDCreateChild(baseClassName);
			childNode.BDWriteAttr("Type", subclassTypeName);
			childVal.WriteToXml(childNode);

			return childNode;
		}

		public static XmlElement BDReadChild<BaseClassType, SubclassAttributeType>(this XmlElement element, ref BaseClassType childVal)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			string baseClassName = typeof(BaseClassType).BDGetAttr<XmlableAttribute>().Name;
			XmlElement childNode = element.BDGetChild(baseClassName);
			return childNode.BDReadNode<BaseClassType, SubclassAttributeType>(ref childVal);
		}

		public static XmlElement BDReadNode<BaseClassType, SubclassAttributeType>(this XmlElement element, ref BaseClassType childVal)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			string subclassTypeName = "";
			element.BDReadAttr("Type", ref subclassTypeName);
			Type childType = AttributeHelper.TryAndGetTypeWithNamedAttribute<SubclassAttributeType>(subclassTypeName);
			UnityEngine.Debug.Assert(childType.BDIsOfType(typeof(BaseClassType)));

			childVal = (BaseClassType)Activator.CreateInstance(childType);
			childVal.ReadFromXml(element);
			return element;
		}

		//
		//  ___            __                                __        ___ 
		// |__   /\  |\ | /  ` \ /    | \_/  |\/| |     /\  |__) |    |__  
		// |    /~~\ | \| \__,  |     | / \  |  | |___ /~~\ |__) |___ |___
		//

		public static XmlElement BDWriteCollection<BaseClassType, SubclassAttributeType>(this XmlElement element, string collectionName, ICollection<BaseClassType> collection)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			element.BDCreateChild(collectionName).BDWriteCollection<BaseClassType, SubclassAttributeType>(collection);
			return element;
		}

		public static XmlElement BDWriteCollection<BaseClassType, SubclassAttributeType>(this XmlElement element, ICollection<BaseClassType> collection)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			foreach (BaseClassType item in collection)
				element.BDWriteChild<BaseClassType, SubclassAttributeType>(item);

			return element;
		}

		public static XmlElement BDReadCollection<BaseClassType, SubclassAttributeType>(this XmlElement element, string collectionName, ICollection<BaseClassType> collection)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			element.BDGetChild(collectionName).BDReadCollection<BaseClassType, SubclassAttributeType>(collection);
			return element;
		}


		public static XmlElement BDReadCollection<BaseClassType, SubclassAttributeType>(this XmlElement element, ICollection<BaseClassType> collection)
			where BaseClassType : IXmlable
			where SubclassAttributeType : NamedAttribute
		{
			collection.Clear();

			string baseClassTypeName = typeof(BaseClassType).BDGetAttr<XmlableAttribute>().Name;
			List<XmlElement> children = element.BDGetChildren(baseClassTypeName);

			foreach (XmlElement child in children)
			{
				BaseClassType val = default(BaseClassType); //null, if type is a class
				child.BDReadNode<BaseClassType, SubclassAttributeType>(ref val);
				collection.Add(val);
			}

			return element;
		}



		public static XmlElement BDWriteCollection<T>(this XmlElement element, string collectionName, ICollection<T> collection)
			where T : IXmlable
		{
			element.BDCreateChild(collectionName).BDWriteCollection(collection);
			return element;
		}

		public static XmlElement BDWriteCollection<T>(this XmlElement element, ICollection<T> collection)
			where T : IXmlable
		{
			foreach (T item in collection)
				element.BDWriteChild(item);

			return element;
		}

		public static XmlElement BDReadCollection<T>(this XmlElement element, string collectionName, ICollection<T> collection)
			where T : IXmlable
		{
			element.BDGetChild(collectionName).BDReadCollection(collection);
			return element;
		}


		public static XmlElement BDReadCollection<T>(this XmlElement element, ICollection<T> collection)
			where T : IXmlable
		{
			collection.Clear();

			string typeName = typeof(T).BDGetAttr<XmlableAttribute>().Name;
			List<XmlElement> children = element.BDGetChildren(typeName);

			foreach (XmlElement child in children)
			{
				T val = (T)Activator.CreateInstance(typeof(T));
				val.ReadFromXml(child);
				collection.Add(val);
			}

			return element;
		}
	}
}
