﻿using System;
using System.Xml;

//NVector2 is for, like, actual-definition-of-vector (normalized). Use them for directions.

namespace BD
{
	[Xmlable("NVector2")]
	public struct NVector2 : IXmlable
    {
        public float X { get { return m_x; } }
        public float Y { get { return m_y; } }

        private float m_x;
        private float m_y;

        public NVector2(float x, float y) { m_x = x; m_y = y; AssertValid(); }
        public static implicit operator NVector2(UnityEngine.Vector2 v) { NVector2 ret = new NVector2(v.x, v.y); ret.AssertValid(); return ret; }
		public static implicit operator NVector2(UnityEngine.Vector3 v) { NVector2 ret = new NVector2(v.x, v.y); ret.AssertValid(); return ret; }
        public static implicit operator UnityEngine.Vector2(NVector2 v) { return new UnityEngine.Vector2(v.X, v.Y); }
		public static implicit operator UnityEngine.Vector3(NVector2 v) { return new UnityEngine.Vector3(v.X, v.Y); }
		public static implicit operator UnityEngine.Rect(NVector2 v) { return new UnityEngine.Rect(v); }

        public static Vector2 operator *(float val, NVector2 a) { return new Vector2(a.X * val, a.Y * val); }
        public static Vector2 operator /(float val, NVector2 a) { return new Vector2(a.X / val, a.Y / val); }
        public static Vector2 operator *(NVector2 a, float val) { return new Vector2(a.X * val, a.Y * val); }
        public static Vector2 operator /(NVector2 a, float val) { return new Vector2(a.X / val, a.Y / val); }

        public static NVector2 Zero { get { return new NVector2(0, 0); } }
        public static NVector2 Left { get { return new NVector2(-1, 0); } }
        public static NVector2 Right { get { return new NVector2(1, 0); } }
        public static NVector2 Up { get { return new NVector2(0, 1); } }
        public static NVector2 Down { get { return new NVector2(0, -1); } }
		public static NVector2 UpRight { get { return new NVector2(Maths.SqrtTwoOverTwo, -Maths.SqrtTwoOverTwo); } }
		public static NVector2 UpLeft { get { return new NVector2(-Maths.SqrtTwoOverTwo, -Maths.SqrtTwoOverTwo); } }
		public static NVector2 DownRight { get { return new NVector2(Maths.SqrtTwoOverTwo, Maths.SqrtTwoOverTwo); } }
		public static NVector2 DownLeft { get { return new NVector2(-Maths.SqrtTwoOverTwo, Maths.SqrtTwoOverTwo); } }

		//this is the unary operator, like "x = -myVector"
		public static NVector2 operator -(NVector2 val) { return new NVector2(-val.X, -val.Y); }

        public static bool operator !=(NVector2 lhs, NVector2 rhs) { return !(lhs == rhs); }
        public static bool operator ==(NVector2 lhs, NVector2 rhs) { return BD.Maths.NearEqual(lhs.X, rhs.X) && BD.Maths.NearEqual(lhs.Y, rhs.Y); }

        //These silence the compiler about CS0660 / CS0661
        public override bool Equals(object o) { return base.Equals(o); }
        public override int GetHashCode() { return base.GetHashCode(); }

        public override string ToString() { return "N<<" + X + ", " + Y + ">>"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref m_x).BDReadAttr("Y", ref m_y); }

		public static bool IsSameLine(NVector2 a, NVector2 b) { return a == b || a == -b; }

		void AssertValid()
		{
			UnityEngine.Debug.Assert( (X == 0 && Y == 0) || Maths.NearEqual((float)Math.Sqrt(X*X + Y*Y), 1f),
				"NVector attempted to be formed out of X="+X+", Y="+Y );
		}

        public float Dot(Vector2 other)
        {
            return this.X * other.X + this.Y * other.Y;
        }

        public bool PointsAlong(NVector2 axis)
        {
            return this.Dot(axis) > Maths.Epsilon;
        }

        public bool IsPerpendicularTo(NVector2 other)
        {
            return Maths.NearEqual(this.Dot(other), 0f);
        }

        public NVector2 GetPerpendicularVector()
        {
            return Rotate(270);
        }

        //rotates counter-clockwise
        public NVector2 Rotate(float degrees)
        {
            float cos = (float)Math.Cos(degrees * Maths.DegreesToRadians);
            float sin = (float)Math.Sin(degrees * Maths.DegreesToRadians);

            return new NVector2(X*cos - Y*sin, X*sin + Y*cos);
        }

		//returns whichever direction is closer to facing dir: this, or -this
		public NVector2 Face(NVector2 dir)
		{
			float cosTheta = this.Dot(dir);
			return (cosTheta > 0) ? this : -this;
		}
    }
}
