﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteReplacer : MonoBehaviour 
{
	SpriteRenderer m_assocSpriteRenderer;
	public UDictionarySpriteToSprite Replacements;

	void Awake()
	{
		m_assocSpriteRenderer = GetComponent<SpriteRenderer>();
	}

	void LateUpdate() 
	{
		m_assocSpriteRenderer.sprite = TryAndReplace(m_assocSpriteRenderer.sprite);
	}

	public Sprite TryAndReplace(Sprite toReplace)
	{
		if (toReplace != null && Replacements.ContainsKey(toReplace))
		{
			Sprite val = Replacements[toReplace];
			if (val != null)
				return val;
		}

		return toReplace;
	}
}

[System.Serializable]
public class UDictionarySpriteToSprite : BD.UDictionary<Sprite, Sprite> { }