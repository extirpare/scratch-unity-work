﻿using UnityEngine;
using System;
using System.Xml;
using System.Collections.Generic;
using Sirenix.Serialization;

//I don't want to manually make whatever I'm flagging have vals 1 << 0, 1 << 1, 1 << 2 etc so we just go by order in values array

namespace BD 
{
	[Serializable]
	[Xmlable("Flags")]
	public struct FlagsOf<T> : IXmlable
		where T : struct, IConvertible, IComparable, IFormattable
	{
        [OdinSerialize] uint m_flags;
		
		public FlagsOf(T val) { m_flags = 0; Set(val); }
		public FlagsOf(T val1, T val2) { m_flags = 0; Set(val1); Set(val2); }
		public FlagsOf(T val1, T val2, T val3) { m_flags = 0; Set(val1); Set(val2); Set(val3); }
		public FlagsOf(bool fillVal) { m_flags = fillVal ? uint.MaxValue : 0; }

		public static implicit operator FlagsOf<T>(T val) { return new FlagsOf<T>(val); }

		void SanityCheck()
		{
			Debug.Assert(typeof(T).IsEnum);
			foreach (T enumVal in BD.EnumHelper.GetValuesIn<T>())
			{
				Debug.Assert(Enums.ToUInt(enumVal) <= 31);
			}
		}
		
		public bool Contains(T flag) { return Test(flag, true); }
		public bool Test(T flag) { return Test(flag, true); }
		public bool Test(T flag, bool desiredValue)
		{
			return BD.Algo.IsBitSet(m_flags, Enums.ToInt(flag));
		}

		public void Set(T flag) { Set(flag, true); }
		public void Set(T flag, bool val)
		{
			m_flags = BD.Algo.SetBit(m_flags, Enums.ToInt(flag), val);
		}
		
		public bool this[T flagVal]
		{
			get { return Test(flagVal); }
			set { Set(flagVal, value); }
		}

		public void Clear()
		{
			m_flags = 0;
		}

		public int FlaggedCount()
		{
			return BD.Algo.CountOneBits(m_flags);
		}

		public List<T> FlaggedValues()
		{
			List<T> ret = new List<T>();
			foreach (T val in System.Enum.GetValues(typeof(T)))
				if (Test(val))
					ret.Add(val);

			return ret;
		}

		public override string ToString()
		{
			string ret = typeof(T) + ", " + BD.Algo.CountOneBits(m_flags) + " set: ";
			foreach (T val in System.Enum.GetValues(typeof(T)))
			{
				if (Test(val))
					ret += " " + val;
			}

			return ret;
		}

        public void WriteToXml(XmlElement parent)
        {
			foreach (T enumVal in System.Enum.GetValues(typeof(T)))
				parent.BDCreateChild(enumVal.ToString()).BDWriteAttr("Set", this[enumVal]);
        }

        public void ReadFromXml(XmlElement parent)
        {
			Clear();
			foreach (T enumVal in System.Enum.GetValues(typeof(T)))
			{
				bool newVal = false;
				if (parent.BDHasChild(enumVal.ToString()))
					parent.BDGetChild(enumVal.ToString()).BDReadAttr("Set", ref newVal);

				this[enumVal] = newVal;
			}
        }

		public FlagsOf<T> Or(FlagsOf<T> other)
		{
			FlagsOf<T> ret = new FlagsOf<T>();
			foreach (T val in System.Enum.GetValues(typeof(T)))
				ret[val] = this[val] || other[val];
			return ret;
		}

		public FlagsOf<T> And(FlagsOf<T> other)
		{
			FlagsOf<T> ret = new FlagsOf<T>();
			foreach (T val in System.Enum.GetValues(typeof(T)))
				ret[val] = this[val] && other[val];
			return ret;
		}
	}

}