﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.Serialization;
using Sirenix.OdinInspector;

//Literally the whole point of the UGameobjectPool is that it reclaims and hides everything it owns at end of frame
//I'm using it in place of instantiating 100 GameObjects with a sprite renderer, per frame, in like hit point counters

namespace BD
{
	public class UGameObjectPool<T> : SerializedMonoBehaviour
		where T : Component
	{
		[SerializeField] T m_prefabComponent;
		public GameObject PrefabObject 
		{
			get { return m_prefabComponent.gameObject; }
			set { Debug.Assert(value.BDHasComponent<T>()); m_prefabComponent = value.GetComponent<T>(); }
		}

		[ShowInInspector, ReadOnly] List<T> m_instances = new List<T>(); //this is synechdochal -- each Component is used as a handle to its unique GameObject
		[ShowInInspector, ReadOnly] int m_iter = 0;
		[ShowInInspector, ReadOnly] bool m_calledThisFrame = false;

		//This is just for debugging purposes
		[ShowInInspector, ReadOnly] public int MaxCount = 0;

		void OnDisable()
		{
			Reset();
		}

		IEnumerator ClearAtFrameEndCoroutine()
		{
			yield return new WaitForEndOfFrame();

			bool disableAll = true;
			#if UNITY_EDITOR
				disableAll = !UnityEditor.EditorApplication.isPaused;
			#endif
			
			if (disableAll)
				for (int i = 0; i < m_instances.Count; ++i)
					if (m_instances[i] != null && !m_instances[i].BDIsDestroyed()) 
						m_instances[i].gameObject.SetActive(false);

			MaxCount = Mathf.Max(MaxCount, m_iter);
			m_iter = 0;
			m_calledThisFrame = false;
		}

		T CreateNewInstance(int iter)
		{
			GameObject instance = UHelper.InstantiatePrefabAsChild(PrefabObject.gameObject, this.transform);
			instance.name = PrefabObject.name + " [" + iter + "]";
			return instance.GetComponent<T>();
		}

		public T GetNextInstance()
		{
			while (m_iter >= m_instances.Count)
			{
				m_instances.Add(CreateNewInstance(m_iter));
			}

			int i = m_iter;
			++m_iter;
			if (m_instances[i].BDIsDestroyed())
				m_instances[i] = CreateNewInstance(i);
			m_instances[i].gameObject.SetActive(true);
			
			if (!m_calledThisFrame)
			{
				m_calledThisFrame = true;
				StartCoroutine(ClearAtFrameEndCoroutine());
			}
			
			return m_instances[i];
		}

		public void Reset()
		{
			foreach (T instance in m_instances)
				GameObject.Destroy(instance);
			m_instances.Clear();
		}
	}

}
