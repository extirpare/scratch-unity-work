﻿using System;
//
//Look I pretty much just use attributes to associate a name in XML with a given class
//So this is really convenient for me
//
namespace BD
{
	public class NamedAttribute : Attribute
	{
		public string Name { get; private set; }
		public NamedAttribute(string name) { Name = name; }

		public override string ToString() { return GetType().ToString() + ": Name '" + Name + "'"; }
	}

	//URI isn't really the correct term but anyhow I'm using it for menu location
	//so like Attribute "LoopingCameraMovement" would be under "Camera" > "Movements" > "Looping"
	public class NamedURIedAttribute : NamedAttribute
	{
		public string URI { get; private set; }
		public NamedURIedAttribute(string name, string uri) : base(name) { URI = uri; }

		public override string ToString() { return GetType().ToString() + ": Name '" + Name + "' URI '" + URI + "'"; }
	}
}
