﻿using System;

namespace BD
{
	public interface ITyped<eType>
		where eType : struct, IConvertible, IComparable, IFormattable //is an enum
	{
		eType Subtype { get; }
	}

	/*
	Hey! Every inheritor of ITyped<Type> should also have the following Attribute:

	public class MyTypedAttribute : BD.NamedAttribute
	{
		public MyTypedAttribute(string name) : base(name) { }
	}

	And the name passed in to that attribute should == name of the Subtype it is!
	That makes everyone's lives much easier!
	*/
}
