﻿using UnityEngine;
using System;

namespace BD
{
	//Slider can never go above max value, and max value must be > 0
	[Serializable]
	public class Slider
	{
		[SerializeField] float m_currentValue = 0;
		[SerializeField] float m_maxValue = 1;
		public float CurrentValue { get { return m_currentValue; } set { m_currentValue = BD.Maths.Clamp(value, 0, m_maxValue); } }
		public float MaxValue { get { return m_maxValue; } set { m_maxValue = Mathf.Max(BD.Maths.Epsilon, value); } }

		public float Normalized { get { return CurrentValue / MaxValue; } set { CurrentValue = BD.Maths.Saturate(value) * MaxValue; } }

		public void ToMaxValue() { CurrentValue = MaxValue; }
		public void ToZero() { CurrentValue = 0; }

		public bool AtZero() { return BD.Maths.NearEqual(CurrentValue, 0); }
		public bool AtMax() { return BD.Maths.NearEqual(CurrentValue, MaxValue); }

		public override string ToString()
		{
			return "Slider, at " + CurrentValue + " / " + MaxValue + ", norm " + Normalized;
		}
	}
}
