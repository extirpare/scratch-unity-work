﻿using System;
using System.Xml;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace BD
{
	[Serializable]
	public class FlowList<TAction, TActionAttribute, TTypeEnum> : IXmlable
		where TAction : class, ITyped<TTypeEnum>, IXmlable
		where TActionAttribute : NamedAttribute
		where TTypeEnum : struct, IConvertible, IComparable, IFormattable //is an enum
	{
		[SerializeField, ListDrawerSettings(ShowIndexLabels = true)] List<TAction> m_actions = new List<TAction>();
		[ShowInInspector, ReadOnly] int m_currentIndex = -1;

		public List<TAction> Actions { get { return m_actions; } }
		public int CurrentIndex { get { return m_currentIndex; } set { m_currentIndex = value; } }

		public TAction CurrentAction { get { return !IsValid() ? null : m_actions[CurrentIndex]; } }

		public bool IsValid()
		{
			return m_actions.Count > 0 && BD.Maths.ValidIndex(m_currentIndex, m_actions.Count);
		}

		public bool IsCompleted()
		{
			return m_actions.Count == 0 || m_currentIndex >= m_actions.Count;
		}

		public void WriteToXml(XmlElement parent)
		{
			parent.BDWriteCollection<TAction, TActionAttribute>(Actions);
		}

		public void ReadFromXml(XmlElement parent)
		{
			parent.BDReadCollection<TAction, TActionAttribute>(Actions);
		}

		public void Reset()
		{
			m_actions.Clear();
			m_currentIndex = -1;
		}

		public void ToBeginning()
		{
			m_currentIndex = 0;
		}

		public void ToPrevious()
		{
			m_currentIndex--;
		}

		public void ToNext()
		{
			m_currentIndex++;
		}

		public void Insert(int index, TAction action)
		{
			m_actions.Insert(index, action);
		}

		public void RemoveAt(int index)
		{
			m_actions.RemoveAt(index);
		}

		public TAction UpNext()
		{
			if (BD.Maths.ValidIndex(m_currentIndex + 1, Actions.Count))
				return m_actions[m_currentIndex + 1];
			return null;
		}

		public void InsertUpNext(TAction action)
		{
			m_actions.Insert(Math.Min(m_currentIndex + 1, Actions.Count), action);
		}

		public void RemoveUpNext()
		{
			m_actions.RemoveAt(m_currentIndex + 1);
		}

		public void InsertUpNext(IEnumerable<TAction> actions)
		{
			m_actions.InsertRange(Math.Min(m_currentIndex + 1, Actions.Count), actions);
		}

		public void RemoveAllFollowing()
		{
			if (m_actions.Count > m_currentIndex + 1)
				m_actions.RemoveRange(m_currentIndex + 1, Actions.Count - m_currentIndex - 1);
		}

		public void Add(TAction action)
		{
			m_actions.Add(action);
		}

		public void AddRange(IEnumerable<TAction> actions)
		{
			m_actions.AddRange(actions);
		}
	}
}
