﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class DictionaryContainer
{
	[OdinSerialize] public Dictionary<string, float> MyDictionary = new Dictionary<string, float>();
}

public class Wrapper : SerializedScriptableObject
{
	[OdinSerialize] public List<DictionaryContainer> Containers = new List<DictionaryContainer>();
}

public class TestSerialize : SerializedMonoBehaviour
{
	public List<int> Test;
	[OdinSerialize] public Wrapper MyWrapper;
	void OnEnable()
	{
		MyWrapper = ScriptableObject.CreateInstance<Wrapper>();
	}
}
