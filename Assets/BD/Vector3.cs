﻿using System;
using System.Xml;

//Vector3 is for any three floats (i.e. three points in-world) and has no restrictions
namespace BD
{
	[Xmlable("Vector3")]
	[Serializable] //Unity might use this to serialize but mostly we use it to expose to inspector
	public struct Vector3
	{
        public float X;
        public float Y;
        public float Z;

        public Vector3(float splat) { this.X = splat; this.Y = splat; this.Z = splat; }
        public Vector3(float x, float y, float z) { this.X = x; this.Y = y; this.Z = z; }
		public Vector3(Vector2 p, float z) { this.X = p.X; this.Y = p.Y; this.Z = z; }
		public Vector3(Vector3 clone) { this.X = clone.X; this.Y = clone.Y; this.Z = clone.Z; }
		public static implicit operator Vector3(Vector2 p) { return new Vector3(p.X, p.Y, 0); }
		public static implicit operator Vector2(Vector3 p) { return new Vector2(p.X, p.Y); }
		public static implicit operator Vector3(UnityEngine.Vector2 v) { return new Vector3(v.x, v.y, 0); }
		public static implicit operator UnityEngine.Vector2(Vector3 v) { return new UnityEngine.Vector2(v.X, v.Y); }
		public static implicit operator Vector3(UnityEngine.Vector3 v) { return new Vector3(v.x, v.y, v.z); }
        public static implicit operator UnityEngine.Vector3(Vector3 v) { return new UnityEngine.Vector3(v.X, v.Y, v.Z); }

		public static Vector3 Zero { get { return new Vector3(0, 0, 0); } }
		public static Vector3 One { get { return new Vector3(1, 1, 1); } }
		public static Vector3 None { get { return new Vector3(-1, -1, -1); } }
		public static Vector3 Right { get { return new Vector3(1, 0, 0); } }
		public static Vector3 Left { get { return new Vector3(-1, 0, 0); } }
		public static Vector3 Up { get { return new Vector3(0, 1, 0); } }
		public static Vector3 Down { get { return new Vector3(0, -1, 0); } }
		public static Vector3 Out { get { return new Vector3(0, 0, 1); } }
		public static Vector3 In { get { return new Vector3(0, 0, -1); } }
		public static Vector3 AlongAxis(eAxis axis)
		{
			switch (axis)
			{
				case eAxis.X: return Vector3.Right;
				case eAxis.Y: return Vector3.Up;
				case eAxis.Z: return Vector3.Out;
				default: throw new Exception();
			}
		}


		//this is the unary operator, like "x = -myPoint"
		public static Vector3 operator -(Vector3 val) { return new Vector3(-val.X, -val.Y, -val.Z); }

        //Arithmetic...
        public static Vector3 operator +(Vector3 a, Vector3 b) { return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z); }
        public static Vector3 operator -(Vector3 a, Vector3 b) { return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z); }
        public static Vector3 operator *(Vector3 a, Vector3 b) { return new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z); }
        public static Vector3 operator /(Vector3 a, Vector3 b) { return new Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z); }

        public static Vector3 operator *(float val, Vector3 a) { return new Vector3(a.X * val, a.Y * val, a.Z * val); }
        public static Vector3 operator /(float val, Vector3 a) { return new Vector3(a.X / val, a.Y / val, a.Z / val); }
        public static Vector3 operator *(Vector3 a, float val) { return new Vector3(a.X * val, a.Y * val, a.Z * val); }
        public static Vector3 operator /(Vector3 a, float val) { return new Vector3(a.X / val, a.Y / val, a.Z / val); }

        //We don't need to overload assignemnt operators like *=, they automatically pick up the overloaded *, that is cool C#

        public static bool operator ==(Vector3 lhs, Vector3 rhs)
        {
            return BD.Maths.NearEqual(lhs.X, rhs.X)
                && BD.Maths.NearEqual(lhs.Y, rhs.Y)
                && BD.Maths.NearEqual(lhs.Z, rhs.Z);
        }

        public static bool operator !=(Vector3 lhs, Vector3 rhs) { return !(lhs == rhs); }
		public bool NearEqual(Vector3 other) { return Maths.NearEqual(X, other.X) && Maths.NearEqual(Y, other.Y) && Maths.NearEqual(Z, other.Z); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
        public override int GetHashCode() { return base.GetHashCode(); }

        public override string ToString() { return "<<" + X + ", " + Y + ", " + Z + ">>"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y).BDWriteAttr("Z", Z); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref X).BDReadAttr("Y", ref Y).BDReadAttr("Z", ref Z); }

		public Vector3 SetX(float x) { Vector3 ret = this; ret.X = x; return ret; }
		public Vector3 SetY(float y) { Vector3 ret = this; ret.Y = y; return ret; }
		public Vector3 SetZ(float z) { Vector3 ret = this; ret.Z = z; return ret; }

		public float GetAxis(eAxis axis)
		{
			switch (axis)
			{
				case eAxis.X: return X;
				case eAxis.Y: return Y;
				case eAxis.Z: return Z;
				default: throw new Exception();
			}
		}

		public Vector3 SetAxis(eAxis axis, float val)
		{
			Vector3 ret = this;
			switch (axis)
			{
				case eAxis.X: ret.X = val; break;
				case eAxis.Y: ret.Y = val; break;
				case eAxis.Z: ret.Z = val; break;
				default: throw new Exception();
			}

			return ret;
		}

		public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public bool IsNormalized()
        {
            return Maths.NearEqual(Length(), 1f);
        }

        public void Normalize()
        {
			float length = Length();
			if (length > 0)
			{
				X /= length;
				Y /= length;
				Z /= length;
			}
        }

        public NVector3 Normalized { get {
				Vector3 ret = new Vector3(this); ret.Normalize(); return ret;
		} }

        public float DistanceAlong(NVector3 axis) { return Dot(axis); }
        public float Dot(NVector3 other) { return this.X * other.X + this.Y * other.Y + this.Z * other.Z; }

		public static Vector3 Lerp(Vector3 zeroPos, Vector3 onePos, float dist)
		{
			return onePos * dist + zeroPos * (1 - dist);
		}

		public bool LiesOn(NVector3 axis) { return Maths.NearEqual(this.Dot(axis), Length()); }

		public bool IsPerpendicularTo(NVector3 axis) { UnityEngine.Debug.Assert(Length() > 0); return this.Dot(axis) == 0f; }

        public NVector3 GetPerpendicularVector()
        {
			UnityEngine.Debug.Assert(Length() > 0);

            if (LiesOn(NVector3.Up) || LiesOn(NVector3.Down))
                return Normalized.Cross(NVector3.Right);
            return Normalized.Cross(NVector3.Up);
        }

        public Vector3 RoundToInt() { return RoundToNearest(1f); }
        public Vector3 RoundToNearest(float val) //Note this is bad if val is much smaller than one of our vals
        {
            Vector3 tmp = this / val;
            tmp.X = (float)Math.Round(tmp.X, MidpointRounding.AwayFromZero);
            tmp.Y = (float)Math.Round(tmp.Y, MidpointRounding.AwayFromZero);
            tmp.Z = (float)Math.Round(tmp.Z, MidpointRounding.AwayFromZero);
            return tmp * val;
        }
    }
}
