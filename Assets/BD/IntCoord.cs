﻿using System;
using System.Xml;
using System.Collections.Generic;

using UnityEngine;
using System.Collections;

namespace BD
{
	[Xmlable("IntCoord")]
	[Serializable]
	public struct IntCoord : IXmlable
	{
		//Hi! We are holding IntCoord values as shorts, not ints, because they are our #1 source of garbage
		//and this cuts down on GC by half. Please don't have IntCoord values outside of Short.MinValue / Short.MaxValue!
		[SerializeField] short m_x;
		[SerializeField] short m_y;
		
		public int X { get { return m_x; } set { Debug.Assert(value >= short.MinValue && value <= short.MaxValue); m_x = (short)value; } }
		public int Y { get { return m_y; } set { Debug.Assert(value >= short.MinValue && value <= short.MaxValue); m_y = (short)value; } }
		public int Width { get { return X; } set { value = X; } }
		public int Height { get { return Y; } set { value = Y; } }

		public static IntCoord Zero { get { return new IntCoord(0, 0); } }
		public static IntCoord None { get { return new IntCoord(-1, -1); } }
		public static IntCoord One { get { return new IntCoord(1, 1); } }
		public static IntCoord Right { get { return new IntCoord(1, 0); } }
		public static IntCoord Up { get { return new IntCoord(0, 1); } }
		public static IntCoord Left { get { return new IntCoord(-1, 0); } }
		public static IntCoord Down { get { return new IntCoord(0, -1); } }
		public static IntCoord Max { get { return new IntCoord(short.MaxValue, short.MaxValue); } }

		public static IntCoord InDirection(eDirection dir)
		{
			switch (dir)
			{
				case eDirection.Up:
					return IntCoord.Up;
				case eDirection.Right:
					return IntCoord.Right;
				case eDirection.Down:
					return IntCoord.Down;
				case eDirection.Left:
					return IntCoord.Left;
				default:
					throw new System.Exception();
			}
		}

		public IntCoord MovedInDirection(eDirection dir, int amt) { return this + InDirection(dir) * amt; }
		public IntCoord MovedInDirection(eDirection dir) 
		{ 
			switch (dir)
			{
				case eDirection.Up:
					return new IntCoord(X, Y+1);
				case eDirection.Right:
					return new IntCoord(X+1, Y);
				case eDirection.Down:
					return new IntCoord(X, Y-1);
				case eDirection.Left:
					return new IntCoord(X-1, Y);
				default:
					throw new System.Exception();
			}
		}

        public class Enumerable : IEnumerable<IntCoord>
        {
			IntCoord m_min, m_max;
			public Enumerable(IntCoord maxExclusive) { m_min = IntCoord.Zero; m_max = maxExclusive; }
			public Enumerable(IntCoord minInclusive, IntCoord maxExclusive) { m_min = minInclusive; m_max = maxExclusive; }
			public Enumerable(IntRect rectMaxExclusive) { m_min = rectMaxExclusive.TopLeft; m_max = rectMaxExclusive.BottomRight; }

            public IEnumerator<IntCoord> GetEnumerator() { return EnumerateAllValuesBetween(m_min, m_max); }
            IEnumerator IEnumerable.GetEnumerator() { return EnumerateAllValuesBetween(m_min, m_max); }
        }

        //Inclusive of min, exclusive of max
        public static IEnumerator<IntCoord> EnumerateAllValuesBetween(IntCoord min, IntCoord max)
		{
			for (int y = min.Y; y != max.Y; y = BD.Maths.IncrementTowards(y, max.Y))
				for (int x = min.X; x != max.X; x = BD.Maths.IncrementTowards(x, max.X))
					yield return new IntCoord(x, y);
		} 
		public static IEnumerator<IntCoord> ValuesIn(IntCoord dims) { return EnumerateAllValuesBetween(IntCoord.Zero, dims); }

		public IntCoord(int x, int y) 
		{
			Debug.Assert(x >= short.MinValue && x <= short.MaxValue);
			Debug.Assert(y >= short.MinValue && y <= short.MaxValue);
			m_x = (short)x; 
			m_y = (short)y; 
		}
		
		public IntCoord(BD.Vector2 vector) 
		{ 
			Debug.Assert(vector.X >= short.MinValue && vector.X <= short.MaxValue);
			Debug.Assert(vector.Y >= short.MinValue && vector.Y <= short.MaxValue);
			m_x = (short)BD.Maths.Round(vector.X);	
			m_y = (short)BD.Maths.Round(vector.Y);	
		}

		public static bool operator !=(IntCoord lhs, IntCoord rhs) { return !(lhs == rhs); }
		public static bool operator ==(IntCoord lhs, IntCoord rhs) { return lhs.X == rhs.X && lhs.Y == rhs.Y; }
		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public static IntCoord operator +(IntCoord a, IntCoord b) { return new IntCoord(a.X + b.X, a.Y + b.Y); }
		public static IntCoord operator -(IntCoord a, IntCoord b) { return new IntCoord(a.X - b.X, a.Y - b.Y); }
		public static IntCoord operator *(IntCoord a, int b) { return new IntCoord(a.X * b, a.Y * b); }
		public static IntCoord operator /(IntCoord a, int b) { return new IntCoord(a.X / b, a.Y / b); }

		public override string ToString() { return "[" + X + "," + Y + "]"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref m_x).BDReadAttr("Y", ref m_y); }

		public IntCoord SetX(int x) { return new IntCoord(x, this.Y); }
		public IntCoord SetY(int y) { return new IntCoord(this.X, y); }

		public IntCoord IncrementTowards(IntCoord other) { return new IntCoord(Maths.IncrementTowards(X, other.X), Maths.IncrementTowards(Y, other.Y)); }
		public IntCoord IncrementAwayFrom(IntCoord other) { return new IntCoord(Maths.IncrementAwayFrom(X, other.X), Maths.IncrementAwayFrom(Y, other.Y)); }

		public eDirection DirectionTo(IntCoord other)
		{
			IntCoord delta = other - this;
			if (Math.Abs(delta.Y) > Math.Abs(delta.X))
				return (delta.Y > 0) ? eDirection.Up : eDirection.Down;
			else return (delta.X >= 0) ? eDirection.Right : eDirection.Left;
		}

		public int DistanceInDirection(eDirection dir)
		{
			switch (dir)
			{
				case eDirection.Right:
					return this.X;
				case eDirection.Left:
					return -1 * this.X;
				case eDirection.Up:
					return this.Y;
				case eDirection.Down:
					return -1 * this.Y;
				default: throw new System.Exception();
			}
		}

		public static IntCoord FromDirAndLen(eDirection dir, int len)
		{
			switch (dir)
			{
				case eDirection.Right:
					return new IntCoord(len, 0);
				case eDirection.Left:
					return new IntCoord(-len, 0);
				case eDirection.Up:
					return new IntCoord(0, len);
				case eDirection.Down:
					return new IntCoord(0, -len);
				default: throw new System.Exception();
			}
		}

		// This is inclusive -- the result includes this (at beginning of list) and other (at end of list)
		// A *thin* line means only one x per y, or only one y per x. Like this:
		// x				  xx
		//  x		not		   x
		//   x				   xx
		public List<IntCoord> ThinPathTo(IntCoord other)
		{
			List<IntCoord> ret = new List<IntCoord>();
			ret.Add(this);
			IntCoord delta = other - this;
			bool isHorizontal = Math.Abs(delta.X) > Math.Abs(delta.Y);
			
			if (isHorizontal)
			{
				float yPerX = (float)delta.Y / (float)delta.X;
				for (int i = 1; i <= Math.Abs(delta.X); ++i)
				{
					int dx = delta.X > 0 ? i : -i;
					int dy = (int)Maths.Round(dx * yPerX, Maths.eRoundType.ToNearest);
					ret.Add(this + new IntCoord(dx, dy));
				}
			}
			else
			{
				float xPerY = (float)delta.X / (float)delta.Y;
				for (int i = 1; i <= Math.Abs(delta.Y); ++i)
				{
					int dy = delta.Y > 0 ? i : -i;
					int dx = (int)Maths.Round(dy * xPerY, Maths.eRoundType.ToNearest);
					ret.Add(this + new IntCoord(dx, dy));
				}
			}

			return ret;
		}

		// This is inclusive -- the result includes this (at beginning of list) and other (at end of list)
		// A *thick* line means at each step, we give you a list containing the two possible IntCoords you could round to
		// xx				  x
		//  x		not		   x
		//  xx				    x
		public List<List<IntCoord>> ThickPathTo(IntCoord other)
		{
			List<List<IntCoord>> ret = new List<List<IntCoord>>();

			IntCoord delta = other - this;
			Pair<eDirection?, eDirection?> directions = delta.MainDirections();

			if (!directions.First.HasValue)
			{
				//this == other
				List<IntCoord> myselfList = new List<IntCoord>();
				myselfList.Add(this);
				ret.Add(myselfList);
			}
			else if (!directions.Second.HasValue)
			{
				//straight line
				eDirection dir = directions.First.Value;
				int len = delta.DistanceInDirection(dir);
				ret.Capacity = len + 1;

				for (int i = 0; i <= len; ++i)
				{
					IntCoord thisCoord = this.MovedInDirection(dir, i);
					List<IntCoord> thisCoordList = new List<IntCoord>();
					thisCoordList.Add(thisCoord);
					ret.Add(thisCoordList);
				}
			}
			else
			{
				//not a straight line
				BD.Vector2 line = new BD.Vector2(other - this);
				BD.Vector2 lineNormalized = line.Normalized;
				float lineLength = line.Length; 
				for (int iterDist = 0; iterDist <= Maths.RoundToInt(lineLength, Maths.eRoundType.MostPositive); ++iterDist)
				{
					float iterLength = Mathf.Clamp((float)iterDist, 0, lineLength);
					BD.Vector2 pos = new BD.Vector2(this) + lineNormalized * iterLength;
					IntCoord lowLow = new IntCoord(Maths.RoundToInt(pos.X, Maths.eRoundType.LeastPositive), Maths.RoundToInt(pos.Y, Maths.eRoundType.LeastPositive));
					IntCoord lowHigh = new IntCoord(Maths.RoundToInt(pos.X, Maths.eRoundType.LeastPositive), Maths.RoundToInt(pos.Y, Maths.eRoundType.MostPositive));
					IntCoord highLow = new IntCoord(Maths.RoundToInt(pos.X, Maths.eRoundType.MostPositive), Maths.RoundToInt(pos.Y, Maths.eRoundType.LeastPositive));
					IntCoord highHigh = new IntCoord(Maths.RoundToInt(pos.X, Maths.eRoundType.MostPositive), Maths.RoundToInt(pos.Y, Maths.eRoundType.MostPositive));

					List<IntCoord> thisDist = new List<IntCoord>();
					thisDist.Add(lowLow);
					if (!thisDist.Contains(lowHigh)) thisDist.Add(lowHigh);
					if (!thisDist.Contains(highLow)) thisDist.Add(highLow);
					if (!thisDist.Contains(highHigh)) thisDist.Add(highHigh);
					ret.Add(thisDist);
				}
			}

			return ret;
		}

		//Pair.First is the longer Direction
		//Pair.Second will be null if we're exactly in one direction (i.e. [3,0] => [right, null])
		//Pair.First will be null if we're [0,0]
		public Pair<eDirection?, eDirection?> MainDirections()
		{
			Pair<eDirection?, eDirection?> ret = new Pair<eDirection?, eDirection?>(null, null);

			if (Math.Abs(this.X) > Math.Abs(this.Y))
			{
				ret.First = (this.X > 0) ? eDirection.Right : eDirection.Left;
				if (this.Y > 0) ret.Second = eDirection.Up; else if (this.Y < 0) ret.Second = eDirection.Down;
			}
			else if (Math.Abs(this.Y) > Math.Abs(this.X))
			{
				ret.First = (this.Y > 0) ? eDirection.Up : eDirection.Down;
				if (this.X > 0) ret.Second = eDirection.Right; else if (this.X < 0) ret.Second = eDirection.Left;
			}
			else if (this.X != 0) //same absolute value
			{
				ret.First = (this.X > 0) ? eDirection.Right : eDirection.Left;
				ret.Second = (this.Y > 0) ? eDirection.Up : eDirection.Down;
			}

			return ret;
		}

		//This is much simpler than CalculateDirections() but does return 'right' for [0,0]
		public eDirection MainDirection()
		{
			if (Math.Abs(Y) > Math.Abs(X))
				return Y > 0 ? eDirection.Up : eDirection.Down;
			else
				return X > 0 ? eDirection.Right : eDirection.Left;
		}

		//Last goofy way of expressing your direction
		//[3,-2] becomes [Right: 3, Left: 0, Up: 0, Down: 2]
		public EnumToVal<eDirection, int> DirectionAmts()
		{
			EnumToVal<eDirection, int> ret = new EnumToVal<eDirection, int>();
			ret[eDirection.Right] = Maths.Max(X, 0);
			ret[eDirection.Left] = Maths.Max(-1 * X, 0);
			ret[eDirection.Up] = Maths.Max(Y, 0);
			ret[eDirection.Down] = Maths.Max(-1 * Y, 0);
			return ret;
		}

		public bool IsAdjacentTo(IntCoord other)
		{
			if (this.X == other.X)
				return this.Y == other.Y + 1 || this.Y == other.Y - 1;
			else if (this.Y == other.Y)
				return this.X == other.X + 1 || this.X == other.X - 1;
			return false;
		}

		//inclusive of min, exclusive of max
		public bool FitsWithin(IntCoord max) { return X >= 0 && Y >= 0 && X < max.X && Y < max.Y; }
		public bool FitsWithin(IntCoord min, IntCoord max) { return FitsWithin(new IntRect(min, max - min)); }
		public bool FitsWithin(IntRect rect) { return rect.Contains(this); }

		//inclusive of min, exclusive of max
		public IntCoord FitWithin(IntCoord max) { return FitWithin(IntCoord.Zero, max); }
		public IntCoord FitWithin(IntCoord min, IntCoord max)
		{
			return new IntCoord(Math.Max(min.X, Math.Min(max.X - 1, X)), Math.Max(min.Y, Math.Min(max.Y - 1, Y)));
		}

		public int DistanceTo(IntCoord other)
		{
			return Math.Abs(this.X - other.X) + Math.Abs(this.Y - other.Y);
		}

		public float Length()
		{
			return Mathf.Sqrt(X * X + Y * Y);
		}
	}
}
