﻿using System;

//Commonplace enums
namespace BD
{
	public enum eDirection
	{
		Right,
		Up,
		Left,
		Down
	}

	public enum eDirection1D
	{
		Right,
		Left
	}

	public enum eRelativeDirection
	{
		SameDirection,
		ToLeft,
		ToRight,
		Behind
	}

	public enum eCorner
	{
		TopRight,
		TopLeft,
		BottomRight,
		BottomLeft
	}

	public enum eDirCorner
	{
		TopLeft,
		CenterLeft,
		BottomLeft,
		TopCenter,
		Center,
		BottomCenter,
		TopRight,
		CenterRight,
		BottomRight
	}

	public enum eAxis
	{
		X,
		Y,
		Z
	}

	public enum eHorizVert
	{
		Horizontal,
		Vertical
	}
	
	public enum ePopupState
	{
		Inactive,
		Active,
		Complete
	}

	public enum eLogicOp
	{
		And,
		Or,
	}

	public enum ePhonetic
	{
		Alpha,
		Bravo,
		Charlie,
		Delta,
		Echo,
		Foxtrot,
		Golf
	}

	public enum eDayOfWeek
	{
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday,
		Sunday
	}

	public enum eOverrideMethod
	{
		Replace,
		Add,
		PercentageMultiply //Adds 100%, so "20" = 1.2x
	}

	public static class Enums
	{
		public static int ApplyOverride(int baseVal, eOverrideMethod overrideMethod, int overrideVal)
		{
			switch (overrideMethod)
			{
				case eOverrideMethod.Replace:
					return overrideVal >= 0 ? overrideVal : baseVal;
				case eOverrideMethod.Add:
					return baseVal + overrideVal;
				case eOverrideMethod.PercentageMultiply:
					return Maths.Round(baseVal * (1 + (float)overrideVal / 100), Maths.eRoundType.MostPositive);
				default:
					throw new Exception();
			}
		}

		public static eDirection OppositeDirection(eDirection dir) { return ApplyRelativeDirection(dir, eRelativeDirection.Behind); }
		public static eDirection ApplyRelativeDirection(eDirection dir, eRelativeDirection relativeDir)
		{
			if (relativeDir == eRelativeDirection.SameDirection)
				return dir;

			switch (dir)
			{
				case eDirection.Right:
					switch (relativeDir)
					{
						case eRelativeDirection.ToLeft:  return eDirection.Up;
						case eRelativeDirection.Behind:  return eDirection.Left;
						case eRelativeDirection.ToRight: return eDirection.Down;
						default: throw new Exception();
					}
				case eDirection.Up:
					switch (relativeDir)
					{
						case eRelativeDirection.ToLeft:  return eDirection.Left;
						case eRelativeDirection.Behind:  return eDirection.Down;
						case eRelativeDirection.ToRight: return eDirection.Right;
						default: throw new Exception();
					}
				case eDirection.Left:
					switch (relativeDir)
					{
						case eRelativeDirection.ToLeft:  return eDirection.Down;
						case eRelativeDirection.Behind:  return eDirection.Right;
						case eRelativeDirection.ToRight: return eDirection.Up;
						default: throw new Exception();
					}
				case eDirection.Down:
					switch (relativeDir)
					{
						case eRelativeDirection.ToLeft:  return eDirection.Right;
						case eRelativeDirection.Behind:  return eDirection.Up;
						case eRelativeDirection.ToRight: return eDirection.Left;
						default: throw new Exception();
					}
				default: throw new Exception();
			}
		}

		public static eRelativeDirection CalcRelativeDirection(eDirection baseDir, eDirection compareDir)
		{
			if (baseDir == compareDir)
				return eRelativeDirection.SameDirection;
			
			switch (baseDir)
			{
				case eDirection.Right:
					switch (compareDir)
					{
						case eDirection.Down: return eRelativeDirection.ToRight;
						case eDirection.Left: return eRelativeDirection.Behind;
						case eDirection.Up:   return eRelativeDirection.ToLeft;
						default: throw new Exception();
					}
				case eDirection.Up:
					switch (compareDir)
					{
						case eDirection.Right: return eRelativeDirection.ToRight;
						case eDirection.Down:  return eRelativeDirection.Behind;
						case eDirection.Left:  return eRelativeDirection.ToLeft;
						default: throw new Exception();
					}
				case eDirection.Left:
					switch (compareDir)
					{
						case eDirection.Up:    return eRelativeDirection.ToRight;
						case eDirection.Right: return eRelativeDirection.Behind;
						case eDirection.Down:  return eRelativeDirection.ToLeft;
						default: throw new Exception();
					}
				case eDirection.Down:
					switch (compareDir)
					{
						case eDirection.Left: return eRelativeDirection.ToRight;
						case eDirection.Up:   return eRelativeDirection.Behind;
						case eDirection.Right: return eRelativeDirection.ToLeft;
						default: throw new Exception();
					}
				default: throw new Exception();
			}
		}

		//First direction is to the left, second direction is to the right
		public static BD.Pair<eDirection, eDirection> GetPerpendicularDirections(eDirection dir)
		{
			return new BD.Pair<eDirection, eDirection>(ApplyRelativeDirection(dir, eRelativeDirection.ToLeft), ApplyRelativeDirection(dir, eRelativeDirection.ToRight));
		}

		public static eDirection GetDirection(BD.IntCoord from, BD.IntCoord to)
		{
			return GetDirection(to - from);
		}
		public static eDirection GetDirection(BD.IntCoord movement)
		{
			return GetDirection(movement, eHorizVert.Horizontal);
		}
		public static eDirection GetDirection(BD.IntCoord movement, eHorizVert tiebreakingDir)
		{
			if (Math.Abs(movement.X) > Math.Abs(movement.Y))
				return movement.X > 0 ? eDirection.Right : eDirection.Left;
			else if (Math.Abs(movement.X) < Math.Abs(movement.Y))
				return movement.Y > 0 ? eDirection.Up : eDirection.Down;
			else
				switch (tiebreakingDir)
				{
					case eHorizVert.Horizontal:
						return movement.X > 0 ? eDirection.Right : eDirection.Left;
					case eHorizVert.Vertical:
						return movement.Y > 0 ? eDirection.Up : eDirection.Down;
					default:
						throw new Exception();
				}
		}

		public static T Min<T>(T left, T right)
			where T : struct, IConvertible, IComparable, IFormattable //enum
		{
			return ToInt(left) <= ToInt(right) ? left : right;
		}

		public static T Max<T>(T left, T right)
			where T : struct, IConvertible, IComparable, IFormattable //enum
		{
			return ToInt(left) >= ToInt(right) ? left : right;
		}

		public static int ToInt<T>(T val)
		{
			//this is goofy phrasing but doesn't allocate hella garbage like Convert.ToInt32
			return (int)(object)val;
		}

		public static uint ToUInt<T>(T val)
		{
			//this is goofy phrasing but doesn't allocate hella garbage like Convert.ToInt32
			return (uint)(object)val;
		}
	}
}
