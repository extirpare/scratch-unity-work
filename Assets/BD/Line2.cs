﻿using System.Xml;

//2D Line Segments

namespace BD
{
	[Xmlable("Line2")]
	public class Line2 : IXmlable
	{
		public Vector2 StartPt;
		public Vector2 EndPt;

		public Line2() { StartPt = Vector2.Zero; EndPt = Vector2.Zero;  }
		public Line2(Vector2 end) { this.StartPt = Vector2.Zero;  this.EndPt = end; }
		public Line2(Vector2 start, Vector2 end) { this.StartPt = start; this.EndPt = end; }

		//We don't need to overload assignemnt operators like *=, they automatically pick up the overloaded *, that is cool C#

		public static bool operator !=(Line2 lhs, Line2 rhs) { return !(lhs == rhs); }
		public static bool operator ==(Line2 lhs, Line2 rhs)
		{
			if (object.ReferenceEquals(lhs, null) || object.ReferenceEquals(rhs, null)) return object.ReferenceEquals(lhs, rhs); //stupid copy/paste for == null checks 
			return lhs.StartPt == rhs.StartPt && lhs.EndPt == rhs.EndPt;
		}

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString() { return "[" + StartPt + "]-[" + EndPt + "]"; }
		public void WriteToXml(XmlElement parent)
		{
			parent.BDCreateChild("StartPt").BDWriteChild(StartPt);
			parent.BDCreateChild("EndPt").BDWriteChild(EndPt);
		}
		public void ReadFromXml(XmlElement parent)
		{
			parent.BDGetChild("StartPt").BDReadChild(ref StartPt);
			parent.BDGetChild("EndPt").BDReadChild(ref EndPt);
		}

		//float length of line
		public float Length { get { return (EndPt - StartPt).Length; } }

		//normalized vector direction of line
		public NVector2 Direction()
		{
			UnityEngine.Debug.Assert(Length > 0);
			Vector2 normalized = (EndPt - StartPt).Normalized;
			return new NVector2(normalized.X, normalized.Y);
		}

		public Vector2 Delta() { return EndPt - StartPt; }

		public Line2 MoveLeft(float val) { return MoveRight(-val); }
		public Line2 MoveRight(float val) { return new Line2(StartPt.MoveRight(val), EndPt.MoveRight(val)); }
		public Line2 MoveUp(float val) { return MoveDown(-val); }
		public Line2 MoveDown(float val) { return new Line2(StartPt.MoveDown(val), EndPt.MoveDown(val)); }

		//returns null if no intersection!
		public Vector2? IntersectionPt(Line2 other)
		{
			//from http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect

			Vector2 thisDistance = this.Delta();
			Vector2 otherDistance = other.Delta();
			Vector2 thisStartToOtherStart = other.StartPt - this.StartPt;

			float thisDistCrossOtherDist = thisDistance.Cross(otherDistance);
			float otherDistCrossThisDist = -thisDistCrossOtherDist; //AxB == -BxA
			if (Maths.NearEqual(thisDistCrossOtherDist, 0))
			{
				//thisDirection is parallel / anti-parallel (aka collinear) to otherDirection
				//This means we can only hit if they're both part of the same line segment
				//anyhow I should solve for that
				return null;
			}

			float thisStartToOtherStartCrossOtherDist = thisStartToOtherStart.Cross(otherDistance);
			float otherStartToThisStartCrossThisDist = (-thisStartToOtherStart).Cross(thisDistance);

			float ratioAlongThisLine = thisStartToOtherStartCrossOtherDist / thisDistCrossOtherDist; //where we'd connect to otherLine if it was infinitely long
			float ratioAlongOtherLine = otherStartToThisStartCrossThisDist / otherDistCrossThisDist; //where otherLine would connect to this line if it was infinitely long
			if (!BD.Maths.IsZeroToOne(ratioAlongThisLine) || !BD.Maths.IsZeroToOne(ratioAlongOtherLine))
			{
				//They'd intersect if they stretched farther!
				return null;
			}

			return StartPt + ratioAlongThisLine * Delta();
		}

		public Vector2? IntersectionPt(Rect rect)
		{
			Vector2? tmp = IntersectionPt(rect.TopLine());
			if (tmp != null) return tmp;
			tmp = IntersectionPt(rect.LeftLine());
			if (tmp != null) return tmp;
			tmp = IntersectionPt(rect.BottomLine());
			if (tmp != null) return tmp;
			tmp = IntersectionPt(rect.RightLine());
			return tmp;
		}

		public bool Intersects(Line2 other) { return IntersectionPt(other) != null; }
		public bool Intersects(Rect rect) { return IntersectionPt(rect) != null; }

		public Line2 StopAt(Line2 other)
		{
			Vector2? intersectionPt = IntersectionPt(other);
			return new Line2(StartPt, intersectionPt == null ? EndPt : intersectionPt.Value);
		}

		public Line2 StopAt(Rect rect)
		{
			Vector2? intersectionPt = IntersectionPt(rect);
			return new Line2(StartPt, intersectionPt == null ? EndPt : intersectionPt.Value);
		}

		//float = distance from passed-in pt to nearest pt on line
		public Pair<float, Vector2> NearestPtOnLine(Vector2 pt)
		{
			Vector2 startToPt = pt - StartPt;

			float dist = startToPt.Dot(Direction());
			Vector2 projected = StartPt + Direction() * dist;

			if (dist < 0)
			{
				float distFromLineStart = -dist;
				distFromLineStart += (pt - projected).Length;
				return new Pair<float, Vector2>(distFromLineStart, StartPt);
			}
			else if (dist > Length)
			{
				float distFromLineEnd = dist - Length;
				distFromLineEnd += (pt - projected).Length;
				return new Pair<float, Vector2>(distFromLineEnd, EndPt);
			}
			else
			{
				return new Pair<float, Vector2>((pt - projected).Length, projected);
			}
		}

		public enum eRelation
		{
			LeftOfLine,
			OnLine,
			RightOfLine
		}
		public eRelation GetRelationToLine(Vector2 pos)
		{
			Vector2 thisNormalized = this.Direction();
			Vector2 toPtNormalized = (pos - this.StartPt).Normalized;

			float cosTheta = thisNormalized.Dot(toPtNormalized);
			if (UnityEngine.Mathf.Abs(cosTheta) > 0.999f) return eRelation.OnLine; //OnLine even if not on the line segment itself
			else
			{
				float sinTheta = thisNormalized.Cross(toPtNormalized);
				return sinTheta < 0 ? eRelation.RightOfLine : eRelation.LeftOfLine;
			}
		}
	}
}
