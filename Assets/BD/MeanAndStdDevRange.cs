﻿using UnityEngine;
using System;

namespace BD
{
	[Serializable]
	public class MeanAndStdDevRange
	{
		public AnimationCurve Mean, OneStdDevUp, OneStdDevDown;
		public bool Bounded;
		public AnimationCurve UpperBound, LowerBound;

		public float SampleAt(float val)
		{
			float ret = BD.Random.RandomGaussian(Mean.Evaluate(val), OneStdDevDown.Evaluate(val), OneStdDevUp.Evaluate(val));
			if (Bounded)
				ret = Maths.Clamp(ret, LowerBound.Evaluate(val), UpperBound.Evaluate(val));
			return ret;
		}
	}
}
