﻿using UnityEngine;
using System;
using System.Xml;

//Hexagons we use are pointy-topped:
//	  =====x=====
//	  ==x=====x==
//	  x=========x
//	  x=========x
//	  x=========x
//	  ==x=====x==
//	  =====x=====

namespace BD
{
	public abstract class Hexagon : IXmlable
	{
		public enum eCorner
		{
			Top,
			TopRight,
			BottomRight,
			Bottom,
			BottomLeft,
			TopLeft
		}

		public enum eSide
		{
			TopRight,
			Right,
			BottomRight,
			BottomLeft,
			Left,
			TopLeft
		}

		public BD.IntCoord Index = BD.IntCoord.None;

		public BD.IntCoord IndexOfNeighbor(eSide side)
		{
			bool isEvenRow = Index.Y % 2 == 0;
			BD.IntCoord delta = BD.IntCoord.Zero;
			switch (side)
			{
				case eSide.TopRight:
					delta = new BD.IntCoord(isEvenRow ? 0 : 1, -1);
					break;
				case eSide.Right:
					delta = new BD.IntCoord(1, 0);
					break;
				case eSide.BottomRight:
					delta = new BD.IntCoord(isEvenRow ? 0 : 1, 1);
					break;
				case eSide.TopLeft:
					delta = new BD.IntCoord(isEvenRow ? -1 : 0, -1);
					break;
				case eSide.Left:
					delta = new BD.IntCoord(-1, 0);
					break;
				case eSide.BottomLeft:
					delta = new BD.IntCoord(isEvenRow ? -1 : 0, 1);
					break;
				default: throw new Exception();
			}
			return Index + delta;
		}

		//"Offset" = rendering offset, normalized by diameter
		//Since we're pointy-topped, ++y = +0.75 offset in y (because we fit to a lower edge), ++x = +sqrt(3)/2 in x
		public static BD.Vector2 IndexToDrawOffset(BD.IntCoord index)
		{
			float y = (float)index.Y * 3 / 4;
			float x = (float)index.X * Mathf.Sqrt(3) / 2;
			if (index.Y % 2 == 1) x += Mathf.Sqrt(3) / 4; //odd-r layout

			return new BD.Vector2(x, y);
		}

		public abstract void WriteToXml(XmlElement parent);
		public abstract void ReadFromXml(XmlElement parent);
	}

} //end namespace BD