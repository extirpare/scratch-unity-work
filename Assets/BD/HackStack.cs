﻿using System;
using System.Collections.Generic;

//
// A HackStack is a stack that you can push / pop / peek, but it's actually built on top of a normal List
// so you can get the value at [4] or set it or whatever. This is useful to me.
// 
// Everything is in array time, pushing and popping is probably O(n) not O(1).
// If you use this for a big enough dataset that this becomes a problem: use something else.
//

namespace BD
{
	public class HackStack<T> : List<T>
	{
		public HackStack() { }
		public HackStack(HackStack<T> other) : base(other) { }
		public HackStack(T val) { Add(val); }

		public T Peek()
		{
			if (Count == 0)
				throw new InvalidOperationException();
			return this[0];
		}
		public T Top() { return Peek(); }

		public T Pop()
		{
			if (Count == 0)
				throw new InvalidOperationException();
			T ret = this[0];
			RemoveAt(0);
			return ret;
		}

		public void Push(T val)
		{
			Insert(0, val);
		}

		public override string ToString() { return "" + Count + " elem" + (Count > 0 ? ", top: '" + this[0].ToString() + "'" : ""); }
	}
}
