﻿namespace BD
{
	public class Plane
	{
		public BD.NVector3 Normal = BD.Vector3.Up;
		public float DistAlongNormal = 0;

		public Plane(BD.NVector3 normal, float distAlongNormal)
		{
			Normal = normal;
			DistAlongNormal = distAlongNormal;
		}

		public Pair<NVector3, NVector3> GetCoordinateVecs()
		{
			Pair<NVector3, NVector3> ret = new Pair<NVector3, NVector3>();
			float cosThetaUp = Normal.Dot(BD.NVector3.Up);
			if (cosThetaUp > 0.95f)
				ret.First = Normal.Cross(BD.NVector3.Right);
			else
				ret.First = Normal.Cross(BD.NVector3.Up);

			ret.Second = Normal.Cross(ret.First);
			return ret;
		}

		public Pair<NVector3, NVector3> GetCoordinateVecs(NVector3 IdealYVec)
		{
			float distAlongNormal = Normal.Dot(IdealYVec);
			Vector3 noNormalYVec = IdealYVec - Normal * distAlongNormal;
			if (noNormalYVec.Length() < 0.01f)
				return GetCoordinateVecs();
			NVector3 yVec = noNormalYVec.Normalized;
			return new Pair<NVector3, NVector3>(Normal.Cross(yVec), yVec);
		}
	}
}
