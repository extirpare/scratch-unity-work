﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(LayoutElement), typeof(ContentSizeFitter))]
public class BUILayoutElementMatcher : MonoBehaviour
{
	public Vector2 Margin = BD.Vector2.Zero;
	public RectTransform ToMatch;

	void Awake() { FitToMatch(); }
	void Update() { FitToMatch(); }
	void LateUpdate() { FitToMatch(); }

	public void FitToMatch()
	{
		LayoutElement myLayoutElement = GetComponent<LayoutElement>();
		myLayoutElement.minWidth = UnityEngine.UI.LayoutUtility.GetMinWidth(ToMatch) + Margin.x;
		myLayoutElement.minHeight = UnityEngine.UI.LayoutUtility.GetMinHeight(ToMatch) + Margin.y;
		myLayoutElement.preferredWidth = UnityEngine.UI.LayoutUtility.GetPreferredWidth(ToMatch) + Margin.x;
		myLayoutElement.preferredHeight = UnityEngine.UI.LayoutUtility.GetPreferredHeight(ToMatch) + Margin.y;
	}
}

