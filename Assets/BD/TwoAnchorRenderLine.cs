﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(SpriteRenderer))]
public class TwoAnchorRenderLine : MonoBehaviour
{
	public Transform StartPt, EndPt;

	public bool LengthCapped;
	public float MinLength, MaxLength;
	public float Width = 0.2f;

	public void Update()
	{
		if (StartPt != null && EndPt != null)
		{
			Vector2 startPt = StartPt.position;
			Vector2 endPt = EndPt.position;

			BD.Vector2 delta = endPt - startPt;
			BD.NVector2 rightVec = (delta.NearEqual(BD.Vector2.Zero)) ? BD.NVector2.Right : ((BD.Vector2)(endPt - startPt)).Normalized;

			this.transform.position = startPt;
			this.transform.rotation = Quaternion.FromToRotation(BD.Vector2.Right, rightVec);

			float spriteWidth = this.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
			float desiredLength = LengthCapped ? BD.Maths.Clamp(delta.Length, MinLength, MaxLength) : delta.Length;

			this.transform.localScale = new BD.Vector2(desiredLength / spriteWidth, Width);
		}
		else
		{
			this.transform.localScale = BD.Vector2.Zero;
		}
	}
}
