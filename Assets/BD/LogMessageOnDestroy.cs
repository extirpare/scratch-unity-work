﻿using UnityEngine;

public class LogMessageOnDestroy : MonoBehaviour 
{
	void OnDestroy()
	{
		Debug.Log("I'm " + this.gameObject.name + " and I'm getting destroyed!");
	}
}
