﻿using System;

namespace BD
{
	[Serializable]
	public class Bezier3D
	{
		static int k_normalizedPathPtCount = 21;

		private Vector3 m_startPt = Vector3.Zero;
		private Vector3 m_endPt = Vector3.Down;
		private Vector3 m_anchorPt = Vector3.Right;
		private Vector3[] m_normalizedPts = new Vector3[k_normalizedPathPtCount];

		public Vector3 StartPt { get { return m_startPt; } set { if (value != m_startPt) { m_startPt = value; RecalculateNormalizedPositions(); } } }
		public Vector3 EndPt { get { return m_endPt; } set { if (value != m_endPt) { m_endPt = value; RecalculateNormalizedPositions(); } } }
		public Vector3 AnchorPt { get { return m_anchorPt; } set { if (value != m_anchorPt) { m_anchorPt = value; RecalculateNormalizedPositions(); } } }
		public Vector3[] PtArray { get { return m_normalizedPts; } }

		public Bezier3D()
		{
			RecalculateNormalizedPositions();
		}

		public Bezier3D(Vector3 startPt, Vector3 endPt)
		{
			StartPt = startPt;
			EndPt = endPt;
			AnchorPt = new Vector3(startPt.X, endPt.Y, endPt.Z);
			RecalculateNormalizedPositions();
		}

		public Bezier3D(Vector2 startPt, Vector2 endPt, Vector2 anchorPt)
		{
			StartPt = startPt;
			EndPt = endPt;
			AnchorPt = anchorPt;
			RecalculateNormalizedPositions();
		}

		public Vector3 PositionAlongCurve(float dist)
		{
			UnityEngine.Debug.Assert(Maths.IsZeroToOne(dist));
			float val = dist * (k_normalizedPathPtCount-1);
			var indexAndDist = Maths.ToIntAndRemainder(val);

			if (indexAndDist.First == k_normalizedPathPtCount - 1)
				return m_normalizedPts[indexAndDist.First];
			else
				return Vector3.Lerp(m_normalizedPts[indexAndDist.First], m_normalizedPts[indexAndDist.First + 1], indexAndDist.Second);
		}

		public float Length()
		{
			float fullDist = 0;
			for (int i = 0; i < k_normalizedPathPtCount - 1; ++i) fullDist += (m_normalizedPts[i + 1] - m_normalizedPts[i]).Length();
			return fullDist;
		}

		void RecalculateNormalizedPositions()
		{
			int bezierCount = k_normalizedPathPtCount * 2;

			//actual bezier pt calculation! everything after this just works on linear approximations
			Vector3[] bezierPts = new Vector3[bezierCount];
			for (int i = 0; i < bezierCount; ++i)
			{
				float normalizedDistance = (float)i / (bezierCount - 1);
				Vector3 fromStart = Vector3.Lerp(StartPt, AnchorPt, normalizedDistance);
				Vector3 toEnd = Vector3.Lerp(AnchorPt, EndPt, normalizedDistance);
				bezierPts[i] = Vector3.Lerp(fromStart, toEnd, normalizedDistance);
			}

			float[] distances = new float[bezierCount - 1];
			for (int i = 0; i < bezierCount - 1; ++i)
				distances[i] = (bezierPts[i + 1] - bezierPts[i]).Length();

			float fullDist = 0;
			foreach (float distance in distances) fullDist += distance;
			float incrementalDist = fullDist / (k_normalizedPathPtCount-1);

			m_normalizedPts[0] = bezierPts[0];
			m_normalizedPts[k_normalizedPathPtCount - 1] = bezierPts[bezierCount - 1];

			//and position-equidistant points!
			for (int i = 1; i < k_normalizedPathPtCount-1; ++i)
			{
				float thisSegmentDist = incrementalDist * i;
				float iterDist = 0;
				int iterBezierPt = 0;

				while ((iterDist + distances[iterBezierPt]) < thisSegmentDist - Maths.Epsilon)
				{
					iterDist += distances[iterBezierPt];
					++iterBezierPt;
				}

				Vector3 zeroVec = bezierPts[iterBezierPt];
				Vector3 oneVec = bezierPts[iterBezierPt+1];
				float distBetween = thisSegmentDist - iterDist;
				float normalized = distBetween / distances[iterBezierPt];
				m_normalizedPts[i] = Vector3.Lerp(zeroVec, oneVec, normalized);
			}
		}
	}
}
