﻿using System;
using UnityEngine;

namespace BD
{
	public static class TypeHelper
	{
		//Is type either == checkType, or a child of / implementor of checkType
		//from http://stackoverflow.com/questions/8868119/find-all-parent-types-both-base-classes-and-interfaces 
		public static bool BDIsOfType(this Type type, Type checkType)
		{
			return checkType.IsInterface ? Array.IndexOf(type.GetInterfaces(), checkType) != -1
					   : type == checkType || type.IsSubclassOf(checkType);
		}

		public static bool BDIsOfType<T>(this Type type)
		{
			return type.BDIsOfType(typeof(T));
		}

		public static bool BDHasAttribute(this Type type, Type checkAttributeType)
		{
			if (type == null || checkAttributeType == null)
				throw new ArgumentNullException();

			return (type.GetCustomAttributes(checkAttributeType, false).Length > 0);
		}

		public static T CreateNewInstanceOf<T>()
		{
			T newInstance;
			if (typeof(T).BDIsOfType(typeof(string)))
				newInstance = (T)Convert.ChangeType("new string", typeof(T));
			else if (typeof(T).BDIsOfType(typeof(ScriptableObject)))
				newInstance = (T)Convert.ChangeType(ScriptableObject.CreateInstance(typeof(T)), typeof(T));
			else if (typeof(T).IsAbstract)
				newInstance = default(T);
			else
				newInstance = (T)Activator.CreateInstance(typeof(T));

			return newInstance;
		}

		public static System.Object CreateNewInstanceOf(Type type)
		{
			System.Object newInstance;
			if (type.BDIsOfType(typeof(string)))
				newInstance = "new string";
			else if (type.BDIsOfType(typeof(ScriptableObject)))
				newInstance = ScriptableObject.CreateInstance(type);
			else if (type.IsAbstract)
				newInstance = null;
			else
				newInstance = Activator.CreateInstance(type);

			return newInstance;
		}
		

		public static ITyped<TType> CreateOfType<TAssocAttribute, TType>(TType type)
			where TAssocAttribute : NamedAttribute
			where TType : struct, IConvertible, IComparable, IFormattable //is an enum
		{
			System.Type classType = AttributeHelper.TryAndGetTypeWithNamedAttribute<TAssocAttribute>(type.ToString());
			if (classType == null)
				return null;
			if (!classType.BDIsOfType(typeof(ITyped<TType>)))
				return null;

			return CreateNewInstanceOf(classType) as ITyped<TType>;
		}
	}
}
