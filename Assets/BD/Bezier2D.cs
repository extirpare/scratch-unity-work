﻿using System;

namespace BD
{
	[Serializable]
	public class Bezier2D
	{
		public Vector2 StartPt;
		public Vector2 EndPt;
		public Vector2 AnchorPt;

		public Bezier2D()
		{
			StartPt = Vector2.Zero;
			EndPt = Vector2.Zero;
			AnchorPt = Vector2.Zero;
		}

		public Bezier2D(Vector2 startPt, Vector2 endPt)
		{
			StartPt = startPt;
			EndPt = endPt;
			AnchorPt = new Vector2(startPt.X, endPt.Y);
		}

		public Bezier2D(Vector2 startPt, Vector2 endPt, Vector2 anchorPt)
		{
			StartPt = startPt;
			EndPt = endPt;
			AnchorPt = anchorPt;
		}

		public Vector2 PositionAlongCurve(float normalizedDistance)
		{
			UnityEngine.Debug.Assert(Maths.IsZeroToOne(normalizedDistance));

			Vector2 fromStart = Vector2.Lerp(StartPt, AnchorPt, normalizedDistance);
			Vector2 toEnd = Vector2.Lerp(AnchorPt, EndPt, normalizedDistance);
			return Vector2.Lerp(fromStart, toEnd, normalizedDistance);
		}
	}
}
