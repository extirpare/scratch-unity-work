﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

// At Start() time, UTilePathPool creates a UGameObjectPool for each distinct PathPrefab on its GameObject

namespace BD
{
	public class UTilePathPool : SerializedMonoBehaviour
	{
		public EnumToVal<eDirection, EnumToVal<eDirection, GameObject>> PathPrefabs = new EnumToVal<eDirection, EnumToVal<eDirection, GameObject>>();
		public EnumToVal<eDirection, GameObject> PathEndPrefabs = new EnumToVal<eDirection, GameObject>();
		public GameObject NoMovementPrefab = null;

		Dictionary<GameObject, USpriteRendererPool> m_gameObjectPools = new Dictionary<GameObject, USpriteRendererPool>();

		void Start()
		{
			TryAndAddPool(NoMovementPrefab);

			foreach (BD.eDirection fromDir in EnumHelper.GetValuesIn<eDirection>())
			{
				TryAndAddPool(PathEndPrefabs[fromDir]);

				foreach (BD.eDirection toDir in EnumHelper.GetValuesIn<eDirection>())
					TryAndAddPool(PathPrefabs[fromDir][toDir]);
			}
		}

		void TryAndAddPool(GameObject poolPrefab)
		{
			if (poolPrefab != null && !m_gameObjectPools.ContainsKey(poolPrefab))
			{
				USpriteRendererPool newPool = this.gameObject.AddComponent<USpriteRendererPool>();
				newPool.PrefabObject = poolPrefab;
				m_gameObjectPools[poolPrefab] = newPool;
			}
		}

		public GameObject GetNextPathInstance(eDirection fromDir, eDirection toDir)
		{
			Debug.Assert(fromDir != toDir);
			return m_gameObjectPools[PathPrefabs[fromDir][toDir]].GetNextInstance().gameObject;
		}

		public GameObject GetPathEndInstance(eDirection fromDir)
		{
			return m_gameObjectPools[PathEndPrefabs[fromDir]].GetNextInstance().gameObject;
		}

		public GameObject GetNoMovementInstance()
		{
			return m_gameObjectPools[NoMovementPrefab].GetNextInstance().gameObject;
		}
	}
}
