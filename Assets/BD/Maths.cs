﻿using System;

//Look really this is just for scalar math

namespace BD
{
	public static class Maths
	{
		public const float Epsilon = 0.0005f;
		public const float Pi = 3.141592654f;
		public const float TwoPi = 2 * Pi;
		public const float DegreesToRadians = Pi / 180;
		public const float RadiansToDegrees = 180 / Pi;
		public const float SqrtTwo = 1.4142136f;
		public const float SqrtTwoOverTwo = SqrtTwo / 2f;

		public enum eRoundType
		{
			ToNearest,
			MostPositive,
			LeastPositive,
			TowardsZero, 
			AwayFromZero
		}

		public static bool NearEqual(float left, float right) { return NearEqual(left, right, Epsilon); }

		public static bool NearEqual(float left, float right, float epsilon)
		{
			return Math.Abs(right-left) <= epsilon;
		}

		public static bool IsZeroToOne(float val)
		{
			return val >= 0 && val <= 1;
		}

		public static bool ValidIndex(int val, int arrLen)
		{
			return val >= 0 && val < arrLen;
		}

		public static int WithinIndex(int val, int arrLen)
		{
			return Math.Max(0, Math.Min(arrLen - 1, val));
		}

		public static bool ValidByte(int val)
		{
			return ValidIndex(val, 256);
		}

		public static int IncrementTowards(int toIncrement, int finalVal)
		{
			if (toIncrement < finalVal)
				return ++toIncrement;
			else if (toIncrement > finalVal)
				return --toIncrement;
			else return toIncrement;
		}

		public static int IncrementAwayFrom(int toIncrement, int finalVal)
		{
			if (toIncrement < finalVal)
				return --toIncrement;
			else
				return ++toIncrement;
		}

		public static float Lerp(float start, float end, float dist)
		{
			return dist*end + (1-dist)*start;
		}

		public static float Normalize(float val, float zeroAmt, float oneAmt)
		{
			return (val - zeroAmt) / (oneAmt - zeroAmt);
		}

		public static float Saturate(float val)
		{
			return UnityEngine.Mathf.Max(0f, UnityEngine.Mathf.Min(1f, val));
		}

		public static float NormalizeAndSaturate(float val, float zeroAmt, float oneAmt)
		{
			return Saturate(Normalize(val, zeroAmt, oneAmt));
		}

		public static NVector3 Slerp(NVector3 start, NVector3 end, float normalizedDist)
		{
			throw new System.NotImplementedException();
		}

		public static NVector2 Slerp(NVector2 start, NVector2 end, float normalizedDist)
		{
			throw new System.NotImplementedException();
		}

		//0 = round to int, 1 = to tenths place, -1 = to tens place, etc.
		public static float ToDecimalPlace(float val, int decimalPlaces)
		{
			return RoundToNearest(val, (float)Math.Pow(10, -decimalPlaces));
		}

		//Hey if you feel like you need a ToSigFig function then remember that Log(val, 10) 
		//will list how many powers of 10 it goes out to

		public static int RoundToInt(float val) { return (int)RoundToNearest(val, 1f); }
		public static int RoundToInt(float val, eRoundType roundType) { return (int)RoundToNearest(val, 1f, roundType); }
		public static int Round(float val) { return (int)RoundToNearest(val, 1f, eRoundType.ToNearest); }
		public static int Round(float val, eRoundType roundType) { return (int)RoundToNearest(val, 1f, roundType); }
		public static float RoundToNearest(float val, float roundVal) { return RoundToNearest(val, roundVal, eRoundType.ToNearest); }
		public static float RoundToNearest(float val, float roundVal, eRoundType roundType)
		{
			float valDiv = val / roundVal;
			Pair<int, float> split = ToIntAndRemainder(valDiv);
			if (split.Second < Epsilon)
			{
				//do nothing, since we'll return split.first * roundVal alone later
			}
			else if (split.Second > 1f - Epsilon)
			{
				split.First++;
			}
			else
			{
				switch (roundType)
				{
					case eRoundType.ToNearest:
						if (split.Second > 0.5f) ++split.First;
						break;
					case eRoundType.MostPositive:
						++split.First; //we didn't succeed < Epsilon above so we can assume this
						break;
					case eRoundType.LeastPositive:
						if (split.First < 0) --split.First;
						break;
					case eRoundType.TowardsZero:
						//do nothing, this is the default behavior of ToIntAndRemainder
						break;
					case eRoundType.AwayFromZero:
						if (split.First > 0) ++split.First; else --split.First;
						break;
				}
			}
			return split.First * roundVal;
		}

		public static Pair<int, float> ToIntAndRemainder(float val)
		{
			Pair<int, float> ret = new Pair<int, float>(0, 0);
			ret.First = (int)Math.Floor(val);
			ret.Second = val - ret.First;
			return ret;
		}

		public static Pair<int, float> ToCountAndRemainder(float val, float countBy)
		{
			Pair<int, float> ret = new Pair<int, float>(0, 0);
			ret.First = (int)RoundToNearest(val / countBy, 1, eRoundType.TowardsZero);
			ret.Second = val - (countBy * ret.First);
			return ret;
		}

		public static int CountInstancesBetween(float startPt, float endPt, float instanceLength)
		{
			UnityEngine.Debug.Assert(startPt <= endPt);
			UnityEngine.Debug.Assert(instanceLength > 0);
			float startNormalized = startPt / instanceLength;
			float endNormalized = endPt / instanceLength;

			var startSplit = ToIntAndRemainder(startNormalized);
			var endSplit = ToIntAndRemainder(endNormalized);
			return endSplit.First - startSplit.First;
		}

		public static float MoveTowards(float toMove, float toMoveTowards, float maxDelta)
		{
			maxDelta = Math.Abs(maxDelta);

			if (toMove < toMoveTowards)
				return Math.Min(toMove + maxDelta, toMoveTowards);
			else
				return Math.Max(toMove - maxDelta, toMoveTowards);
		}

		public static Vector2? IntersectionBetweenVectors(BD.Vector2 pt1, BD.NVector2 dir1, BD.Vector2 pt2, BD.NVector2 dir2)
		{
			if (dir1.Dot(dir2) > 0.9999f) return null;

			//this makes no sense to me but apparently works
			BD.Vector2 c = pt2 - pt1;
			BD.Vector2 cPerp = new BD.Vector2(-c.Y, c.X);
			BD.Vector2 dir1Perp = new BD.NVector2(-dir1.Y, dir1.X);

			float cPerpDotBdir = cPerp.Dot(dir2);
			float adirPerpDotBdir = dir1Perp.Dot(dir2);

			float distAlongPt1 = cPerpDotBdir / adirPerpDotBdir;
			if (distAlongPt1 > 0)
				return pt1 + dir1 * distAlongPt1;
			else
				return null;
		}

		//Returns whether a camera sitting at lineStart, looking towards lineEnd, would see pt as being to the left or right of center
		public static eDirection1D CalcSideOfLine(BD.Vector2 lineStart, BD.Vector2 lineEnd, BD.Vector2 pt)
		{
			BD.Vector2 toEnd = lineEnd - lineStart;
			BD.Vector2 toPt = pt - lineStart;
			float crossProductLength = toEnd.Cross(toPt);
			return crossProductLength >= 0 ? eDirection1D.Right : eDirection1D.Left;
		}

		public static float Clamp(float val, float min, float max)
		{
			return val < min ? min : val > max ? max : val;
		}

		public static int Clamp(int val, int min, int max)
		{
			return val < min ? min : val > max ? max : val;
		}

		public static float Max(float valA, float valB)
		{
			return valA > valB ? valA : valB;
		}

		public static int Max(int valA, int valB)
		{
			return valA > valB ? valA : valB;
		}

		public static float Min(float valA, float valB)
		{
			return valA < valB ? valA : valB;
		}

		public static int Min(int valA, int valB)
		{
			return valA < valB ? valA : valB;
		}

		public static BD.Pair<BD.Vector2, BD.Vector2> CenterAndExtentsToBounds(BD.Vector2 center, BD.Vector2 extents)
		{
			return new BD.Pair<BD.Vector2, BD.Vector2>(new BD.Vector2(center.X - extents.X / 2, center.Y - extents.Y / 2),
													   new BD.Vector2(center.X + extents.X / 2, center.Y + extents.Y / 2));
		}
	}
}
