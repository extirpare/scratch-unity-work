﻿using System;

namespace BD
{
	[Serializable]
	public struct Pair<Type1, Type2>
	{
		public Pair(Type1 t1, Type2 t2) { First = t1; Second = t2; }

		public static bool operator !=(Pair<Type1, Type2> lhs, Pair<Type1, Type2> rhs) { return !(lhs == rhs); }
		public static bool operator ==(Pair<Type1, Type2> lhs, Pair<Type1, Type2> rhs)
		{
			if (object.ReferenceEquals(lhs, null) || object.ReferenceEquals(rhs, null)) return object.ReferenceEquals(lhs, rhs); //stupid copy/paste for == null checks 
			return (lhs.First == null) == (rhs.First == null) && (lhs.First == null || lhs.First.Equals(rhs.First))
				&& (lhs.Second == null) == (rhs.Second == null) && (lhs.Second == null || lhs.Second.Equals(rhs.Second));
		}

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString()
		{
			return "First: " + First.ToString() + " Second: " + Second.ToString();
		}

		public Type1 First;
		public Type2 Second;

		//Look it's reasonable to think in these terms
		public Type1 Key { get { return First; } set { First = value; } }
		public Type2 Value { get { return Second; } set { Second = value; } }
		public Type1 X { get { return First; } set { First = value; } }
		public Type2 Y { get { return Second; } set { Second = value; } }
	}
}
