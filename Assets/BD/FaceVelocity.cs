﻿using BD;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class FaceVelocity : MonoBehaviour 
{
	[ShowInInspector] BD.Vector2 m_prevFramePos;

	void Update()
	{
		BD.Vector2 deltaPos = (BD.Vector2)this.transform.position - m_prevFramePos;
		if (deltaPos.Length < Maths.Epsilon)
			return;
		Quaternion facingQuat = Quaternion.LookRotation(BD.Vector3.Out, deltaPos.Normalized.Rotate(90));
		BD.Vector3 eulerAngles = facingQuat.eulerAngles;
		this.transform.rotation = facingQuat;
		m_prevFramePos = this.transform.position;
	}
}
