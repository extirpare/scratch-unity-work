﻿using UnityEngine;
using System.Collections;

public interface IKeyboardShortcutListener 
{
	void OnKeyPressed(KeyCode key);
}
