﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//todo: once this turns on, it can't turn off

public class MaxHeightClamp : MonoBehaviour 
{
	public int MaxHeight = 0;
	RectTransform m_assocRectTransform;
	LayoutElement m_assocLayoutElement;

	void Start()
	{
		m_assocRectTransform = GetComponent<RectTransform>();
		m_assocLayoutElement = GetComponent<LayoutElement>();
		m_assocLayoutElement.flexibleHeight = 1;
		m_assocLayoutElement.preferredHeight = -1; //disables layoutelement override of preferredheight
	}

	void Update() 
	{
		if (MaxHeight > 0)
		{
			float currentHeight = m_assocRectTransform.rect.height;
			if (currentHeight > MaxHeight)
			{
				m_assocLayoutElement.flexibleHeight = 0;
				m_assocLayoutElement.preferredHeight = MaxHeight;
			}
		}
	}
}
