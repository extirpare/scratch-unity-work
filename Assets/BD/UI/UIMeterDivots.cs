﻿using BD;
using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public struct DivotInfo
{
	[MinValue(1)] public int Spacing;
	[AssetsOnly] public RectTransform DivotPrefab;
}

[RequireComponent(typeof(UIMeter))]
public class UIMeterDivots : SerializedMonoBehaviour 
{
	[ShowInInspector, ReadOnly] UIMeter m_associatedMeter;
	[Tooltip("Larger spacings will be used if possible")] public List<DivotInfo> Divots;
	public RectTransform DivotParent;

	void Awake()
	{
		m_associatedMeter = GetComponent<UIMeter>();
	}

	void Start()
	{
		GenerateDivots();
	}

	[Button]
	public void GenerateDivots()
	{
		DivotParent.BDDeleteAllChildren();

		for (int i = 1; i < m_associatedMeter.MaxFillAmt; ++i)
		{
			DivotInfo? divot = null;

			for (int divotIndex = Divots.Count - 1; divotIndex >= 0; --divotIndex)
				if (i % Divots[divotIndex].Spacing == 0 && (divot == null || Divots[divotIndex].Spacing > divot.Value.Spacing))
					divot = Divots[divotIndex];

			if (divot != null)
			{
				RectTransform divotInstance = UHelper.InstantiateUIPrefabAsChild(divot.Value.DivotPrefab, DivotParent);
				BD.Vector2 anchoredPos = m_associatedMeter.GetAnchoredPosForValue(i);
				switch (m_associatedMeter.FillDirection)
				{
					case eDirection.Left:
					case eDirection.Right:
						divotInstance.anchoredPosition = new BD.Vector2(anchoredPos.X, divotInstance.anchoredPosition.y);
						break;
					default:
						divotInstance.anchoredPosition = new BD.Vector2(divotInstance.anchoredPosition.x, anchoredPos.Y);
						break;
				}
			}
		}
	}
}
