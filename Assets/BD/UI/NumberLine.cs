﻿using UnityEngine;
using BD;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

//A number line is meant to show a number, and labels/descriptions surrounding it..
//i.e. "5 base damage" or "+2 backstab / attack from behind" or "2-4 damage"

[ExecuteInEditMode]
public class NumberLine : SerializedMonoBehaviour
{
	public enum eFlags
	{
		ShowPlusSignIfPositive,
		ReverseModifierColors,
		DoNotColorLabel,
		HideNumber
	}

	public BD.Color AboveZeroColor;
	public BD.Color AtZeroColor;
	public BD.Color BelowZeroColor;

	[Tooltip("Optional, goes into NumberText otherwise")] public TMPro.TextMeshProUGUI PreNumberLabelText;
	[Tooltip("Optional, goes into NumberText otherwise")] public TMPro.TextMeshProUGUI PostNumberLabelText;
	public TMPro.TextMeshProUGUI NumberText;
	public TMPro.TextMeshProUGUI DescriptionText; 

	public FlagsOf<eFlags> Flags = new FlagsOf<eFlags>();

	public int LowNumber, HighNumber;
	public int Number { get { return HighNumber; } set { LowNumber = value; HighNumber = value; } }

	public string PreNumberLabel;
	[Tooltip("Will only be used if not empty and Number == 1")] public string PreNumberLabelSingular;
	public string PostNumberLabel;
	[Tooltip("Will only be used if not empty and Number == 1")] public string PostNumberLabelSingular;

	[Multiline] public string Description;

	void Start() { Update(); }

	string NumToString(int number)
	{
		if (number > 0)
			return (Flags.Test(eFlags.ShowPlusSignIfPositive) ? "+" : "") + number.ToString();
		else if (number < 0)
			return number.ToString(); //the "-" will be included
		else
			return "0";
	}

	[Button]
	public void Update()
	{
		BD.Color color = AtZeroColor;
		if ((Number > 0 && !Flags.Test(eFlags.ReverseModifierColors)) || (Number < 0 && Flags.Test(eFlags.ReverseModifierColors)))
			color = AboveZeroColor;
		else if ((Number > 0 && Flags.Test(eFlags.ReverseModifierColors)) || (Number < 0 && !Flags.Test(eFlags.ReverseModifierColors)))
			color = BelowZeroColor;

		string preNumberLabelText = (LowNumber == HighNumber && HighNumber == 1 && !Strings.IsNullOrEmpty(PreNumberLabelSingular)) 
				? PreNumberLabelSingular 
				: PreNumberLabel;
		string postNumberLabelText = (LowNumber == HighNumber && HighNumber == 1 && !Strings.IsNullOrEmpty(PostNumberLabelSingular)) 
				? PostNumberLabelSingular 
				: PostNumberLabel;

		if (!Flags.Test(eFlags.DoNotColorLabel))
		{
			preNumberLabelText = "<color=#" + color.AsHexTuple() + ">" + preNumberLabelText + "</color>";
			postNumberLabelText = "<color=#" + color.AsHexTuple() + ">" + postNumberLabelText + "</color>";
		}

		string numberText = "";
		if (PreNumberLabelText != null)
			PreNumberLabelText.text = preNumberLabelText;
		else
			numberText += preNumberLabelText;
		
		if (!Flags.Test(eFlags.HideNumber))
		{
			string temp = LowNumber == HighNumber ? NumToString(Number) : NumToString(LowNumber) + "-" + NumToString(HighNumber);
			if (!Flags.Test(eFlags.DoNotColorLabel))
				temp = "<color=#" + color.AsHexTuple() + ">" + temp + "</color> ";
			numberText += temp;
		}

		if (PostNumberLabelText != null)
			PostNumberLabelText.text = postNumberLabelText;
		else
			numberText += postNumberLabelText;

		if (NumberText != null)
			NumberText.text = numberText;
		
		if (DescriptionText != null)
		{
			if (!Strings.IsNullOrEmpty(Description))
				DescriptionText.text = Description;
			else
				DescriptionText.text = "";
		}
	}
}
