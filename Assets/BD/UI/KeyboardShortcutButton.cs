﻿using UnityEngine;
using UnityEngine.EventSystems;
using BD;

public class KeyboardShortcutButton : MonoBehaviour 
{
	public KeyCode ShortcutKey;

	UnityEngine.UI.Button m_associatedButton;
	UnityEngine.UI.Toggle m_associatedToggle;
	IKeyboardShortcutListener m_associatedKeyListener;

	void Awake()
	{
		m_associatedButton = GetComponent<UnityEngine.UI.Button>();
		m_associatedToggle = GetComponent<UnityEngine.UI.Toggle>();
		m_associatedKeyListener = this.gameObject.BDGetInterface<IKeyboardShortcutListener>();
		Debug.Assert(m_associatedButton != null || m_associatedToggle != null || m_associatedKeyListener != null,
			"KeyboardShortcutButton requires a toggle, a button, or an IKeyListener on its GameObject!");
	}
	
	void Update() 
	{
		if (Input.GetKeyDown(ShortcutKey))
		{
			var pointer = new PointerEventData(EventSystem.current);
			ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.pointerEnterHandler);
			ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.pointerDownHandler);

			if (m_associatedButton != null && m_associatedButton.interactable)
				m_associatedButton.onClick.Invoke();
			if (m_associatedToggle != null && m_associatedToggle.interactable)
				m_associatedToggle.isOn = !m_associatedToggle.isOn;
			if (m_associatedKeyListener != null)
				m_associatedKeyListener.OnKeyPressed(ShortcutKey);
		}
		else if (Input.GetKeyUp(ShortcutKey))
		{
			var pointer = new PointerEventData(EventSystem.current);
			ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.pointerUpHandler);
			ExecuteEvents.Execute(this.gameObject, pointer, ExecuteEvents.pointerExitHandler);
		}
	}
}
