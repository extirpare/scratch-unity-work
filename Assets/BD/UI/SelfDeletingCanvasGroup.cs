﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Sirenix.OdinInspector;

//
// Whatever's in this CanvasGroup will be faded out and then deleted after DisplayTime is up
//

[RequireComponent(typeof(CanvasGroup))]
public class SelfDeletingCanvasGroup : MonoBehaviour 
{
	public float DisplayTime;
	[ShowInInspector, ReadOnly] CanvasGroup m_associatedCanvasGroup;

	void Awake()
	{
		m_associatedCanvasGroup = GetComponent<CanvasGroup>();
	}

	void Start()
	{
		m_associatedCanvasGroup.alpha = 1;
		StartCoroutine(WaitThenDeleteCoroutine());
	}

	IEnumerator WaitThenDeleteCoroutine()
	{
		yield return new WaitForSeconds(DisplayTime);
		yield return BD.UHelper.FadeUICanvas(m_associatedCanvasGroup, 0);
		GameObject.Destroy(this.gameObject);
	}
}
