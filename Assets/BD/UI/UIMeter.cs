﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections;

namespace BD
{
	public class UIMeter : MonoBehaviour 
	{
		[SerializeField] IntSlider m_fillSlider = new IntSlider();
		public eDirection FillDirection;
		public bool ControlAnchors = true;
		public RectTransform FullMeter, MeterJuice;
		public TMPro.TextMeshProUGUI LabelText;
		public string Label;

		[ShowInInspector] public int CurrentFillAmt { get { return m_fillSlider.CurrentValue; } set { m_fillSlider.CurrentValue = value; } }
		[ShowInInspector] public int MaxFillAmt { get { return m_fillSlider.MaxValue; } set { m_fillSlider.MaxValue = value; } }
		[ShowInInspector] public float NormalizedFillAmt { get { return m_fillSlider.Normalized; } }

		//animatedFillAmt is a float cuz we animate between actual possible values
		float? m_animatedFillAmt = null;
		[ShowInInspector] public float AnimatedFillAmt { get { return m_animatedFillAmt ?? (float)CurrentFillAmt; } }
		[ShowInInspector] public float NormalizedAnimatedFillAmt { get { return MaxFillAmt > 0 ? AnimatedFillAmt / MaxFillAmt : 1; } }

		[Button]
		void Update()
		{
			switch (FillDirection)
			{
				case eDirection.Left:
					if (ControlAnchors)
					{
						MeterJuice.pivot = new BD.Vector2(1, 0.5f);
						MeterJuice.anchorMin = new BD.Vector2(1, 0);
						MeterJuice.anchorMax = new BD.Vector2(1, 1);
						MeterJuice.sizeDelta = new BD.Vector2(0, MeterJuice.sizeDelta.y);
					}
					MeterJuice.sizeDelta = new BD.Vector2(NormalizedAnimatedFillAmt * FullMeter.rect.width, MeterJuice.sizeDelta.y);
					break;
				case eDirection.Up:
					if (ControlAnchors)
					{
						MeterJuice.pivot = new BD.Vector2(0.5f, 0);
						MeterJuice.anchorMin = new BD.Vector2(0, 0);
						MeterJuice.anchorMax = new BD.Vector2(1, 0);
						MeterJuice.sizeDelta = new BD.Vector2(MeterJuice.sizeDelta.x, 0);
					}
					MeterJuice.sizeDelta = new BD.Vector2(MeterJuice.sizeDelta.x, NormalizedAnimatedFillAmt * FullMeter.rect.height);
					break;
				case eDirection.Right:
					if (ControlAnchors)
					{
						MeterJuice.pivot = new BD.Vector2(0, 0.5f);
						MeterJuice.anchorMin = new BD.Vector2(0, 0);
						MeterJuice.anchorMax = new BD.Vector2(0, 1);
						MeterJuice.sizeDelta = new BD.Vector2(0, MeterJuice.sizeDelta.y);
					}
					MeterJuice.sizeDelta = new BD.Vector2(NormalizedAnimatedFillAmt * FullMeter.rect.width, MeterJuice.sizeDelta.y);
					break;
				case eDirection.Down:
					if (ControlAnchors)
					{
						MeterJuice.pivot = new BD.Vector2(0.5f, 1);
						MeterJuice.anchorMin = new BD.Vector2(0, 1);
						MeterJuice.anchorMax = new BD.Vector2(1, 1);
						MeterJuice.sizeDelta = new BD.Vector2(MeterJuice.sizeDelta.x, 0);
					}
					MeterJuice.sizeDelta = new BD.Vector2(MeterJuice.sizeDelta.x, NormalizedAnimatedFillAmt * FullMeter.rect.height);
					break;
			}

			if (LabelText != null)
			{
				LabelText.text = Maths.RoundToInt(AnimatedFillAmt) + "/" + MaxFillAmt;
				if (!BD.Strings.IsNullOrEmpty(Label))
					LabelText.text += " " + Label;
			}
		}

		public BD.Vector2 GetAnchoredPosForValue(int value)
		{
			value = BD.Maths.Clamp(value, 0, m_fillSlider.MaxValue);
			switch (FillDirection)
			{
				case eDirection.Left:
				case eDirection.Right:
					return new BD.Vector2(FullMeter.rect.width * value / m_fillSlider.MaxValue, MeterJuice.rect.height);
				case eDirection.Up:					
				case eDirection.Down:					
					return new BD.Vector2(MeterJuice.rect.width, FullMeter.rect.height * value / m_fillSlider.MaxValue);
				default: throw new System.Exception();
			}
		}

		public void AnimateFillTo(int newVal) { AnimateFillTo(newVal, 0.5f); }
		public void AnimateFillTo(int newVal, float deltaTime)
		{
			StopAllCoroutines();
			StartCoroutine(AnimateFillCoroutine(newVal, 1f));
		}

		IEnumerator AnimateFillCoroutine(int newVal, float deltaTime)
		{
			int startVal = CurrentFillAmt;
			CurrentFillAmt = newVal;

			m_animatedFillAmt = startVal;
			float startTime = Time.time;
			while (Time.time < startTime + deltaTime)
			{
				m_animatedFillAmt = BD.Maths.Lerp(startVal, newVal, Mathf.InverseLerp(startTime, startTime + deltaTime, Time.time));
				yield return null;
			}

			m_animatedFillAmt = null;
		}
	}
}

