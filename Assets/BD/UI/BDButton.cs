﻿using UnityEngine;

//BDButton is going to be more useful than the default Unity button, over time.

namespace BD
{

	public class BDButton : UnityEngine.UI.Button 
	{
		public bool IsBeingPressed { get { return IsPressed(); } }
	}

}
