﻿using UnityEngine.EventSystems;

//BDStandaloneInputModule is going to be more useful than the default StandaloneInputModule, over time.

public class BDStandaloneInputModule : StandaloneInputModule 
{
	protected override void Start()
	{
		base.Start();
		
		//This is an unfortunate hack to force our EventSystem to initialize correctly
		//when we've been async loaded and another EventSystem briefly existed while being unloaded.
		GetComponent<EventSystem>().enabled = false;
		GetComponent<EventSystem>().enabled = true;
	}
     public PointerEventData GetPointerData()
     {
		 if (m_PointerData.ContainsKey(kMouseLeftId))
	         return m_PointerData[kMouseLeftId];
			else
				return new PointerEventData(EventSystem.current);
     }
}
