﻿using UnityEngine;
using System.Collections.Generic;

public class RandomUIText : MonoBehaviour 
{
	public List<string> TextOptions = new List<string>();
	public TMPro.TextMeshProUGUI TextToReplace;

	void OnEnable()
	{
		int index = BD.Random.NextInt(TextOptions.Count);
		TextToReplace.text = TextOptions[index];
	}
}
