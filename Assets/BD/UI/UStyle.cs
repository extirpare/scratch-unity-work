﻿using System;

namespace BD
{
	public enum eBackgroundType
	{
		None,
		Color,
		Image //will be multiplied by Color (well not yet)
	}

	[Flags]
	public enum eTextFormat
	{
		Bold = 1,
		Italic = 2,
	}

	public class UStyle
	{
		public UStyle(UStyle other) { FontSize = other.FontSize; FontColor = other.FontColor; WordWrap = other.WordWrap; TextFormat = other.TextFormat; Background = other.Background; }
		public UStyle(int size) { FontSize = size; }
		public UStyle(int size, BD.Color color) { FontSize = size; FontColor = color; }
		public UStyle(int size, bool wordWrap) { FontSize = size; WordWrap = wordWrap; }
		public UStyle(int size, BD.Color color, bool wordWrap) { FontSize = size; WordWrap = wordWrap; }

		public int FontSize = 12;
		public BD.Color FontColor = BD.Color.Black;
		public bool WordWrap = true;
		public DirDims Padding = BD.DirDims.Zero;

		public eTextFormat TextFormat = 0;

		//null = no background, color = solid color of that background
		//I really need to create a BD.Texture class.
		public BD.Color? Background = null;

		public static implicit operator UnityEngine.GUIStyle(UStyle val)
		{
			UnityEngine.GUIStyle ret = new UnityEngine.GUIStyle();
			ret.wordWrap = val.WordWrap;
			ret.fontSize = val.FontSize;
			ret.normal.textColor = val.FontColor;
			ret.clipping = UnityEngine.TextClipping.Clip;

			if (val.TextFormat == eTextFormat.Bold)
				ret.fontStyle = UnityEngine.FontStyle.Bold;
			else if (val.TextFormat == eTextFormat.Italic)
				ret.fontStyle = UnityEngine.FontStyle.Italic;
			else if (val.TextFormat == (eTextFormat.Bold | eTextFormat.Italic))
				ret.fontStyle = UnityEngine.FontStyle.BoldAndItalic;

			if (val.Background.HasValue)
			{
				ret.normal.background = BD.BDraw.ColorToTex(val.Background.Value);
			}

			return ret;
		}

		public float CalcWidthNoWordWrap(string text)
		{
			UnityEngine.GUIContent textAsGuiContent = new UnityEngine.GUIContent(text);
			BD.Vector2 ret = ((UnityEngine.GUIStyle)this).CalcSize(textAsGuiContent);
			return ret.Width;
		}

		public float CalcHeight(string text, float width)
		{
			//note that if this style isn't wordwrapped it's just gonna be the font size
			UnityEngine.GUIContent textAsGuiContent = new UnityEngine.GUIContent(text);
			return ((UnityEngine.GUIStyle)this).CalcHeight(textAsGuiContent, width);
		}

		public UStyle SetColor(BD.Color color)
		{
			UStyle ret = new UStyle(this);
			ret.FontColor = color;
			return ret;
		}

		public UStyle SetFontSize(int height)
		{
			UStyle ret = new UStyle(this);
			ret.FontSize = height;
			return ret;
		}



		public static UStyle Title {
			get {
				UStyle ret = new UStyle(20, BD.Color.White, false);
				ret.TextFormat = eTextFormat.Bold;
				//ret.BackgroundType = eBackgroundType.Image;
				//ret.BackgroundTexture = Resources.TryAndLoadTexture("Assets/Editor/Cable/Resources/title-bg.png");
				return ret;
			}
		}

		public static UStyle Comment {
			get {
				UStyle ret = LabelStyle;
				ret.FontColor = BD.Color.Green;
				ret.TextFormat = eTextFormat.Italic;
				return ret;
			}
		}

		public static UStyle LabelStyle {
			get {
				UStyle ret = new UStyle(11);
				return ret;
			}
		}

		public static UStyle BoldLabelStyle {
			get {
				UStyle ret = LabelStyle;
				ret.TextFormat = eTextFormat.Bold;
				return ret;
			}
		}

		public static UStyle SmallLabelStyle {
			get {
				UStyle ret = LabelStyle;
				ret.FontSize = 10;
				return ret;
			}
		}
	}
}
