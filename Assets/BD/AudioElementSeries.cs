﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace BD
{

	// An AudioElementSeries is a bunch of audio clips played in order one after the other.

	[Serializable]
	public class AudioElementSeries
	{
		public List<AudioClip> clips;
		public UnityEngine.Audio.AudioMixerGroup mixerGroup;
		public float volume = 1;
	}
}
