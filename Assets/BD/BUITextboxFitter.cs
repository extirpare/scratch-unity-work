﻿using UnityEngine;



[ExecuteInEditMode]
[RequireComponent(typeof(UnityEngine.UI.LayoutElement), typeof(TMPro.TextMeshProUGUI))]
public class BUITextboxFitter : MonoBehaviour
{
	public float MinWidth = 2; //in world units
	public float FullWidth = 4;

	void Awake() { ResizeTextboxToFit(); }
	void Update() { ResizeTextboxToFit(); }

	public void ResizeTextboxToFit()
	{
		TMPro.TextMeshProUGUI thisText = this.GetComponent<TMPro.TextMeshProUGUI>();
		UnityEngine.UI.LayoutElement thisLayoutElement = this.GetComponent<UnityEngine.UI.LayoutElement>();

		BD.Vector2 idealDims = thisText.GetPreferredValues();

		if (idealDims.Width < FullWidth)
		{
			thisLayoutElement.minWidth = Mathf.Max(MinWidth, idealDims.Width);
		}
		else
		{
			thisLayoutElement.minWidth = FullWidth;
		}
	}
}
