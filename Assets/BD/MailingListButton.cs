﻿using UnityEngine;
using BD;

public class MailingListButton : MonoBehaviour 
{
	public TMPro.TMP_InputField AssociatedInputField;
	public string EmailFileURI;

	public void OnClick()
	{
		string newLine = AssociatedInputField.textComponent.text;
		if (newLine != "")
		{
			string text = FileHelper.GetTextInFile(FileHelper.GetStreamingAssetsFolderURI() + EmailFileURI);	
			text += newLine;
			text += "\n";
			FileHelper.WriteTextToFile(text, FileHelper.GetStreamingAssetsFolderURI() + EmailFileURI);
		}
		AssociatedInputField.text = "";
		AssociatedInputField.textComponent.text = "";
	}
}
