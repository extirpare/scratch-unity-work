﻿using System.Collections.Generic;
using UnityEngine;
using BD;
using Sirenix.OdinInspector;

public class ColorCycler : MonoBehaviour 
{
	public List<BD.Color> Colors;
	public float CycleTime;

	[ShowInInspector, ReadOnly] SpriteRenderer m_assocSpriteRenderer;
	[ShowInInspector, ReadOnly] UnityEngine.UI.Image m_assocImage;

	void Start()
	{
		m_assocSpriteRenderer = GetComponent<SpriteRenderer>();
		m_assocImage = GetComponent<UnityEngine.UI.Image>();
	}

	void Update()
	{
		Pair<int, float> cycleAndRemainder = Maths.ToIntAndRemainder(Time.time / CycleTime);
		int currentIndex = cycleAndRemainder.First % Colors.Count;
		int nextIndex = (currentIndex + 1) % Colors.Count;
		float normalizedDist = cycleAndRemainder.Second;
		normalizedDist = Easing.Hermite(0, 1, normalizedDist);

		BD.Color newColor = BD.Color.Lerp(Colors[currentIndex], Colors[nextIndex], normalizedDist);
		if (m_assocSpriteRenderer != null)
			m_assocSpriteRenderer.color = newColor;
		if (m_assocImage != null)
			m_assocImage.color = newColor;
	}
}
