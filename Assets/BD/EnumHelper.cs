﻿using System;

namespace BD
{
	public static class EnumHelper
	{
		public static int NumValues<T>()
			where T : struct, IConvertible, IComparable, IFormattable
		{
			return Enum.GetValues(typeof(T)).Length;
		}

		public static Array GetValuesIn<T>()
			where T : struct, IConvertible, IComparable, IFormattable
		{
			return Enum.GetValues(typeof(T));
		}

		public static int OrderInEnum<T>(T val)
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = Enum.GetValues(typeof(T));
			for (int i = 0; i < vals.Length; ++i)
				if (((T)vals.GetValue(i)).CompareTo(val) == 0)
					return i;

			return -1;
		}

		public static T NextEnumVal<T>(T val)
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = Enum.GetValues(typeof(T));
			for (int i = 0; i < vals.Length; ++i)
				if (((T)vals.GetValue(i)).CompareTo(val) == 0)
					return (T)vals.GetValue( (i+1) % vals.Length);

			return default(T);
		}

		public static T PrevEnumVal<T>(T val)
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = Enum.GetValues(typeof(T));
			for (int i = 0; i < vals.Length; ++i)
				if (((T)vals.GetValue(i)).CompareTo(val) == 0)
					return (T)vals.GetValue( (i+vals.Length-1) % vals.Length);

			return default(T);
		}

		public static T FirstEnumVal<T>()
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = Enum.GetValues(typeof(T));
			return (T)vals.GetValue(0);
		}

		public static T FinalEnumVal<T>()
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = Enum.GetValues(typeof(T));
			return (T)vals.GetValue(vals.Length - 1);
		}

		public static T? StringToEnum<T>(string str)
			where T : struct, IConvertible, IComparable, IFormattable
		{
			foreach (T enumVal in Enum.GetValues(typeof(T)))
			{
				if (BD.Strings.EzCompare(str, enumVal.ToString()))
					return enumVal;
			}

			return null;
		}

		public static T RandomVal<T>()
			where T : struct, IConvertible, IComparable, IFormattable
		{
			Array vals = GetValuesIn<T>();
			return (T)vals.GetValue(BD.Random.NextInt(0, vals.Length));
		}
	}
}
