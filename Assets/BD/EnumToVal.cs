﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace BD
{
	[Serializable]
	public class EnumToVal<TEnumKey, TValue>
		where TEnumKey : struct, IConvertible, IComparable, IFormattable
	{
		[SerializeField] TValue[] m_values = new TValue[EnumHelper.NumValues<TEnumKey>()]; 

		public EnumToVal() 
		{ 
			for (int i = 0; i < m_values.Length; ++i)
				m_values[i] = default(TValue);
		}

		public EnumToVal(TValue defaultVal)
		{
			for (int i = 0; i < m_values.Length; ++i)
				m_values[i] = defaultVal;	
		}

		public EnumToVal(EnumToVal<TEnumKey, TValue> toCopyFrom)
		{
			CopyFrom(toCopyFrom);
		}
		
		public static bool operator !=(EnumToVal<TEnumKey, TValue> lhs, EnumToVal<TEnumKey, TValue> rhs) { return !(lhs == rhs); }
		public static bool operator ==(EnumToVal<TEnumKey, TValue> lhs, EnumToVal<TEnumKey, TValue> rhs) 
		{ 
			if (object.ReferenceEquals(lhs, null) || object.ReferenceEquals(rhs, null)) return object.ReferenceEquals(lhs, rhs); //stupid copy/paste for == null checks 

			if (lhs.m_values.Length != rhs.m_values.Length)
				return false;
			
			for (int i = 0; i < lhs.m_values.Length; ++i)
				if (!EqualityComparer<TValue>.Default.Equals(lhs.m_values[i], rhs.m_values[i]))
					return false;
			
			return true;
		}

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public void SanityCheck()
		{
			if (m_values.Length != EnumHelper.NumValues<TEnumKey>())
			{
				Debug.LogWarning("EnumToVal<" + typeof(TEnumKey) + ", " + typeof(TValue) + "> sanity check failed! Repairing...");
				TValue[] newValues = new TValue[EnumHelper.NumValues<TEnumKey>()];
				for (int i = 0; i < Math.Min(newValues.Length, m_values.Length); ++i)
					newValues[i] = m_values[i];
				
				m_values = newValues;
			}
		}

		public void CopyFrom<TMaybeDifferentEnumKey>(EnumToVal<TMaybeDifferentEnumKey, TValue> other)
			where TMaybeDifferentEnumKey : struct, IConvertible, IComparable, IFormattable
		{
			Debug.Assert(EnumHelper.NumValues<TMaybeDifferentEnumKey>() == EnumHelper.NumValues<TEnumKey>());
			for (int i = 0; i < m_values.Length; ++i)
				m_values[i] = other.m_values[i];
		}
		
		public TValue this[TEnumKey enumVal]
		{
			get 
			{ 
				int valAsInt = (int)((object)enumVal);
				return BD.Maths.ValidIndex(valAsInt, m_values.Length) 
					? m_values[valAsInt] 
					: default(TValue); 
			}
			set 
			{ 
				int valAsInt = (int)((object)enumVal);
				if (BD.Maths.ValidIndex(valAsInt, m_values.Length)) 
					m_values[valAsInt] = value; 
			}
		}

		public void Fill(TValue val)
		{
			for (int i = 0; i < m_values.Length; ++i) 
				m_values[i] = val;
		}

		public TEnumKey? Find(TValue matchingValue)
		{
			foreach (TEnumKey key in EnumHelper.GetValuesIn<TEnumKey>())
				if (System.Object.ReferenceEquals(this[key], matchingValue))
					return key;

			return null;
		}

		public TEnumKey? Find(Func<TEnumKey, TValue, bool> predicate)
		{
			foreach (TEnumKey key in EnumHelper.GetValuesIn<TEnumKey>())
				if (predicate(key, this[key]))
					return key;

			return null;
		}

		public List<TValue> Values()
		{
			List<TValue> ret = new List<TValue>();
			foreach (TEnumKey key in EnumHelper.GetValuesIn<TEnumKey>())
				if (!System.Object.ReferenceEquals(this[key], null))
					ret.Add(this[key]);
			return ret;
		}
	}
}
