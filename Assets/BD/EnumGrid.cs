﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace BD
{
	//These get a much nicer editor display
	[Serializable]
	public class EnumGrid<TEnumKey> : Grid2D<TEnumKey>, ISerializationCallbackReceiver
			where TEnumKey : struct, IConvertible, IComparable, IFormattable
	{
		[SerializeField] int m_serializedWidth = 1;
		[SerializeField] int m_serializedHeight = 1;
		[SerializeField] List<TEnumKey> m_serializedVals = new List<TEnumKey>();
		public EnumGrid() : base() { }
		public EnumGrid(int width, int height) : base(width, height) { }
		public EnumGrid(IntCoord dims) : base(dims) { }

		public void OnBeforeSerialize()
		{
			m_serializedWidth = this.Dimensions.X;
			m_serializedHeight = this.Dimensions.Y;

			m_serializedVals.Clear();
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
				m_serializedVals.Add(this[iter]); 
		}

		public void OnAfterDeserialize()
		{
			SetDims(new IntCoord(m_serializedWidth, m_serializedHeight));
			TEnumKey defaultVal = (TEnumKey)Enum.GetValues(typeof(TEnumKey)).GetValue(0);

			int i = 0;
			foreach (IntCoord iter in new IntCoord.Enumerable(this.Dimensions))
			{
				this[iter] = i >= m_serializedVals.Count ? defaultVal : m_serializedVals[i];
				++i;
			}
		}

		//picks the largest enum key between this and other, per tile
		public void ApplyOr(EnumGrid<TEnumKey> other)
		{
			Debug.Assert(other.Dimensions == this.Dimensions);
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
				this[index] = Enums.Max(this[index], other[index]);
		}
	}
}

