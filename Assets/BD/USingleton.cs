﻿//USingleton is the Unity Singleton, and it uses Unity's Awake() to generate itself
//Use it on stuff that should be MonoBehaviours, and the non-U Singleton on other stuff.

using Sirenix.OdinInspector;

namespace BD
{
	public class USingleton<T> : SerializedMonoBehaviour
		where T : USingleton<T>
	{
		static USingleton<T> k_instance = null;

		[UnityEngine.ExecuteInEditMode]
		protected virtual void Awake()
		{
			RecordThisInstance();
		}

		protected virtual void OnEnable()
		{
			RecordThisInstance();
		}

		protected virtual void OnDestroy()
		{
			if (k_instance == this)
				k_instance = null;
		}

		void RecordThisInstance()
		{
			if (k_instance == null)
				k_instance = this;
			else if (!object.ReferenceEquals(k_instance, this))
				UnityEngine.Debug.LogError("There are two instances of the singleton component " + GetType().ToString() + ", this is so bad. Objects: "
					+ k_instance.gameObject.name + ", " + this.gameObject.name);
		}

		public static bool IsInstantiated { get { return Instance != null; } }

		public static T Instance { get { 
			#if UNITY_EDITOR
			if (!UnityEditor.EditorApplication.isPlaying)
			{
				//We allow this fallback in editor mode, because it's okay to be slow there and a lot of components don't register correctly. 
				//But no screwing around in game.
				if (k_instance == null)
					k_instance = UnityEngine.Object.FindObjectOfType<T>();
			}
			#endif
			
			return (T)k_instance;
		} }
	}
}
