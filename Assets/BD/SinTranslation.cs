﻿using UnityEngine;
using System.Collections;

//Rotates back and forth using a sine pattern

public class SinTranslation : MonoBehaviour 
{
	public float Speed;
	public BD.Vector3 zeroOffset, oneOffset;
	
	void Update()
	{
		float val = Mathf.Sin(Speed * Time.time);
		val = Mathf.InverseLerp(-1, 1, val);
		this.transform.localPosition = BD.Vector3.Lerp(zeroOffset, oneOffset, val);
	}
}
