﻿using BD;
using UnityEngine;

namespace BD
{
	public class USpriteRendererPool : UGameObjectPool<SpriteRenderer> { }
}
