﻿using UnityEngine;
using BD;
using System;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
#endif

//Just plays the sprite sheet front to back, simple

public class AnimatedSprite : MonoBehaviour 
{
	public float SecondsPerFrame = 0.1f;
	[ShowInInspector, ReadOnly] float m_startTime = -1f;
	float TimeSinceStart { get { return Time.time - m_startTime; } }

	public bool Repeat = true;
	[ShowIf("Repeat")] public float SecondsDelayBeforeRepeat = 1f;
	[ShowIf("Repeat")] public float StdDevDelayBeforeRepeat = 0f;
	[ShowInInspector, ReadOnly] float m_mostRecentRepeatTime = 0;
	[ShowInInspector, ReadOnly] float m_nextRepeatTime = 0;

	public Sprite[] Sprites;

	UnityEngine.UI.Image m_associatedUiImage;
	SpriteRenderer m_associatedSpriteRenderer;

	void Awake()
	{
		m_associatedUiImage = GetComponent<UnityEngine.UI.Image>();
		m_associatedSpriteRenderer = GetComponent<SpriteRenderer>();
	}

	void Start()
	{
		m_startTime = Time.time;
		m_mostRecentRepeatTime = 0;
	}

	void Update() 
	{
		if (Sprites.Length == 0)
			return;
		
		if (Repeat && TimeSinceStart >= m_nextRepeatTime)
		{
			m_mostRecentRepeatTime = TimeSinceStart;
			m_nextRepeatTime = TimeSinceStart + (Sprites.Length * SecondsPerFrame) + SecondsDelayBeforeRepeat + Math.Abs(BD.Random.RandomGaussian(StdDevDelayBeforeRepeat));
		}

		float timeIntoLoop = TimeSinceStart - m_mostRecentRepeatTime;
		int frameNum = (int)(timeIntoLoop / SecondsPerFrame);
		frameNum = BD.Maths.WithinIndex(frameNum, Sprites.Length);
		
		if (m_associatedUiImage != null)
			m_associatedUiImage.sprite = Sprites[frameNum];
		else if (m_associatedSpriteRenderer != null)
			m_associatedSpriteRenderer.sprite = Sprites[frameNum];
		else
			Debug.Assert(false, "AnimatedSprite script can't find an image or sprite renderer to animate!");
	}

#if UNITY_EDITOR
	[SerializeField] Texture2D m_editorConsumeTexture;
	[Button]
	void Editor_ConsumeSprite()
	{
		string path = AssetDatabase.GetAssetPath(m_editorConsumeTexture);
		Sprite[] sprites = AssetDatabase.LoadAllAssetsAtPath(path).OfType<Sprite>().ToArray();
		Debug.Log("Replacing " + gameObject.name + " AnimatedSprite with " + sprites.Count() + " sprites from " + path);
		Sprites = sprites;
	}
#endif
}
