﻿using UnityEngine;
using UnityEditor;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class AudioElementDrawer : OdinValueDrawer<AudioElement>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<AudioElement> entry, GUIContent label)
	{
		if (label != null)
			EditorGUI.LabelField(EditorGUILayout.GetControlRect(), label, BD.UStyle.BoldLabelStyle);

		float oldLabelWidth = EditorGUIUtility.labelWidth;
		EditorGUIUtility.labelWidth = 50;

		entry.Property.Children["clip"].Draw();
		EditorGUILayout.BeginHorizontal();
		entry.Property.Children["mixerGroup"].Draw();
		entry.Property.Children["volume"].Draw();
		EditorGUILayout.EndHorizontal();

		EditorGUIUtility.labelWidth = oldLabelWidth;
	}
}
