﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class Vector2Drawer : OdinValueDrawer<BD.Vector2>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.Vector2> entry, GUIContent label)
	{
		BD.Vector2 val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect xRect = position.AtWidth(position.Width / 2).AtMaxWidth(120);
		BD.Rect yRect = xRect.ScrollRight();

		BD.UEIHelper.PushLabelWidth(20);
		val.X = EditorGUI.FloatField(xRect, "X", val.X);
		val.Y = EditorGUI.FloatField(yRect, "Y", val.Y);
		BD.UEIHelper.PopLabelWidth();

		if (entry.IsEditable)
			entry.SmartValue = val;
	}
}