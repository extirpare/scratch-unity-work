﻿using UnityEngine;
using UnityEditor;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

// Second DrawerPriority == wrapper == this pretty much.
[OdinDrawer]
[DrawerPriority(0, 1, 0)]
public class Wrapper_BoolGridDrawer : OdinValueDrawer<BoolGrid>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BoolGrid> entry, GUIContent label)
	{
		// Draw a box around the property.
		SirenixEditorGUI.BeginBox(label);
		this.CallNextDrawer(entry, null);
		SirenixEditorGUI.EndBox();
	}
}

[OdinDrawer]
[DrawerPriority(0, 0, 1)]
public class BoolGridDrawer : OdinValueDrawer<BoolGrid>
{
	static float k_buttonSize = 20;
	static float k_buttonLeftMargin = 20;

	protected override void DrawPropertyLayout(IPropertyValueEntry<BoolGrid> entry, GUIContent label)
	{
		if (label != null)
			EditorGUILayout.LabelField(label, BD.UStyle.BoldLabelStyle);

		BoolGrid val = entry.SmartValue;
		if (val == null) return;
		
		float oldLabelWidth = EditorGUIUtility.labelWidth;
		EditorGUIUtility.labelWidth = 20;
		
		BD.Rect dimsRow = EditorGUILayout.GetControlRect();
		BD.Rect dimsX = dimsRow.AtMaxWidth(dimsRow.Width / 2).AtMaxWidth(60);
		BD.Rect dimsY = dimsX.ScrollRight();
		IntCoord newDims = val.Dimensions;
		newDims.X = EditorGUI.IntField(dimsX, "X", newDims.X);
		newDims.Y = EditorGUI.IntField(dimsY, "Y", newDims.Y);
		val.SetDims(newDims);

		for (int row = 0; row < newDims.Y; ++row)
		{
			BD.Rect thisRow = EditorGUILayout.GetControlRect(GUILayout.Height(k_buttonSize));
			for (int col = 0; col < newDims.X; ++col)
			{
				BD.Rect pos = thisRow.AtX(k_buttonSize * col + k_buttonLeftMargin).AtWidth(k_buttonSize);
				bool currentEntry = val[new IntCoord(col, row)];
				
				if (GUI.Button(pos, currentEntry ? "Y" : " "))
					val[new IntCoord(col, row)] = !currentEntry;
			}
		}

		EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Fill True", GUILayout.MaxWidth(100)))
			val.Fill(true);
		if (GUILayout.Button("Fill False", GUILayout.MaxWidth(100)))
			val.Fill(false);
		EditorGUILayout.EndHorizontal();

		EditorGUIUtility.labelWidth = oldLabelWidth;
		entry.SmartValue = val;
	}

}
