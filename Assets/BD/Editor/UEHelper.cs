﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public static class UEHelper
{

	public static string GetProjectPath()
	{
		return Application.dataPath;
	}

	public static List<string> GetScenesInBuild()
	{
		List<string> ret = new List<string>();
		foreach (UnityEditor.EditorBuildSettingsScene scene in UnityEditor.EditorBuildSettings.scenes)
		{
			if (scene.enabled)
			{
				string name = scene.path;
				ret.Add(name);
			}
		}

		return ret;
	}

	public static List<string> GetScenesInProject()
	{
		//string projectPath = GetProjectPath();
		//DirectoryInfo dirInfo = new DirectoryInfo(projectPath);
		//FileInfo[] files = new DirectoryInfo(projectPath).GetFiles("*.unity", SearchOption.AllDirectories);

		List<string> ret = new List<string>();
		//foreach (FileInfo fInfo in files)
		//	ret.Add(fInfo.FullName);

		BD.Strings.RemoveSharedPrefixAndSuffix(ret);

		return ret;
	}

	public static bool IsInEditMode()
	{
		return Application.isEditor;
	}

	public static T[] FindAssetsByType<T>() where T : UnityEngine.Object { return FindAssetsByType<T>(""); }

	public static T[] FindAssetsByType<T>(string folderURI) where T : UnityEngine.Object
	{
		List<T> ret = new List<T>();
			
		//Weirdly, AssetDatabase.FindAssets wants a string "t:{type}" for type (you can do "l:{label}" for applied labels)
		string typeAsTagStr = string.Format("t:{0}", typeof(T));
		string[] guids;
		if (!BD.Strings.IsNullOrEmpty(folderURI))
			guids = AssetDatabase.FindAssets(typeAsTagStr);
		else
			guids = AssetDatabase.FindAssets(typeAsTagStr, new string[] { folderURI } );

		foreach (string guid in guids)
		{
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);

			if (asset != null)
				ret.Add(asset);
		}
		return ret.ToArray();
	}	
}
