﻿using UnityEngine;
using UnityEditor;
using System;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

namespace BD
{
	[OdinDrawer]
	[DrawerPriority(0, 0, 1)]
	public class EnumGridDrawer<TEnumKey> : OdinValueDrawer<EnumGrid<TEnumKey>>
		where TEnumKey : struct, IConvertible, IComparable, IFormattable
	{
		static float k_buttonSize = 20;
		static float k_buttonLeftMargin = 20;

		protected override void DrawPropertyLayout(IPropertyValueEntry<EnumGrid<TEnumKey>> entry, GUIContent label)
		{
			EnumGrid<TEnumKey> val = entry.SmartValue;
			if (val == null) return;

			BD.UEIHelper.PushLabelFieldWidth(80, 40);
			BD.Color oldColor = GUI.color;
			
			EditorGUILayout.BeginHorizontal();
			IntCoord newDims = val.Dimensions;
			newDims.X = EditorGUILayout.IntField("X (Cols)", newDims.X);
			newDims.Y = EditorGUILayout.IntField("Y (Rows)", newDims.Y);
			val.SetDims(newDims);
			EditorGUILayout.EndHorizontal();

			for (int row = 0; row < newDims.Y; ++row)
			{
				BD.Rect thisRow = EditorGUILayout.GetControlRect(GUILayout.Height(k_buttonSize));
				for (int col = 0; col < newDims.X; ++col)
				{
					BD.Rect pos = thisRow.AtX(k_buttonSize * col + k_buttonLeftMargin).AtWidth(k_buttonSize);
					TEnumKey currentEntry = val[new IntCoord(col, row)];
					int entryAsInt = EnumHelper.OrderInEnum(currentEntry);
					
					GUI.color = GetColorForEnumIndex(entryAsInt);
					if (GUI.Button(pos, entryAsInt.ToString()))
						val[new IntCoord(col, row)] = EnumHelper.NextEnumVal(currentEntry);
				}
			}

			EditorGUILayout.GetControlRect(); //spacer

			//draw the legend
			
			GUI.Label(EditorGUILayout.GetControlRect(), typeof(TEnumKey).ToString());

			for (int i = 0; i < EnumHelper.NumValues<TEnumKey>(); ++i)
			{
				TEnumKey thisEnum = (TEnumKey)EnumHelper.GetValuesIn<TEnumKey>().GetValue(i);
				BD.Rect thisRow = EditorGUILayout.GetControlRect();
				BD.Rect colorPos = thisRow.AtWidth(20);
				BD.Rect labelPos = thisRow.MoveLeftRight(20);
				GUI.color = GetColorForEnumIndex(i);
				if (GUI.Button(colorPos, i.ToString()))
					val.Fill(thisEnum);
				GUI.Label(labelPos, thisEnum.ToString());
			}

			BD.UEIHelper.PopLabelFieldWidth();
			GUI.color = oldColor;

			entry.SmartValue = val;
		}

		BD.Color GetColorForEnumIndex(int index)
		{
			switch (index)
			{
				case 0:
					return BD.Color.LightGray;
				case 1:
					return BD.Color.LightRed;
				case 2:
					return BD.Color.LightGreen;
				case 3:
					return BD.Color.LightBlue;
				default:
					return BD.Color.Yellow;
			}
		}
	}
}
