﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class TestSerializeDrawer : OdinValueDrawer<TestSerialize>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<TestSerialize> entry, GUIContent label)
	{
		EditorGUILayout.LabelField("TEST2");	
		EditorGUILayout.LabelField("TEST3");	
		entry.Property.Children["MyWrapper"].Draw();
		entry.Property.Children["Test"].Draw();
	}
}
