﻿using UnityEngine;
using UnityEditor;
using System;

namespace BD
{
	public class UDictionaryDrawer<TKey, TValue> : PropertyDrawer
	{
		float CalcBodyHeight(SerializedProperty property)
		{
			// Sanity check our values
			SerializedProperty keyArray = property.FindPropertyRelative("m_serializedKeys");
			SerializedProperty valArray = property.FindPropertyRelative("m_serializedVals");

			if (keyArray == null || valArray == null || keyArray.arraySize != valArray.arraySize)
				return 0;

			// Calculate how tall each element is
			float totalHeight = 0;
			for (int i = 0; i < valArray.arraySize; ++i)
			{
				float keyHeight = UEIHelper.CalcPropertyHeight(keyArray.GetArrayElementAtIndex(i));
				float valHeight = UEIHelper.CalcPropertyHeight(valArray.GetArrayElementAtIndex(i));
				totalHeight += Math.Max(keyHeight, valHeight);
			}

			return totalHeight;
		}

		protected virtual float CalcKeyLength(float rowLength)
		{
			if (typeof(TValue).BDIsOfType(typeof(int)) || typeof(TValue).BDIsOfType(typeof(bool)) || typeof(TValue).BDIsOfType(typeof(float)))
				return 0.7f * rowLength;
			else
				return 0.3f * rowLength;
		}

		public override void OnGUI(UnityEngine.Rect position, SerializedProperty property, GUIContent label)
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

			SerializedProperty keyArray = property.FindPropertyRelative("m_serializedKeys");
			SerializedProperty valArray = property.FindPropertyRelative("m_serializedVals");

			// Draw label
			BD.Rect labelPos = new BD.Rect(position.x, position.y, position.width, UEIHelper.k_rowHeight);
			EditorGUI.LabelField(labelPos, label.text + ": " + keyArray.arraySize + " count", BD.UStyle.BoldLabelStyle);

			// And draw each value
			BD.Rect bodyPos = labelPos.ScrollDown();
			float bodyHeight = CalcBodyHeight(property);
			bodyPos = bodyPos.AtHeight(bodyHeight);
			BD.Rect thisRowPos = new BD.Rect(bodyPos).AtHeight(0);

			if (keyArray == null || valArray == null || keyArray.arraySize != valArray.arraySize)
				return;
			
			int removeIndex = -1;
			for (int i = 0; i < keyArray.arraySize; ++i)
			{
				SerializedProperty keyProperty = keyArray.GetArrayElementAtIndex(i);
				SerializedProperty valProperty = valArray.GetArrayElementAtIndex(i);

				float keyHeight = UEIHelper.CalcPropertyHeight(keyProperty);
				float valHeight = UEIHelper.CalcPropertyHeight(valProperty);
				thisRowPos = thisRowPos.AtHeight(Math.Max(keyHeight, valHeight));

				BD.Rect removeButtonLoc = thisRowPos.AtWidth(UEIHelper.k_rowHeight);
				BD.Rect keyLoc = removeButtonLoc.ScrollRight().AtWidth(CalcKeyLength(thisRowPos.Length));
				BD.Rect valLoc = keyLoc.ScrollRight().AtWidth(thisRowPos.Width - keyLoc.Width - removeButtonLoc.Width).MoveLeftRight(5);

				if (GUI.Button(removeButtonLoc.AtMaxHeight(UEIHelper.k_rowHeight), "-"))
					removeIndex = i;

				EditorGUI.PropertyField(keyLoc, keyProperty, GUIContent.none, true);
				EditorGUI.PropertyField(valLoc, valProperty, GUIContent.none, true);
				
				thisRowPos = thisRowPos.ScrollDown();
			}

			if (removeIndex != -1)
			{
				keyArray.DeleteArrayElementAtIndex(removeIndex);
				valArray.DeleteArrayElementAtIndex(removeIndex);
			}

			//Finally, the '+' button at the end
			BD.Rect finalPlusPos = thisRowPos.AtHeight(UEIHelper.k_rowHeight);
			finalPlusPos.Left = finalPlusPos.Right - 2*UEIHelper.k_rowHeight;
			finalPlusPos.Width = 2*UEIHelper.k_rowHeight;
			if (GUI.Button(finalPlusPos, "+"))
			{
				keyArray.arraySize += 1;
				valArray.arraySize += 1;
			}

			// Set indent back to what it was
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			//additional UEIHelper.k_rowHeight for bold title and bottom '+' button
			return 2*UEIHelper.k_rowHeight + CalcBodyHeight(property) + UEIHelper.k_spacerHeight;
		}
	}
}

