﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class RectDrawer : OdinValueDrawer<BD.Rect>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.Rect> entry, GUIContent label)
	{
		BD.Rect val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect xRect = position.AtWidth(position.Width / 2).AtMaxWidth(160);
		BD.Rect yRect = xRect.ScrollRight();

		position = EditorGUILayout.GetControlRect();
		BD.Rect widthRect = xRect.ScrollDown();
		BD.Rect heightRect = yRect.ScrollDown();

		BD.UEIHelper.PushLabelWidth(30);
		val.X = EditorGUI.FloatField(xRect, "X", val.X);
		val.Y = EditorGUI.FloatField(yRect, "Y", val.Y);
		val.Width = EditorGUI.FloatField(widthRect, "Wid", val.Width);
		val.Height = EditorGUI.FloatField(heightRect, "Hei", val.Height);
		BD.UEIHelper.PopLabelWidth();

		if (entry.IsEditable)
			entry.SmartValue = val;
	}
}