﻿using UnityEngine;
using UnityEditor;

namespace BD
{
	class BDImageImportPostprocessor : AssetPostprocessor
	{
		void OnPreprocessTexture()
		{
			TextureImporter textureImporter = (TextureImporter)assetImporter;
			textureImporter.maxTextureSize = 4096;

			if (FileHelper.HasParentFolderWithName(assetPath, "Plugins") || FileHelper.HasParentFolderWithName(assetPath, "TextMesh Pro"))
				return;

			if (FileHelper.HasParentFolderWithName(assetPath, "Cursors"))
			{
				Debug.Log("BDImageImportPostprocessor:  " + assetPath + " is a Cursor!");
				textureImporter.textureType = TextureImporterType.Cursor;
				textureImporter.crunchedCompression = false;
			}
			else if (FileHelper.HasParentFolderWithName(assetPath, "UI"))
			{
				Debug.Log("BDImageImportPostprocessor:  " + assetPath + " is UI!");
				textureImporter.textureType = TextureImporterType.Sprite;
				textureImporter.mipmapEnabled = false;
				textureImporter.crunchedCompression = true;
				textureImporter.textureCompression = TextureImporterCompression.CompressedHQ;
				textureImporter.compressionQuality = 70;
			}
			else if (FileHelper.HasParentFolderWithName(assetPath, "Props"))
			{
				Debug.Log("BDImageImportPostprocessor:  " + assetPath + " is a prop!");
				textureImporter.textureType = TextureImporterType.Sprite;
				textureImporter.mipmapEnabled = false;
				textureImporter.crunchedCompression = true;
				textureImporter.textureCompression = TextureImporterCompression.CompressedHQ;
				textureImporter.compressionQuality = 70;
			}
			else if (FileHelper.HasParentFolderWithName(assetPath, "Characters"))
			{
				Debug.Log("BDImageImportPostprocessor:  " + assetPath + " is a character!");
				textureImporter.textureType = TextureImporterType.Sprite;
				textureImporter.mipmapEnabled = true;
				textureImporter.crunchedCompression = true;
				textureImporter.textureCompression = TextureImporterCompression.CompressedHQ;
				textureImporter.compressionQuality = 70;

				TextureImporterSettings textureSettings = new TextureImporterSettings();
				textureImporter.ReadTextureSettings(textureSettings);
			
				if (textureSettings.spriteMeshType != SpriteMeshType.FullRect)
				{
					textureSettings.spriteMeshType = SpriteMeshType.FullRect;
					textureImporter.SetTextureSettings(textureSettings);
					textureImporter.SaveAndReimport();
				}
			}
			else if (FileHelper.HasParentFolderWithName(assetPath, "Levels"))
			{
				Debug.Log("BDImageImportPostprocessor:  " + assetPath + " is a level!");
				textureImporter.textureType = TextureImporterType.Sprite;
				textureImporter.mipmapEnabled = false;
				textureImporter.crunchedCompression = true;
				textureImporter.textureCompression = TextureImporterCompression.CompressedHQ;
				textureImporter.compressionQuality = 70;
			}
		}
	}
}