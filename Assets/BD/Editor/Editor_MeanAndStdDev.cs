﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(BD.MeanAndStdDev))]
public class MeanAndStdDevDrawer : PropertyDrawer
{
	// Draw the property inside the given rect
	public override void OnGUI(UnityEngine.Rect position, SerializedProperty property, GUIContent label)
	{
		// Using BeginProperty / EndProperty on the parent property means that prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		float cellWidth = position.width / 3;
		float cellHeight = 16;
		BD.Vector2 cellSize = new Vector2(cellWidth, cellHeight);
		BD.Vector2 topLeft = new BD.Rect(position).TopLeft;

		EditorGUI.LabelField(new BD.Rect(topLeft, cellSize), "Mean");
		EditorGUI.LabelField(new BD.Rect(topLeft.MoveRight(cellWidth), cellSize), "1 dev up");
		EditorGUI.LabelField(new BD.Rect(topLeft.MoveRight(2*cellWidth), cellSize), "1 dev down");

		EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(cellHeight), cellSize), property.FindPropertyRelative("Mean"), GUIContent.none);
		EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(cellHeight).MoveRight(cellWidth), cellSize), property.FindPropertyRelative("OneStdDevUp"), GUIContent.none);
		EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(cellHeight).MoveRight(2*cellWidth), cellSize), property.FindPropertyRelative("OneStdDevDown"), GUIContent.none);

		bool isBounded = property.FindPropertyRelative("Bounded").boolValue;
		if (!isBounded)
		{
			EditorGUI.LabelField(new BD.Rect(topLeft.MoveDown(2*cellHeight), cellSize.SetX(2*cellWidth)), "    Unbounded");
			EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(2*cellHeight), cellSize), property.FindPropertyRelative("Bounded"), GUIContent.none);
		}
		else
		{
			EditorGUI.LabelField(new BD.Rect(topLeft.MoveDown(2 * cellHeight), cellSize), "    Hi/Lo:");
			EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(2 * cellHeight), cellSize), property.FindPropertyRelative("Bounded"), GUIContent.none);
			EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(2 * cellHeight).MoveRight(cellWidth), cellSize), property.FindPropertyRelative("UpperBound"), GUIContent.none);
			EditorGUI.PropertyField(new BD.Rect(topLeft.MoveDown(2 * cellHeight).MoveRight(2 * cellWidth), cellSize), property.FindPropertyRelative("LowerBound"), GUIContent.none);
		}

		float mean = property.FindPropertyRelative("Mean").floatValue;
		float oneStdDevDown = property.FindPropertyRelative("OneStdDevDown").floatValue;
		float oneStdDevUp = property.FindPropertyRelative("OneStdDevUp").floatValue;
		bool clamp = property.FindPropertyRelative("Bounded").boolValue;

		float oneStdDevBelowMean = mean - oneStdDevDown;
		float oneStdDevAboveMean = mean + oneStdDevUp;
		float threeStdDevBelowMean = mean - 3*oneStdDevDown;
		float threeStdDevAboveMean = mean + 3*oneStdDevUp;
		if (clamp)
		{
			float lowerBound = property.FindPropertyRelative("LowerBound").floatValue;
			float upperBound = property.FindPropertyRelative("UpperBound").floatValue;

			oneStdDevBelowMean = BD.Maths.Clamp(oneStdDevBelowMean, lowerBound, upperBound);
			oneStdDevAboveMean = BD.Maths.Clamp(oneStdDevAboveMean, lowerBound, upperBound);
			threeStdDevBelowMean = BD.Maths.Clamp(threeStdDevBelowMean, lowerBound, upperBound);
			threeStdDevAboveMean = BD.Maths.Clamp(threeStdDevAboveMean, lowerBound, upperBound);
		}

		BD.Rect lineOne = new BD.Rect(topLeft.MoveDown(3 * cellHeight), cellSize.SetX(3 * cellWidth));
		BD.Rect lineTwo = lineOne.ScrollDown();

		//G4 = general format (whichever is shorter, fixed-point or scientific notation) at 4 decimal places
		EditorGUI.LabelField(lineOne, "68% in [" + oneStdDevBelowMean.ToString("G4") + ", " + oneStdDevAboveMean.ToString("G4") + "]");
		EditorGUI.LabelField(lineTwo, "99.7% in [" + threeStdDevBelowMean.ToString("G4") + ", " + threeStdDevAboveMean.ToString("G4") + "]");

		EditorGUI.EndProperty();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return Mathf.Max(base.GetPropertyHeight(property, label), 80);
	}
}