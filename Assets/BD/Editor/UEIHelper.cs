﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace BD
{
	public static partial class UEIHelper
	{
		public static float k_rowHeight = 16;
		public static float k_spacerHeight = 5;

		static float k_recordedLabelWidth = -1;
		static float k_recordedFieldWidth = -1;
		static int k_recordedIndentLevel = -1;

		public static void PushLabelWidth(float labelWidth)
		{
			k_recordedIndentLevel = EditorGUI.indentLevel;
			k_recordedLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUI.indentLevel = 0;
			EditorGUIUtility.labelWidth = labelWidth; 
		}

		public static void PopLabelWidth()
		{
			EditorGUI.indentLevel = k_recordedIndentLevel;
			EditorGUIUtility.labelWidth = k_recordedLabelWidth;
		}

		public static void PushLabelFieldWidth(float labelWidth, float fieldWidth)
		{
			k_recordedIndentLevel = EditorGUI.indentLevel;
			k_recordedLabelWidth = EditorGUIUtility.labelWidth;
			k_recordedFieldWidth = EditorGUIUtility.fieldWidth;
			EditorGUI.indentLevel = 0;
			EditorGUIUtility.labelWidth = labelWidth; 
			EditorGUIUtility.fieldWidth = fieldWidth;
		}

		public static void PopLabelFieldWidth()
		{
			EditorGUI.indentLevel = k_recordedIndentLevel;
			EditorGUIUtility.labelWidth = k_recordedLabelWidth;
			EditorGUIUtility.fieldWidth = k_recordedFieldWidth;
		}

		static readonly Dictionary<SerializedPropertyType, Type> SerializedPropertyTypes =
			new Dictionary<SerializedPropertyType, Type>()
			{
				{ SerializedPropertyType.Integer, typeof(int) },
				{ SerializedPropertyType.Boolean, typeof(bool) },
				{ SerializedPropertyType.Float, typeof(float) },
				{ SerializedPropertyType.String, typeof(string) },
				{ SerializedPropertyType.Color, typeof(Color) },
				{ SerializedPropertyType.ObjectReference, typeof(UnityEngine.Object) },
				{ SerializedPropertyType.Enum, typeof(int) },
				{ SerializedPropertyType.Vector2, typeof(UnityEngine.Vector2) },
				{ SerializedPropertyType.Vector3, typeof(UnityEngine.Vector3) },
				{ SerializedPropertyType.Vector4, typeof(UnityEngine.Vector4) },
				{ SerializedPropertyType.Rect, typeof(UnityEngine.Rect) },
				{ SerializedPropertyType.ArraySize, typeof(int) },
				{ SerializedPropertyType.AnimationCurve, typeof(AnimationCurve) },
				{ SerializedPropertyType.Bounds, typeof(UnityEngine.Bounds) },
				{ SerializedPropertyType.Quaternion, typeof(UnityEngine.Quaternion) },
			};

		static readonly Dictionary<SerializedPropertyType, Func<SerializedProperty, object>> SerializedPropertyGetters =
			new Dictionary<SerializedPropertyType, Func<SerializedProperty, object>>()
			{
				{ SerializedPropertyType.Integer, (prop) => prop.intValue },
				{ SerializedPropertyType.Boolean, (prop) => prop.boolValue },
				{ SerializedPropertyType.Float, (prop) => prop.floatValue },
				{ SerializedPropertyType.String, (prop) => prop.stringValue },
				{ SerializedPropertyType.Color, (prop) => prop.colorValue },
				{ SerializedPropertyType.ObjectReference, (prop) => prop.objectReferenceValue },
				{ SerializedPropertyType.Enum, (prop) => prop.enumValueIndex },
				{ SerializedPropertyType.Vector2, (prop) => prop.vector2Value },
				{ SerializedPropertyType.Vector3, (prop) => prop.vector3Value },
				{ SerializedPropertyType.Vector4, (prop) => prop.vector4Value },
				{ SerializedPropertyType.Rect, (prop) => prop.rectValue },
				{ SerializedPropertyType.ArraySize, (prop) => prop.arraySize },
				{ SerializedPropertyType.AnimationCurve, (prop) => prop.animationCurveValue },
				{ SerializedPropertyType.Bounds, (prop) => prop.boundsValue },
				{ SerializedPropertyType.Quaternion, (prop) => prop.quaternionValue },
			};

		static readonly Dictionary<SerializedPropertyType, Action<SerializedProperty, object>> SerializedPropertySetters =
			new Dictionary<SerializedPropertyType, Action<SerializedProperty, object>>()
			{
				{ SerializedPropertyType.Integer, (prop, obj) => prop.intValue = (int)obj },
				{ SerializedPropertyType.Boolean, (prop, obj) => prop.boolValue = (bool)obj },
				{ SerializedPropertyType.Float, (prop, obj) => prop.floatValue = (float)obj },
				{ SerializedPropertyType.String, (prop, obj) => prop.stringValue = (string)obj },
				{ SerializedPropertyType.Color, (prop, obj) => prop.colorValue = (Color)obj },
				{ SerializedPropertyType.ObjectReference, (prop, obj) => prop.objectReferenceValue = (UnityEngine.Object)obj },
				{ SerializedPropertyType.Enum, (prop, obj) => prop.enumValueIndex = (int)obj },
				{ SerializedPropertyType.Vector2, (prop, obj) => prop.vector2Value = (UnityEngine.Vector2)obj },
				{ SerializedPropertyType.Vector3, (prop, obj) => prop.vector3Value = (UnityEngine.Vector3)obj },
				{ SerializedPropertyType.Vector4, (prop, obj) => prop.vector4Value = (UnityEngine.Vector4)obj },
				{ SerializedPropertyType.Rect, (prop, obj) => prop.rectValue = (UnityEngine.Rect)obj },
				{ SerializedPropertyType.ArraySize, (prop, obj) => prop.arraySize = (int)obj },
				{ SerializedPropertyType.AnimationCurve, (prop, obj) => prop.animationCurveValue = (AnimationCurve)obj },
				{ SerializedPropertyType.Bounds, (prop, obj) => prop.boundsValue = (UnityEngine.Bounds)obj },
				{ SerializedPropertyType.Quaternion, (prop, obj) => prop.quaternionValue = (UnityEngine.Quaternion)obj },
			};

		static readonly Dictionary<Type, Func<Rect, object, object>> EditorFields =
			new Dictionary<Type, Func<Rect, object, object>>()
			{
				{ typeof(int), (rect, value) => EditorGUI.IntField(rect, (int)value) },
				{ typeof(float), (rect, value) => EditorGUI.FloatField(rect, (float)value) },
				{ typeof(string), (rect, value) => EditorGUI.DelayedTextField(rect, (string)value) },
				{ typeof(bool), (rect, value) => EditorGUI.Toggle(rect, (bool)value) },
				{ typeof(Vector2), (rect, value) => EditorGUI.Vector2Field(rect, GUIContent.none, (Vector2)value) },
				{ typeof(Vector3), (rect, value) => EditorGUI.Vector3Field(rect, GUIContent.none, (Vector3)value) },
				{ typeof(Bounds), (rect, value) => EditorGUI.BoundsField(rect, (Bounds)value) },
				{ typeof(Rect), (rect, value) => EditorGUI.RectField(rect, (Rect)value) },
				{ typeof(Color), (rect, value) => EditorGUI.ColorField(rect, (Color)value) },
				{ typeof(Quaternion), (rect, value) => EditorGUI.Vector4Field(rect, GUIContent.none, (Vector4)value) },
				{ typeof(UnityEngine.Object), (rect, value) => EditorGUI.ObjectField(rect, (UnityEngine.Object)value, value == null ? typeof(UnityEngine.Object) : value.GetType(), true) },
			};

		static readonly Dictionary<Type, Func<object, object>> EditorLayoutFields =
			new Dictionary<Type, Func<object, object>>()
			{
				{ typeof(int), (value) => EditorGUILayout.IntField((int)value) },
				{ typeof(float), (value) => EditorGUILayout.FloatField((float)value) },
				{ typeof(string), (value) => EditorGUILayout.DelayedTextField((string)value) },
				{ typeof(bool), (value) => EditorGUILayout.Toggle((bool)value) },
				{ typeof(Vector2), (value) => EditorGUILayout.Vector2Field(GUIContent.none, (Vector2)value) },
				{ typeof(Vector3), (value) => EditorGUILayout.Vector3Field(GUIContent.none, (Vector3)value) },
				{ typeof(Bounds), (value) => EditorGUILayout.BoundsField((Bounds)value) },
				{ typeof(Rect), (value) => EditorGUILayout.RectField((Rect)value) },
				{ typeof(Color), (value) => EditorGUILayout.ColorField((Color)value) },
				{ typeof(Quaternion), (value) => EditorGUILayout.Vector4Field(GUIContent.none, (Vector4)value) },
				{ typeof(UnityEngine.Object), (value) => EditorGUILayout.ObjectField((UnityEngine.Object)value, value == null ? typeof(UnityEngine.Object) : value.GetType(), true) },
			};

		static readonly Dictionary<Type, Func<object, object, bool>> EditorCompareMethods =
			new Dictionary<Type, Func<object, object, bool>>()
			{
				{ typeof(int), (left, right) => (int)left == (int)right },
				{ typeof(float), (left, right) => BD.Maths.NearEqual((float)left, (float)right) },
				{ typeof(string), (left, right) => (string)left == (string)right },
				{ typeof(bool), (left, right) => (bool)left == (bool)right },
				{ typeof(Vector2), (left, right) => ((Vector2)left).NearEqual((Vector2)right) },
				{ typeof(Vector3), (left, right) => ((Vector3)left).NearEqual((Vector3)right) },
				{ typeof(Bounds), (left, right) => (Bounds)left == (Bounds)right },
				{ typeof(Rect), (left, right) => (Rect)left == (Rect)right },
				{ typeof(Color), (left, right) => (Color)left == (Color)right },
				{ typeof(Quaternion), (left, right) => (Quaternion)left == (Quaternion)right },
				{ typeof(UnityEngine.Object), (left, right) => System.Object.ReferenceEquals(left, right) },
			};

		public static float CalcPropertyHeight(SerializedProperty prop)
		{
			float ret = Math.Max(EditorGUI.GetPropertyHeight(prop), k_rowHeight);
			if (ret > k_rowHeight)
				ret += k_spacerHeight; //leave a space after long properties for easier reading
			return ret;
		}

		//This is a (imo rare) case of needing a templated and a pass-in-a-type version of a function
		//See http://stackoverflow.com/questions/4667981/c-sharp-use-system-type-as-generic-parameter 
		//This is the way for both System.Type and <T> to use the same code
		public static object EditField(Rect rect, object value, Type valueType)
		{
			Func<Rect, object, object> field;
			if (EditorFields.TryGetValue(valueType, out field))
				return field(rect, value);

			if (valueType.IsEnum)
				return (object)EditorGUI.EnumPopup(rect, (Enum)(object)value);

			if (valueType.BDIsOfType(typeof(ScriptableObject)))
			{
				Editor scriptableObjectEditor = Editor.CreateEditor((ScriptableObject)value);
				scriptableObjectEditor.OnInspectorGUI();
			}

			if (typeof(UnityEngine.Object).IsAssignableFrom(valueType))
				return (object)EditorGUI.ObjectField(rect, (UnityEngine.Object)(object)value, valueType, true);


			//Fallback: just write out its ToString and you can't change anything
			if (value == null)
				EditorGUI.LabelField(rect, "null");
			else
				EditorGUI.LabelField(rect, value.ToString());
			return value;
		}

		public static T EditField<T>(Rect rect, T value)
		{
			Type type = typeof(T);
			object ret = EditField(rect, value as object, type);
			return (T)ret;
		}

		public static bool HasBultInEditField<T>()
		{
			Type type = typeof(T);
			
			Func<Rect, object, object> field;
			if (EditorFields.TryGetValue(type, out field))
				return true;

			if (type.IsEnum)
				return true;

			if (typeof(UnityEngine.Object).IsAssignableFrom(type))
				return true;

			return false;
		}

		//In retrospect, this is probably useless thanks to simply EditorGUI.PropertyField(fieldLoc, val, GUIContent.none);
		public static void EditField(Rect rect, SerializedProperty value)
		{
			Type type;
			bool success = SerializedPropertyTypes.TryGetValue(value.propertyType, out type);
			if (!success) return;

			Func<SerializedProperty, object> getter;
			success = SerializedPropertyGetters.TryGetValue(value.propertyType, out getter);
			if (!success) return;

			Action<SerializedProperty, object> setter;
			success = SerializedPropertySetters.TryGetValue(value.propertyType, out setter);
			if (!success) return;

			Func<object, object, bool> compareFunc;
			success = EditorCompareMethods.TryGetValue(type, out compareFunc);
			if (!success) return;

			object oldVal = getter(value);
			object newVal = EditField(rect, oldVal, type);
			if (!compareFunc(oldVal, newVal))
				setter(value, newVal);
		}

		public static T EditLayoutField<T>(T value)
		{
			Type type = typeof(T);

			Func<object, object> layoutField;
			if (EditorLayoutFields.TryGetValue(type, out layoutField))
				return (T)layoutField(value);

			if (type.IsEnum)
				return (T)(object)EditorGUILayout.EnumPopup((Enum)(object)value);

			if (typeof(UnityEngine.Object).IsAssignableFrom(type))
				return (T)(object)EditorGUILayout.ObjectField((UnityEngine.Object)(object)value, type, true);

			Debug.Log("Type is not supported: " + type);
			return value;
		}

		static bool AreFieldsSame<T>(T left, T right)
		{
			Type type = typeof(T);

			Func<object, object, bool> compareFunc;
			if (EditorCompareMethods.TryGetValue(type, out compareFunc))
				return compareFunc(left, right);

			if (type.IsEnum)
				return ((Enum)(object)left) == ((Enum)(object)right);

			if (typeof(UnityEngine.Object).IsAssignableFrom(type))
				return UnityEngine.Object.ReferenceEquals(left, right);

			//You probably got here because you're like a BD.IntCoord, which isn't a Unity object or enum,
			//but you're used in dictionaries often. We're probably displaying you as the .ToString() fallback.
			//Just say you haven't changed the field, because you probably can't.
			return true;
		}

		public static void EditList<T>(string title, List<T> vals) { EditList(title, vals, null, () => TypeHelper.CreateNewInstanceOf<T>());}
		public static void EditList<T>(string title, List<T> vals, System.Func<BD.Rect, T, T> editLineFunc) { EditList(title, vals, editLineFunc, () => TypeHelper.CreateNewInstanceOf<T>());}
		public static void EditList<T>(string title, List<T> vals, System.Func<BD.Rect, T, T> editLineFunc, System.Func<T> createNewFunc)
		{
			T[] valsArr = vals.ToArray();
			int oldLength = valsArr.Length;
			EditList(title, ref valsArr, editLineFunc, createNewFunc);

			bool isChanged = (oldLength != valsArr.Length);
			if (!isChanged)
			{
				for (int i = 0; i < valsArr.Length; ++i)
					isChanged |= (!object.ReferenceEquals(vals[i], valsArr[i]));
			}

			if (isChanged)
			{
				vals.Clear();
				vals.AddRange(valsArr);
			}
		}

		public static void EditList<T>(string title, ref T[] vals) { EditList(title, ref vals, null, () => TypeHelper.CreateNewInstanceOf<T>()); }
		public static void EditList<T>(string title, ref T[] vals, System.Func<BD.Rect, T, T> editLineFunc, System.Func<T> createNewFunc)
		{
			if (vals == null)
				EditorGUILayout.LabelField(title + ": null", EditorStyles.boldLabel);
			else
			{
				EditorGUILayout.LabelField(title + " (" + vals.Length + " count):", EditorStyles.boldLabel);

				int removeIndex = -1;
				int insertBeforeIndex = -1;
				for (int i = 0; i < vals.Length; ++i)
				{
					//EditorGuiLayout is awful so this is a dirty trick to reserve a line
					BD.Rect lineRect = EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("");
					EditorGUILayout.EndHorizontal();

					//And actually draw into that line
					BD.Rect minusButtonRect = lineRect.AtMaxWidth(15);
					BD.Rect plusButtonRect = minusButtonRect.ScrollRight();
					BD.Rect dataRect = lineRect.MoveLeftRight(minusButtonRect.Width + plusButtonRect.Width + 3);
					if (GUI.Button(minusButtonRect, "-")) removeIndex = i;
					if (GUI.Button(plusButtonRect, "+")) insertBeforeIndex = i;

					if (editLineFunc != null)
						vals[i] = editLineFunc(dataRect, vals[i]);
					else
						vals[i] = EditField(dataRect, vals[i]);
				}

				if (removeIndex != -1)
				{
					List<T> temp = new List<T>(vals);
					temp.RemoveAt(removeIndex);
					vals = temp.ToArray();
				}

				if (insertBeforeIndex != -1)
				{
					List<T> temp = new List<T>(vals);
					temp.Insert(insertBeforeIndex, createNewFunc());
					vals = temp.ToArray();
				}

				BD.Rect finalLineRect = EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("");
				EditorGUILayout.EndHorizontal();

				BD.Rect finalMinusButtonRect = finalLineRect.AtX(finalLineRect.Right - 30).AtWidth(30);
				BD.Rect finalPlusButtonRect = finalMinusButtonRect.ScrollLeft();

				if (GUI.Button(finalPlusButtonRect, "+"))
				{
					List<T> temp = new List<T>(vals);
					temp.Add(createNewFunc());
					vals = temp.ToArray();
				}
				if (GUI.Button(finalMinusButtonRect, "-"))
				{
					List<T> temp = new List<T>(vals);
					temp.RemoveAt(temp.Count - 1);
					vals = temp.ToArray();
				}

				EditorGUILayout.Space();
			}
		}

		public static void EditList(string title, SerializedProperty valsArray)
		{
			if (valsArray == null)
				EditorGUILayout.LabelField(title + ": null", EditorStyles.boldLabel);
			else
			{
				Debug.Assert(valsArray.isArray, "Called EditList(SerializedProperty) on a non-array serialized property!");
				EditorGUILayout.LabelField(title + " (" + valsArray.arraySize + " count):", EditorStyles.boldLabel);

				int removeIndex = -1;
				int insertAtIndex = -1;
				int moveUpIndex = -1;
				int moveDownIndex = -1;

				for (int i = 0; i < valsArray.arraySize; ++i)
				{
					float lineHeight = EditorGUI.GetPropertyHeight(valsArray.GetArrayElementAtIndex(i));
					lineHeight = Math.Max(lineHeight, 2 * k_rowHeight); //2 rows min for +-^v buttons

					//EditorGuiLayout is awful so this is a dirty trick to reserve a line
					BD.Rect lineRect = EditorGUILayout.BeginHorizontal(GUILayout.MinHeight(lineHeight));
					EditorGUILayout.LabelField("");
					EditorGUILayout.EndHorizontal();

					//draw +-^v buttons on the side...
					BD.Rect minusButtonRect = lineRect.AtMaxWidth(16).AtMaxHeight(k_rowHeight);
					BD.Rect plusButtonRect = minusButtonRect.ScrollRight();
					BD.Rect upButtonRect = minusButtonRect.ScrollDown();
					BD.Rect downButtonRect = plusButtonRect.ScrollDown();
					if (GUI.Button(minusButtonRect, "-")) removeIndex = i;
					if (GUI.Button(plusButtonRect, "+")) insertAtIndex = i;
					if (i > 0 && GUI.Button(upButtonRect, "^")) moveUpIndex = i;
					if (i < valsArray.arraySize - 1 && GUI.Button(downButtonRect, "v")) moveDownIndex = i;

					//And actually draw into that line
					BD.Rect dataRect = lineRect.MoveLeftRight(minusButtonRect.Width + plusButtonRect.Width + 8);

					EditorGUI.PropertyField(dataRect, valsArray.GetArrayElementAtIndex(i), true);
				}

				if (removeIndex != -1)
					valsArray.DeleteArrayElementAtIndex(removeIndex);
				if (insertAtIndex != -1)
					valsArray.InsertArrayElementAtIndex(insertAtIndex);
				if (moveUpIndex != -1)
					valsArray.MoveArrayElement(moveUpIndex, moveUpIndex - 1);
				if (moveDownIndex != -1)
					valsArray.MoveArrayElement(moveDownIndex, moveDownIndex + 1);

				BD.Rect finalLineRect = EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("");
				EditorGUILayout.EndHorizontal();

				BD.Rect finalMinusButtonRect = finalLineRect.AtX(finalLineRect.Right - 30).AtWidth(30);
				BD.Rect finalPlusButtonRect = finalMinusButtonRect.ScrollLeft();

				if (GUI.Button(finalPlusButtonRect, "+"))
				{
					valsArray.arraySize += 1;
				}
				if (GUI.Button(finalMinusButtonRect, "-"))
				{
					valsArray.DeleteArrayElementAtIndex(valsArray.arraySize - 1);
				}

				EditorGUILayout.Space();
			}
		}
		
		public static void DrawList<T>(string title, T[] vals)
			where T : class
		{
			DrawListWithSelected(title, vals, null);
		}

		public static void DrawListWithSelected<T>(string title, T[] vals, int selectedIndex)
			where T : class
		{
			DrawListWithSelected(title, vals, vals != null && BD.Algo.InBounds(selectedIndex, vals.Length) ? vals[selectedIndex] : null);
		}

		public static void DrawListWithSelected<T>(string title, T[] vals, T selected)
			where T : class
		{
			if (vals == null)
				EditorGUILayout.LabelField(title + ": null", EditorStyles.boldLabel);
			else if (vals.Length == 0)
				EditorGUILayout.LabelField(title + ": 0 count", EditorStyles.boldLabel);
			else
			{
				EditorGUILayout.LabelField(title + " (" + vals.Length + " count):", EditorStyles.boldLabel);
				foreach (T val in vals)
				{
					if (object.ReferenceEquals(val, selected))
						EditorGUILayout.LabelField("x " + val.ToString());
					else
						EditorGUILayout.LabelField("  " + val.ToString());
				}
			}

			EditorGUILayout.Space();
		}

		public static void DrawDictionary<T, U>(string title, Dictionary<T, U> vals)
		{
			if (vals == null)
				EditorGUILayout.LabelField(title + ": null", EditorStyles.boldLabel);
			else if (vals.Count == 0)
				EditorGUILayout.LabelField(title + ": 0 count", EditorStyles.boldLabel);
			else
			{
				EditorGUILayout.LabelField(title + " (" + vals.Count + " count):", EditorStyles.boldLabel);

				foreach (var val in vals)
				{
					EditorGUILayout.LabelField("  " + val.Key.ToString() + ": " + val.Value.ToString());
				}
			}

			EditorGUILayout.Space();
		}

		public static BD.IntCoord EditIntCoord(string label, BD.IntCoord val)
		{
			BD.IntCoord temp = val;
			EditIntCoord(label, ref temp);
			return temp;
		}

		public static void EditIntCoord(string label, ref BD.IntCoord val)
		{
			EditorGUILayout.BeginHorizontal();
			
			if (label != "")
				EditorGUILayout.LabelField(label);

			BD.IntCoord newVal = new BD.IntCoord();

			float oldLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = 12;
			newVal.X = EditorGUILayout.DelayedIntField("X", val.X);
			newVal.Y = EditorGUILayout.DelayedIntField("Y", val.Y);
			EditorGUIUtility.labelWidth = oldLabelWidth;
			EditorGUILayout.EndHorizontal();

			if (newVal != val)
				val = newVal;
		}

		public static T SelectFromPopup<T>(string label, T currentSelection, ICollection<T> availableVals, Func<T, string> stringConversionFunc)
		{
			T[] tmp = new T[availableVals.Count];
			availableVals.CopyTo(tmp, 0);
			
			string[] asArrayStrings = new string[availableVals.Count];

			int index = 0;
			for (int i = 0; i < asArrayStrings.Length; ++i)
			{
				asArrayStrings[i] = stringConversionFunc(tmp[i]);

				if (System.Object.ReferenceEquals(currentSelection, tmp[i]))
					index = i;
			}

			int selectedIndex = EditorGUILayout.Popup(label, index, asArrayStrings);
			return tmp[selectedIndex];
		}

		public static T SelectFromPopupWithNone<T>(string label, T currentSelection, ICollection<T> availableVals, Func<T, string> stringConversionFunc)
			where T : class
		{
			T[] tmp = new T[availableVals.Count + 1];
			availableVals.CopyTo(tmp, 1);
			tmp[0] = null;
			
			string[] asArrayStrings = new string[tmp.Length];
			asArrayStrings[0] = "[none]";

			int index = 0;
			for (int i = 1; i < tmp.Length; ++i)
			{
				asArrayStrings[i] = stringConversionFunc(tmp[i]);

				if (System.Object.ReferenceEquals(currentSelection, tmp[i]))
					index = i;
			}

			int selectedIndex = EditorGUILayout.Popup(label, index, asArrayStrings);
			return tmp[selectedIndex];
		}

		public static string SelectStringFromPopup(string label, string stringSelection, ICollection<string> availableStrings)
		{
			string[] asArray = new string[availableStrings.Count];
			availableStrings.CopyTo(asArray, 0);

			int index = 0;
			for (int i = 0; i < asArray.Length; ++i)
				if (BD.Strings.EzCompare(asArray[i], stringSelection))
					index = i;

			return asArray[EditorGUILayout.Popup(label, index, asArray)];
		}
		
		public static void DrawFlowList<TAction, TActionAttribute, TTypeEnum>(string label, FlowList<TAction, TActionAttribute, TTypeEnum> val)
			where TAction : class, ITyped<TTypeEnum>, IXmlable
			where TActionAttribute : NamedAttribute
			where TTypeEnum : struct, IConvertible, IComparable, IFormattable //is an enum
		{
			BD.UEIHelper.DrawListWithSelected(label, val.Actions.ToArray(), val.CurrentIndex);
			EditorGUILayout.LabelField("At index " + val.CurrentIndex + (val.IsCompleted() ? ", COMPLETE" : ""), BD.UStyle.BoldLabelStyle);
		}
	}
}
