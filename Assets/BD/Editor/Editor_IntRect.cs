﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class IntRectDrawer : OdinValueDrawer<BD.IntRect>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.IntRect> entry, GUIContent label)
	{
		BD.IntRect val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect xRect = position.AtWidth(position.Width / 2).AtMaxWidth(160);
		BD.Rect yRect = xRect.ScrollRight();

		position = EditorGUILayout.GetControlRect();
		BD.Rect widthRect = xRect.ScrollDown();
		BD.Rect heightRect = yRect.ScrollDown();

		BD.UEIHelper.PushLabelWidth(30);
		val.X = EditorGUI.IntField(xRect, "X", val.X);
		val.Y = EditorGUI.IntField(yRect, "Y", val.Y);
		val.Width = EditorGUI.IntField(widthRect, "Wid", val.Width);
		val.Height = EditorGUI.IntField(heightRect, "Hei", val.Height);
		BD.UEIHelper.PopLabelWidth();

		if (entry.IsEditable)
			entry.SmartValue = val;
	}
}