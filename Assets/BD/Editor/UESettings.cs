﻿using UnityEditor;

public static class UESettings
{
	[UnityEditor.Callbacks.DidReloadScripts]
	static void OnReloadScripts()
	{
		UnityEditor.EditorApplication.playModeStateChanged += OnGameStateChanged;
		ApplySettings();
	}

	static void OnGameStateChanged(PlayModeStateChange newState)
	{
		if (newState == PlayModeStateChange.ExitingEditMode)
		{
			//UnityEngine.Debug.Log("player just hit the 'play' button and we're about to launch in");
		}
		else if (newState == PlayModeStateChange.EnteredPlayMode)
		{
			//note this occurs AFTER everything has Awake() called on it
			//UnityEngine.Debug.Log("we just finished going into the play state");
		}
		else if (newState == PlayModeStateChange.ExitingPlayMode)
		{
			//UnityEngine.Debug.Log("player just hit the 'play' button again to end a playthrough");
		}
		else if (newState == PlayModeStateChange.EnteredEditMode)
		{
			//UnityEngine.Debug.Log("we just finished going back into edit state");
		}
		ApplySettings();
	}

	static void ApplySettings()
	{
		UnityEngine.Assertions.Assert.raiseExceptions = true;
	}
}
