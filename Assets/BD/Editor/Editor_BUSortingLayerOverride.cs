﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(BUSortingLayerOverride))]
public class BUSortingLayerOverrideInspector : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		BUSortingLayerOverride myObject = (BUSortingLayerOverride)target;

		SortingLayer[] sortingLayers = SortingLayer.layers;
		List<string> sortingLayerNames = new List<string>();
		foreach (var sortingLayer in sortingLayers) sortingLayerNames.Add(sortingLayer.name);

		int currentIndex = -1;
		if (myObject.OverrideSortingLayer.HasValue) currentIndex = sortingLayerNames.FindIndex(x => x == myObject.OverrideSortingLayer.Value.name);


		int selectedIndex = UnityEditor.EditorGUILayout.Popup("Override Sorting Layer", currentIndex, sortingLayerNames.ToArray());
		if (selectedIndex != currentIndex)
		{
			myObject.OverrideSortingLayer = sortingLayers[selectedIndex];
			foreach (Transform childTransform in myObject.transform)
			{
				Renderer rendererComponent = childTransform.gameObject.GetComponent<Renderer>();
				if (rendererComponent != null)
					rendererComponent.sortingLayerID = myObject.OverrideSortingLayer.Value.id;
			}
		}
	}
}