﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class Vector3Drawer : OdinValueDrawer<BD.Vector3>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.Vector3> entry, GUIContent label)
	{
		BD.Vector3 val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect xRect = position.AtWidth(position.Width / 3).AtMaxWidth(120);
		BD.Rect yRect = xRect.ScrollRight();
		BD.Rect zRect = yRect.ScrollRight();

		BD.UEIHelper.PushLabelWidth(20);
		val.X = EditorGUI.FloatField(xRect, "X", val.X);
		val.Y = EditorGUI.FloatField(yRect, "Y", val.Y);
		val.Z = EditorGUI.FloatField(zRect, "Z", val.Z);
		BD.UEIHelper.PopLabelWidth();
		
		entry.SmartValue = val;
	}
}