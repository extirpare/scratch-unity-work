﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class MinMaxSliderDrawer : OdinValueDrawer<BD.MinMaxSlider>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.MinMaxSlider> entry, GUIContent label)
	{
		BD.MinMaxSlider val = entry.SmartValue;
		Vector2 valAsVector = new Vector2(val.Min, val.Max);
		valAsVector = SirenixEditorFields.MinMaxSlider(label, valAsVector, new Vector2(0, 1), true);
		val.Min = valAsVector.x;
		val.Max = valAsVector.y;
		entry.SmartValue = val;
	}
}