﻿using UnityEngine;
using UnityEditor;
using System;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

// Second DrawerPriority == wrapper == this pretty much.
[OdinDrawer]
[DrawerPriority(0, 1, 0)]
public class Wrapper_FlagsOfDrawer<T> : OdinValueDrawer<FlagsOf<T>>
	where T : struct, IConvertible, IComparable, IFormattable
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<FlagsOf<T>> entry, GUIContent label)
	{
		// Draw a box around the property.
		SirenixEditorGUI.BeginBox(label);
		this.CallNextDrawer(entry, null);
		SirenixEditorGUI.EndBox();
	}
}

[OdinDrawer]
public class FlagsOfDrawer<T> : OdinValueDrawer<FlagsOf<T>>
	where T : struct, IConvertible, IComparable, IFormattable
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<FlagsOf<T>> entry, GUIContent label)
	{
		SirenixEditorGUI.BeginVerticalList(false, false);
		FlagsOf<T> val = entry.SmartValue;

		if (label != null)
			EditorGUI.LabelField(EditorGUILayout.GetControlRect(), label, BD.UStyle.BoldLabelStyle);

		foreach (T enumKey in EnumHelper.GetValuesIn<T>())
		{
			SirenixEditorGUI.BeginListItem(false);

			BD.Rect line = EditorGUILayout.GetControlRect();
			BD.Rect labelArea = line.AtWidth(0.8f * line.Width);
			BD.Rect checkboxArea = labelArea.ScrollRight().AtWidth(0.2f * line.Width);

			EditorGUI.LabelField(labelArea, enumKey.ToString());
			val.Set(enumKey, EditorGUI.Toggle(checkboxArea, val[enumKey]));

			SirenixEditorGUI.EndListItem();
		}

		SirenixEditorGUI.EndVerticalList();
		entry.SmartValue = val;
	}
}
