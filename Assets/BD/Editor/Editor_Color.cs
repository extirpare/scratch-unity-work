﻿using UnityEngine;
using UnityEditor;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class ColorDrawer : OdinValueDrawer<BD.Color>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<BD.Color> entry, GUIContent label)
	{
		BD.Color val = entry.SmartValue;
		BD.Color newColor = (label != null) ? EditorGUILayout.ColorField(label, val) : EditorGUILayout.ColorField(val);
		entry.SmartValue = newColor;
	}
}