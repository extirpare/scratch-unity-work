﻿using UnityEngine;
using UnityEditor;
using System;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

namespace BD
{
	public abstract class EnumToValDrawerBase<TEnumKey, TValue> : OdinValueDrawer<EnumToVal<TEnumKey, TValue>>
		where TEnumKey : struct, IConvertible, IComparable, IFormattable
	{
		protected virtual float CalcLabelLength(float rowLength)
		{
			if (typeof(TValue).BDIsOfType(typeof(int)) || typeof(TValue).BDIsOfType(typeof(bool)) || typeof(TValue).BDIsOfType(typeof(float)))
				return 0.7f * rowLength;
			else
				return Mathf.Max(100, 0.3f * rowLength);
		}

		protected abstract BD.Color CalcLabelColor(TEnumKey enumKey, TValue value);
        private static GUIStyle k_marginStyle = new GUIStyle() { margin = new RectOffset(8, 0, 0, 0) };

		protected override void DrawPropertyLayout(IPropertyValueEntry<EnumToVal<TEnumKey, TValue>> entry, GUIContent label)
		{
			SirenixEditorGUI.BeginVerticalList(false, false);
			EnumToVal<TEnumKey, TValue> val = entry.SmartValue;
			val.SanityCheck();
			
			foreach (TEnumKey enumKey in EnumHelper.GetValuesIn<TEnumKey>())
			{
				SirenixEditorGUI.BeginListItem(false);
				int orderInEnum = EnumHelper.OrderInEnum<TEnumKey>(enumKey);

				float labelWidth = CalcLabelLength(EditorGUIUtility.currentViewWidth);
				GUILayout.BeginHorizontal();
				
				//Label
				GUILayout.BeginVertical(GUILayout.MaxWidth(labelWidth));
				BD.UStyle labelStyle = UStyle.LabelStyle;
				labelStyle.FontColor = CalcLabelColor(enumKey, val[enumKey]);
				EditorGUILayout.LabelField(enumKey.ToString(), labelStyle);
				GUILayout.EndVertical();

				//Value
				EditorGUILayout.BeginVertical(k_marginStyle);
				if (entry.Property != null && entry.Property.Children != null && entry.Property.Children["m_values"] != null && entry.Property.Children["m_values"].Children[orderInEnum] != null)
					InspectorUtilities.DrawProperty(entry.Property.Children["m_values"].Children[orderInEnum], null);
				else
					EditorGUILayout.LabelField("null");

				EditorGUILayout.EndVertical();
				GUILayout.EndHorizontal();
				SirenixEditorGUI.EndListItem();
			}
			SirenixEditorGUI.EndVerticalList();

			if (entry.IsEditable)
				entry.SmartValue = val;
		}
	}

	[OdinDrawer]
	public class EnumToValDrawer_ZeroGray<TEnumKey, TValue> : EnumToValDrawerBase<TEnumKey, TValue>
		where TEnumKey : struct, IConvertible, IComparable, IFormattable
		where TValue : IComparable<Int32>
	{
		protected override BD.Color CalcLabelColor(TEnumKey key, TValue val)
		{
			if (val.CompareTo(0) == 0)
				return BD.Color.DarkGray;
			else
				return BD.Color.Black;
		}
	}

	[OdinDrawer]
	public class EnumToValDrawer<TEnumKey, TValue> : EnumToValDrawerBase<TEnumKey, TValue>
		where TEnumKey : struct, IConvertible, IComparable, IFormattable
	{
		protected override BD.Color CalcLabelColor(TEnumKey key, TValue val)
		{
			return BD.Color.Black;
		}
	}
}
