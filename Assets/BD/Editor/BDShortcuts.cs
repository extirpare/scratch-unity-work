﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public static class AcceptArrowKeysAsHotkeys
{
	static AcceptArrowKeysAsHotkeys()
	{
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	static void OnSceneGUI(SceneView sv)
	{
		if (Event.current == null) return;
		if (Event.current.keyCode == KeyCode.LeftArrow && Event.current.control)
			EditorApplication.ExecuteMenuItem("BD Shortcuts/Go To Parent");
	}
}

public class BDShortcuts : Editor
{
	//ctrl+h
	[UnityEditor.MenuItem("BD Shortcuts/Toggle Selection Hidden %h")]
	static void ToggleSelectionActive()
	{
		foreach (GameObject gameObject in UnityEditor.Selection.gameObjects)
			gameObject.SetActive(!gameObject.activeSelf);
	}

	[UnityEditor.MenuItem("BD Shortcuts/Go To Parent")]
	static void GoToParent()
	{
		GameObject currentObject = UnityEditor.Selection.activeGameObject;
		if (currentObject != null && currentObject.transform.parent != null)
			UnityEditor.Selection.activeGameObject = currentObject.transform.parent.gameObject;
	}

	//ctrl+u
	[UnityEditor.MenuItem("BD Shortcuts/Toggle Editor UI Layer Visibility %u")]
	static void ToggleUIVisibility()
	{
		int uiLayerNum = LayerMask.NameToLayer("UI");
		bool isLayerSet = BD.Algo.IsBitSet((uint)Tools.visibleLayers, uiLayerNum);
		Tools.visibleLayers = (int)BD.Algo.SetBit((uint)Tools.visibleLayers, uiLayerNum, !isLayerSet);
	}
}