﻿using BD;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(SpriteReplacer))]
public class Editor_SpriteReplacer : Editor
{
    string m_replacementString;
    Sprite m_replacementSprite;
    
	public override void OnInspectorGUI()
	{
        SpriteReplacer val = target as SpriteReplacer;
        base.OnInspectorGUI();

        EditorGUILayout.BeginHorizontal();
        m_replacementString = EditorGUILayout.TextField(m_replacementString);
        m_replacementSprite = EditorGUILayout.ObjectField(m_replacementSprite, typeof(Sprite), false) as Sprite;
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Replace values for matching keys"))
        {
            List<Sprite> keys = new List<Sprite>(val.Replacements.Keys);
            foreach (Sprite key in keys)
            {
                string path = AssetDatabase.GetAssetPath(key);
                if (Strings.EzContains(path, m_replacementString) || BD.Strings.IsNullOrEmpty(m_replacementString))
                    val.Replacements[key] = m_replacementSprite;
            }
        }
    }
}

[CustomPropertyDrawer(typeof(UDictionarySpriteToSprite))]
public class UDictionarySpriteToSpriteDrawer : BD.UDictionaryDrawer<Sprite, Sprite> { }
