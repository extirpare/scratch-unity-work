﻿using UnityEngine;
using UnityEditor;
using BD;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class EmptyElementDrawer : OdinValueDrawer<EmptyElement>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<EmptyElement> entry, GUIContent label)
	{
		//literally do nothing
	}
}
