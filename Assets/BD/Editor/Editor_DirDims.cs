﻿using UnityEngine;
using UnityEditor;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class DirDimsDrawer : OdinValueDrawer<DirDims>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<DirDims> entry, GUIContent label)
	{
		DirDims val = entry.SmartValue;

		if (label != null)
			EditorGUILayout.LabelField(label, BD.UStyle.BoldLabelStyle);

		BD.Rect rowOne = EditorGUILayout.GetControlRect();
		BD.Rect rowTwo = EditorGUILayout.GetControlRect();

		UEIHelper.PushLabelWidth(40);
		val.Left = EditorGUI.FloatField(rowOne.AtWidth(rowOne.Width / 2), "Left", val.Left);
		val.Top = EditorGUI.FloatField(rowOne.AtWidth(rowOne.Width / 2).ScrollRight(), "Top", val.Top);
		val.Bottom = EditorGUI.FloatField(rowTwo.AtWidth(rowTwo.Width / 2), "Bottom", val.Bottom);
		val.Right = EditorGUI.FloatField(rowTwo.AtWidth(rowTwo.Width / 2).ScrollRight(), "Right", val.Right);
		UEIHelper.PopLabelWidth();

		entry.SmartValue = val;
	}
}
