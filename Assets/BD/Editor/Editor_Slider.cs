﻿using UnityEngine;
using UnityEditor;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

// OdinDrawers with 'super' DrawerPriority (first number) are meant to null-check
[OdinDrawer]
[DrawerPriority(1, 0, 0)]
public class Super_SliderDrawer : OdinValueDrawer<Slider>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<Slider> entry, GUIContent label)
	{
		if (entry.SmartValue == null)
			entry.SmartValue = new Slider();

		this.CallNextDrawer(entry, label);
	}
}

[OdinDrawer]
[DrawerPriority(0, 1, 0)] //skip all the wrappering that normally applies to classes
public class SliderDrawer : OdinValueDrawer<Slider>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<Slider> entry, GUIContent label)
	{
		Slider val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect sliderRect, labelRect, maxRect;
		sliderRect = new BD.Rect(position).AtWidth(position.Width - 50);
		labelRect = sliderRect.ScrollRight().AtWidth(10);
		maxRect = labelRect.ScrollRight().AtWidth(40);

		val.CurrentValue = EditorGUI.Slider(sliderRect, val.CurrentValue, 0, val.MaxValue);
		EditorGUI.LabelField(labelRect, "/");
		val.MaxValue = EditorGUI.FloatField(maxRect, val.MaxValue);

		entry.SmartValue = val;
	}
}