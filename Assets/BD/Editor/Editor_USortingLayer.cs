﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(USortingLayer))]
public class USortingLayerDrawer : PropertyDrawer
{
	public override void OnGUI(UnityEngine.Rect position, SerializedProperty property, GUIContent label)
	{
		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		int currentLayerID = property.FindPropertyRelative("LayerID").intValue;

		SortingLayer[] sortingLayers = SortingLayer.layers;
		List<string> sortingLayerNames = new List<string>();
		foreach (var sortingLayer in sortingLayers) sortingLayerNames.Add(sortingLayer.name);

		int index = System.Array.FindIndex(sortingLayers, x => x.id == currentLayerID);

		int selectedIndex = EditorGUI.Popup(position, index, sortingLayerNames.ToArray());
		if (selectedIndex != index)
		{
			int newId = sortingLayers[selectedIndex].id;
			property.FindPropertyRelative("LayerID").intValue = newId;
		}

		// Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty();
	}
}