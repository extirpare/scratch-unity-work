﻿using UnityEngine;
using UnityEditor;
using BD;

using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

[OdinDrawer]
public class IntCoordDrawer : OdinValueDrawer<IntCoord>
{
	protected override void DrawPropertyLayout(IPropertyValueEntry<IntCoord> entry, GUIContent label)
	{
		IntCoord val = entry.SmartValue;

		BD.Rect position = EditorGUILayout.GetControlRect();
		if (label != null)
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		BD.Rect xRect = position.AtWidth(position.Width / 2).AtMaxWidth(60);
		BD.Rect yRect = xRect.ScrollRight();

		UEIHelper.PushLabelFieldWidth(12, 60);
		val.X = EditorGUI.IntField(xRect, "X", val.X);
		val.Y = EditorGUI.IntField(yRect, "Y", val.Y);
		UEIHelper.PopLabelFieldWidth();

		entry.SmartValue = val;
	}
}