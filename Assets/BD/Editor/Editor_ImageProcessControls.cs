﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(ImageProcessControls))]
public class Editor_ImageProcessControls : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		ImageProcessControls controls = (ImageProcessControls)target;

		if (GUILayout.Button("Cut To 128x128 Squares"))
		{
			Texture2D myTexture = controls.GetComponent<SpriteRenderer>().sprite.texture;
			string texturePath = AssetDatabase.GetAssetPath(myTexture);
			TextureImporter textureImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			textureImporter.isReadable = true;
			textureImporter.spriteImportMode = SpriteImportMode.Multiple;

			List<SpriteMetaData> newData = new List<SpriteMetaData>();

			int SliceWidth = 128;
			int SliceHeight = 128;

			for (int i = 0; i < myTexture.width; i += SliceWidth)
			{
				for (int j = myTexture.height; j > 0; j -= SliceHeight)
				{
					SpriteMetaData smd = new SpriteMetaData();
					smd.pivot = new Vector2(0.5f, 0.5f);
					smd.alignment = 9;
					smd.name = (myTexture.height - j) / SliceHeight + ", " + i / SliceWidth;
					smd.rect = new Rect(i, j - SliceHeight, SliceWidth, SliceHeight);

					newData.Add(smd);
				}
			}

			textureImporter.spritesheet = newData.ToArray();
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		}

		if (GUILayout.Button("Apply Names In Order"))
		{
			Texture2D myTexture = controls.GetComponent<SpriteRenderer>().sprite.texture;
			string texturePath = AssetDatabase.GetAssetPath(myTexture);
			TextureImporter textureImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			textureImporter.isReadable = true;

			SpriteMetaData[] spriteVals = textureImporter.spritesheet;
			for (int i = 0; i < spriteVals.Length; ++i)
			{
				int iter = i;
				int j = 0;
				while (j < controls.NamesAndCounts.Length - 1 && iter >= controls.NamesAndCounts[j].Count)
				{
					iter -= controls.NamesAndCounts[j].Count;
					++j;
				}
				spriteVals[i].name = controls.NamesAndCounts[j].Name + " " + iter;
				spriteVals[i].alignment = 7; //https://docs.unity3d.com/ScriptReference/SpriteMetaData-alignment.html boo Unity boo
			}
			textureImporter.spritesheet = spriteVals;
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		}

		if (GUILayout.Button("Re-Order Sprites"))
		{
			Texture2D myTexture = controls.GetComponent<SpriteRenderer>().sprite.texture;
			string texturePath = AssetDatabase.GetAssetPath(myTexture);
			TextureImporter textureImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			textureImporter.isReadable = true;

			SpriteMetaData[] spriteVals = textureImporter.spritesheet;
			List<SpriteMetaData> scratchSpriteVals = new List<SpriteMetaData>(spriteVals);
			List<SpriteMetaData> outSpriteVals = new List<SpriteMetaData>();

			//the first row consists of those sprites whose tops are above the bottom of the highest sprite bottom in the land
			//this sorts so that LARGEST yMax is first
			scratchSpriteVals.Sort(delegate (SpriteMetaData a, SpriteMetaData b) { return b.rect.yMax.CompareTo(a.rect.yMax); });

			//note that y=0 is the bottom of the sprite sheet (ugh) so we start at biggest yMin and work down
			int count = 0;
			while (scratchSpriteVals.Count > 0)
			{
				float centerLineThisRow = (scratchSpriteVals[0].rect.yMin + scratchSpriteVals[0].rect.yMax) / 2;
				List<SpriteMetaData> thisRow = scratchSpriteVals.FindAll(
					delegate (SpriteMetaData val) { return val.rect.yMin < centerLineThisRow && val.rect.yMax > centerLineThisRow; });

				scratchSpriteVals.RemoveAll(
					delegate (SpriteMetaData val) { return val.rect.yMin < centerLineThisRow && val.rect.yMax > centerLineThisRow; });

				thisRow.Sort(delegate (SpriteMetaData a, SpriteMetaData b) { return a.rect.xMin.CompareTo(b.rect.xMin); });

				for (int i = 0; i < thisRow.Count; ++i)
				{
					SpriteMetaData val = thisRow[i];
					val.name = controls.SpritesheetName + " " + (count + i);
					outSpriteVals.Add(val);
				}
				count += thisRow.Count;

				scratchSpriteVals.Sort(delegate (SpriteMetaData a, SpriteMetaData b) { return b.rect.yMax.CompareTo(a.rect.yMax); });
			}


			textureImporter.spritesheet = outSpriteVals.ToArray();
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		}

		if (GUILayout.Button("Anchor everything bottom center"))
		{
			Texture2D myTexture = controls.GetComponent<SpriteRenderer>().sprite.texture;
			string texturePath = AssetDatabase.GetAssetPath(myTexture);
			TextureImporter textureImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			textureImporter.isReadable = true;

			SpriteMetaData[] spriteVals = textureImporter.spritesheet;
			for (int i = 0; i < spriteVals.Length; ++i)
			{
				SpriteMetaData spriteVal = spriteVals[i];
				spriteVal.alignment = 7; //https://docs.unity3d.com/ScriptReference/SpriteMetaData-alignment.html boo Unity boo
				spriteVals[i] = spriteVal;
			}
			textureImporter.spritesheet = spriteVals;
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		}
	}
}
