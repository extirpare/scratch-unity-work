﻿using System.Xml;

namespace BD
{
	public interface IXmlable
	{
		void WriteToXml(XmlElement parent);
		void ReadFromXml(XmlElement parent);
	}

	//All IXmlables should implement Xmlable attribute. I don't know how to guarantee that.
	public class XmlableAttribute : NamedAttribute
	{
		public XmlableAttribute(string name) : base(name) { }
	}
}
