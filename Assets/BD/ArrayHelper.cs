﻿namespace BD
{
	public static class ArrayHelper
	{
		public static void BDFill<T>(this T[] arr, T val)
		{
			for (int i = 0; i < arr.Length; ++i)
				arr[i] = val;
		}

		public static bool BDContains<T>(this T[] arr, T val)
		{
			return System.Array.FindIndex(arr, x => x.Equals(val)) != -1;
		}
	}
}
