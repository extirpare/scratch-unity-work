﻿using UnityEngine;

[ExecuteInEditMode]
public class BUSortingLayerOverride : MonoBehaviour
{
	public SortingLayer? OverrideSortingLayer = null;

	void Awake()
	{
		bool foundRenderer = false;
		bool foundDifferentValue = false;
		SortingLayer? foundLayer = null;

		foreach (Transform childTransform in this.transform)
		{
			Renderer rendererComponent = childTransform.gameObject.GetComponent<Renderer>();
			if (rendererComponent != null)
			{
				foundRenderer = true;
				SortingLayer thisComponentLayer = System.Array.Find(SortingLayer.layers, x => x.id == rendererComponent.sortingLayerID);
				if (!foundLayer.HasValue)
				{
					foundLayer = thisComponentLayer;
				}
				else if (foundLayer.Value.id != thisComponentLayer.id)
				{
					foundDifferentValue = true;
				}
			}
		}

		if (foundRenderer && !foundDifferentValue)
			OverrideSortingLayer = foundLayer;
		else
			OverrideSortingLayer = null;
	}
}
