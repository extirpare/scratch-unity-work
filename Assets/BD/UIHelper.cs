﻿//This is the [Unity] User Interface helper. UEIHelper is the UnityEditor Inspector helper.

using UnityEngine;
using UnityEngine.UI;

namespace BD
{
	public static class UIHelper
	{
		//ScreenSpace is [0,0] is bottom left and [x,y] is top-right of monitor, where x,y = game resolution
		//I've got canvases set to a reference resolution (probably 1920x1080) and these let me think in 1920x1080 terms
		//regardless of 
		public static BD.Vector2 ScreenSpaceToCanvasSpace(BD.Vector2 screenSpacePos, RectTransform canvasChildTransform)
		{
			BD.Vector2 screenResolution = new BD.Vector2(Screen.width, Screen.height);
			UnityEngine.UI.CanvasScaler canvasScaler = canvasChildTransform.GetComponentInParent<UnityEngine.UI.CanvasScaler>();

			BD.Vector2 ret = screenSpacePos / screenResolution;
			ret *= canvasScaler.referenceResolution;
			return ret;
		}

		public static BD.Vector2 CanvasSpaceToScreenSpace(BD.Vector2 canvasSpacePos, RectTransform canvasChildTransform)
		{
			BD.Vector2 screenResolution = new BD.Vector2(Screen.width, Screen.height);
			UnityEngine.UI.CanvasScaler canvasScaler = canvasChildTransform.GetComponentInParent<UnityEngine.UI.CanvasScaler>();

			BD.Vector2 ret = canvasSpacePos / canvasScaler.referenceResolution;
			ret *= screenResolution;
			return ret;
		}

		public static BD.Vector2 BDCurrentMousePosInCanvasSpace(this CanvasScaler scaler)
		{
			return new BD.Vector2(
				Input.mousePosition.x * scaler.referenceResolution.x / Screen.width,
				Input.mousePosition.y * scaler.referenceResolution.y / Screen.height
			);
		}
	}
}