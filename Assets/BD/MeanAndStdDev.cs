﻿using System;

namespace BD
{
	[Serializable]
	public class MeanAndStdDev
	{
		public float Mean, OneStdDevUp, OneStdDevDown;
		public bool Bounded;
		public float UpperBound, LowerBound;

		public float Sample()
		{
			float ret = BD.Random.RandomGaussian(Mean, OneStdDevDown, OneStdDevUp);
			if (Bounded)
				ret = Maths.Clamp(ret, LowerBound, UpperBound);
			return ret;
		}
	}

}
