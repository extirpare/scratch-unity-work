﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;

namespace BD
{
	public static class FileHelper
	{

		//Note that Directory.GetCurrentDirectory() returns this project's root folder
		public static string[] GlobalURIFolders { get  {return new string[]
			{
				"",
				Application.dataPath + "\\StreamingAssets\\",
				Application.dataPath + "\\StreamingAssets\\Levels\\",
				Application.dataPath + "\\StreamingAssets\\Progression\\",
			};
		} }


		//This is to figure out absolute URIs when people are lazy and assume their folder is a special check-for-global-URIs-from-here folder
		public static string GetAbsoluteURI(string userRelativeURI)
		{
			if (File.Exists(userRelativeURI) || Directory.Exists(userRelativeURI))
				return userRelativeURI;
			foreach (string globalFolder in GlobalURIFolders)
			{
				string uri = globalFolder + userRelativeURI;
				if (File.Exists(uri) || Directory.Exists(uri))
					return uri;
			}

			//hopefully you meant to create a new file here and it's really an absolute URI
			return userRelativeURI;
		}

		public static string GetRelativeURI(string absoluteURI)
		{
			if (!File.Exists(absoluteURI) && !Directory.Exists(absoluteURI))
				return absoluteURI;
				
			string shortestPath = absoluteURI;
			foreach (string folder in GlobalURIFolders)
			{
				if (absoluteURI.IndexOf(folder) == 0 && (absoluteURI.Length - folder.Length) < shortestPath.Length)
					shortestPath = absoluteURI.Substring(folder.Length);
			}

			return shortestPath;
		}


		//quick and dirty helper
		//returns null if can't resolve URI
		public static string GetTextInFile(string uri)
		{
			string absoluteURI = GetAbsoluteURI(uri);
			if (File.Exists(absoluteURI))
			{
				StreamReader sr = new StreamReader(absoluteURI);
				string text = sr.ReadToEnd();
				sr.Close();
				return text;
			}
			return null;
		}

		public static void WriteTextToFile(string text, string uri)
		{
			string absoluteURI = GetAbsoluteURI(uri);
			System.IO.StreamWriter file = new System.IO.StreamWriter(absoluteURI);
			file.WriteLine(text);
			file.Close();
		}

		public static void CreateFileIfNeeded(string fileUri)
		{
			string absoluteUri = GetAbsoluteURI(fileUri);
			if (!File.Exists(absoluteUri))
			{
				FileStream fs = File.Create(absoluteUri);
				fs.Close();
			}
		}

		public static string GetScratchFolderURI()
		{
			Debug.Assert(BD.UHelper.IsInEditMode());
			return Directory.GetCurrentDirectory() + "\\Assets\\Scratch\\";
		}

		public static string GetStreamingAssetsFolderURI()
		{
			return Application.dataPath + "\\StreamingAssets\\";
		}

		public static bool HasParentFolderWithName(string fileUri, string folderName)
		{
			return fileUri.Contains(folderName + "\\") || fileUri.Contains(folderName + "/");
		}

		public static string[] GetFilesOfTypeInDirectory(string directoryURI, string fileType) { return GetFilesOfTypeInDirectory(directoryURI, fileType, false); }
		public static string[] GetFilesOfTypeInDirectory(string directoryURI, string fileType, bool isRecursive)
		{
			string absoluteURI = GetAbsoluteURI(directoryURI);
			string searchPattern = "*." + fileType;
			SearchOption searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
			string[] ret = Directory.GetFiles(absoluteURI, searchPattern, searchOption);
			return ret;
		}


		static char[] k_underscoreTheseChars = { ' ', '-' };
		public static string ToFileName(string displayName)
		{
			char[] invalidPathChars = System.IO.Path.GetInvalidPathChars();
			string ret = displayName.ToLower();

			foreach (char underscoreThisChar in k_underscoreTheseChars)
				ret.Replace(underscoreThisChar, '_');

			for (int i = 0; i < ret.Length; ++i)
				if (invalidPathChars.BDContains(ret[i]))
					ret.Remove(i, 1);

			return ret;
		}
	}
}