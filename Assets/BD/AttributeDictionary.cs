﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

//
//An AttributeDictionary is a singleton for an attribute type
//It lets you iterate over all instances of that attribute!
//

namespace BD
{
	class AttributeDictionary<AttributeType> where AttributeType : Attribute
	{
		//http://stackoverflow.com/questions/3037203/are-static-members-of-a-generic-class-tied-to-the-specific-instance
		private static AttributeDictionary<AttributeType> m_instance;
		public static AttributeDictionary<AttributeType> Get()
		{
			if (m_instance == null)
			{
				m_instance = new AttributeDictionary<AttributeType>();
			}

			return m_instance;
		}

		AttributeDictionary()
		{
			Values = new Dictionary<AttributeType, Type>();

			List<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
			if (!scriptAssemblies.Contains(Assembly.GetExecutingAssembly()))
				scriptAssemblies.Add(Assembly.GetExecutingAssembly());

			foreach (Assembly assembly in scriptAssemblies)
			{
				try
				{
					foreach (Type type in assembly.GetTypes().Where(T => T.IsClass && !T.IsAbstract))
					{
						object[] typeAttributes = type.GetCustomAttributes(typeof(AttributeType), false);
						if (typeAttributes.Length > 0)
						{
							UnityEngine.Debug.Assert(typeAttributes.Length == 1);
							AttributeType attr = (AttributeType)typeAttributes[0];
							UnityEngine.Debug.Assert(!Values.ContainsKey(attr));
							Values.Add(attr, type);
						}
					}
				}
				catch (Exception e)
				{
					//These exceptions happen, they're okay
					if (e is NotSupportedException)
						continue;
				}
			}
		}

		public Dictionary<AttributeType, Type> Values { get; protected set; }
	}
}
