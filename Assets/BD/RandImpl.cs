﻿using System;

namespace BD
{
	public class RandImpl
	{
		//Implementation of xorshift128, taken from http://xorshift.di.unimi.it/xorshift128plus.c
	
		/*  Written in 2014-2015 by Sebastiano Vigna (vigna@acm.org)

			To the extent possible under law, the author has dedicated all copyright
			and related and neighboring rights to this software to the public domain
			worldwide. This software is distributed without any warranty.

			See <http://creativecommons.org/publicdomain/zero/1.0/>.


			This is the fastest generator passing BigCrush without
			systematic failures, but due to the relatively short period it is
			acceptable only for applications with a mild amount of parallelism;
			otherwise, use a xorshift1024* generator.

			The state must be seeded so that it is not everywhere zero. If you have
			a 64-bit seed, we suggest to seed a splitmix64 generator and use its
			output to fill s. */

		//Note that we actually update the seed as we call Next().

		UInt64[] m_seed = new UInt64[2];

		public RandImpl()
		{
			Seed();
		}

		public RandImpl(int seedOffset)
		{
			Seed(seedOffset);
		}

		public void Seed()
		{
			uint seed1 = (uint)DateTime.Now.Ticks;
			uint seed2 = seed1 ^ 0x13371337; //my humor is a national treasure
			uint seed3 = seed2 ^ 0xfab00fab;
			uint seed4 = seed3 ^ 0xdeadbeef;
			UInt64 seedLow = (seed1 << 32) + seed2;
			UInt64 seedHigh = (seed3 << 32) + seed4;
			Seed(seedHigh, seedLow);
		}

		public void Seed(int seedOffset)
		{
			Seed();
			UInt64 doubledOffset = (UInt64)seedOffset << 32;
			doubledOffset += (UInt64)seedOffset;
			m_seed[0] += doubledOffset;
		}

		public void Seed(UInt64 seedTop, UInt64 seedBottom)
		{
			m_seed[0] = seedTop;
			m_seed[1] = seedBottom;
		}

		public UInt64 Next()
		{
			UInt64 val = m_seed[0];
			UInt64 oldSeedBottom = m_seed[1];

			m_seed[0] = oldSeedBottom;
			val ^= val << 23; //this is the bitwise XOR operator
			m_seed[1] = val ^ oldSeedBottom ^ (val >> 18) ^ (oldSeedBottom >> 5);
			return m_seed[1] + val;
		}

		//is 0-int.Max
		public int NextInt()
		{
			return System.Math.Abs( (int)Next() );
		}

		//returns random int in range [0,max)
		public int Next(int max)
		{
			return Next(0, max);
		}

		//returns random int in range [min,max)
		public int Next(int min, int max)
		{
			return min + NextInt() % (max - min);
		}

		//returns random float in range [0,1)
		public float NextFloat()
		{
			float val = (float)NextInt();
			val /= int.MaxValue;
			return val;
		}
	}
}
