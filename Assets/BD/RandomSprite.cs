﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BD;
using Sirenix.OdinInspector;

//Expects there to be a SpriteRenderer or UI.Image on this GameObject
//Replaces their sprite with a random one from here at Start() time

public class RandomSprite : MonoBehaviour 
{
	[Serializable]
	public struct SpriteInfo
	{
		[HideLabel] public Sprite sprite;
		[HorizontalGroup("Flip")] public bool flipX;
		[HorizontalGroup("Flip")] public bool flipY;
	}
	public List<SpriteInfo> PossibleSprites;

	void Start()
	{
		SpriteInfo spriteInfo = BD.Random.FromList(PossibleSprites);
		if (gameObject.BDHasComponent<SpriteRenderer>())
		{
			SpriteRenderer val = gameObject.GetComponent<SpriteRenderer>();
			val.sprite = spriteInfo.sprite;
			val.flipX = spriteInfo.flipX;
			val.flipY = spriteInfo.flipY;
		}
		else if (gameObject.BDHasComponent<Image>())
		{
			Image val = gameObject.GetComponent<Image>();
			val.sprite = spriteInfo.sprite;
			//todo flip x and y if needed
		}
		else
			Debug.LogError("RandomSprite on " + gameObject + " can't find a SpriteRenderer or Image to apply to!");
	}
}
