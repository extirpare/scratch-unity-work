﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

// NOTE: You are only allowed to know prefab information if you're in the Unity Editor (this includes playing from the editor, just not builds).
// So we've got all these #defines to let the functions compile both in and out of the editor. But they're just gonna give the easy answer.

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BD
{
	public static class UHelper
	{
		public static bool IsPrefab(MonoBehaviour monoBehaviour)
		{
			return IsPrefab(monoBehaviour.gameObject);
		}
		public static bool IsPrefab(UnityEngine.Object gameObject)
		{
			#if UNITY_EDITOR
				return gameObject != null && PrefabUtility.GetPrefabType(gameObject) == PrefabType.Prefab;
			#else
				return true;
			#endif
		}

		public static bool AreSamePrefab(GameObject instance1, GameObject instance2)
		{
			#if UNITY_EDITOR
				PrefabType prefabType1 = PrefabUtility.GetPrefabType(instance1);
				PrefabType prefabType2 = PrefabUtility.GetPrefabType(instance2);

				if (!(prefabType1 == PrefabType.ModelPrefab || prefabType1 == PrefabType.ModelPrefabInstance)
					|| !(prefabType2 == PrefabType.ModelPrefab || prefabType2 == PrefabType.ModelPrefabInstance))
				{
					return false;
				}

				return PrefabUtility.GetPrefabObject(instance1) == PrefabUtility.GetPrefabObject(instance2);
			#else
				return true;
			#endif
		}

#if UNITY_EDITOR
		public static bool IsInstanceOfPrefab(GameObject instance, GameObject prefab)
		{
			if (instance == null)
				return false;
				
				PrefabType prefabType = PrefabUtility.GetPrefabType(prefab);
				Debug.Assert(prefabType == PrefabType.Prefab);

				UnityEngine.Object prefabObject = PrefabUtility.GetPrefabObject(instance);
				if (prefabObject == prefab)
					return true;
				else if (BD.Strings.EzContains(instance.name, prefab.name) && BD.Strings.EzContains(instance.name, "Clone"))
					return true;
			return false;
		}
#endif

		public static GameObject TryAndGetNamedObject(string name, bool includeInactiveObjects)
		{
			GameObject[] objectsInScene;
			if (includeInactiveObjects)
				objectsInScene = UnityEngine.Resources.FindObjectsOfTypeAll<GameObject>();
			else
				objectsInScene = UnityEngine.Object.FindObjectsOfType<GameObject>();

			foreach (GameObject obj in objectsInScene)
			{
				if (obj.name == name)
				{
					//activate the inactive object before returning it, this seems like good behavior
					if (includeInactiveObjects && !obj.activeSelf)
						obj.SetActive(true);

					return obj;
				}
			}

			return null;
		}
			
		public static bool IsHoveringUIElement()
		{
			return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
		}
			
		public static T CalcFrontmostHoveredUIElement<T>(string layerName)
			where T : MonoBehaviour
		{
			Debug.Assert(LayerMask.NameToLayer(layerName) >= 0);
			
			EventSystem eventSystem = UnityEngine.EventSystems.EventSystem.current;
			List<RaycastResult> raycastResults = new List<RaycastResult>();
			eventSystem.RaycastAll(new PointerEventData(EventSystem.current), raycastResults);

			if (raycastResults.Count > 0)
			{
				foreach (RaycastResult raycastResult in raycastResults)
				{
					if (raycastResult.gameObject.BDHasComponent<T>())
						return raycastResult.gameObject.GetComponent<T>();
				}
			}

			return null;
		}
			
		public static T CalcFrontmostHoveredWorldElement<T>(string layerName)
			where T : MonoBehaviour
		{
			return CalcFrontmostHoveredWorldElement<T>(layerName, x => true);
		}

		public static T CalcFrontmostHoveredWorldElement<T>(string layerName, Predicate<T> predicate)
			where T : MonoBehaviour
		{
			Debug.Assert(LayerMask.NameToLayer(layerName) >= 0);

			Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D[] rayHits = Physics2D.GetRayIntersectionAll(clickRay, 100, 1 << LayerMask.NameToLayer(layerName));

			if (rayHits.Length > 0)
			{
				List<T> hitComponents = new List<T>();
				foreach (RaycastHit2D rayHit in rayHits)
				{
					T childComponent = rayHit.collider.gameObject.BDGetComponentInSelfOrChildren<T>();
					if (rayHit.collider != null && childComponent != null && predicate(childComponent))
						hitComponents.Add(childComponent);
				}

				if (hitComponents.Count > 0)
				{
					hitComponents.Sort((a, b) => -1 * a.gameObject.BDGetComponentInSelfOrChildren<SpriteRenderer>().sortingOrder.CompareTo(b.gameObject.BDGetComponentInSelfOrChildren<SpriteRenderer>().sortingOrder));
					return hitComponents[0];
				}
			}

			return null;
		}

		//Does its best to instantiate at the values you saved the prefab at, regardless of what its parent is.
		public static T InstantiatePrefabAsChild<T>(T prefabObject, Transform parent)
			where T : UnityEngine.Object
		{
			Debug.Assert(IsPrefab(prefabObject));
			
			Transform prefabTransform = (typeof(T).BDIsOfType<GameObject>()) 
				? (prefabObject as GameObject).transform 
				: (prefabObject as Component).gameObject.transform;

			T newInstance = GameObject.Instantiate(prefabObject, prefabTransform.localPosition, prefabTransform.localRotation, parent) as T;

			Transform newInstanceTransform = (typeof(T).BDIsOfType<GameObject>()) 
				? (newInstance as GameObject).transform 
				: (newInstance as Component).gameObject.transform;

			newInstanceTransform.localScale = prefabTransform.localScale;

			return newInstance;
		}

		//Does its best to instantiate at the values you saved the prefab at, regardless of what its parent is.
		public static T InstantiateUIPrefabAsChild<T>(T prefabObject, RectTransform parent)
			where T : UnityEngine.Object
		{
			Debug.Assert(IsPrefab(prefabObject));
			RectTransform prefabTransform = (typeof(T).BDIsOfType<GameObject>()) 
				? (prefabObject as GameObject).GetComponent<RectTransform>() 
				: (prefabObject as Component).gameObject.GetComponent<RectTransform>();
			
			T newInstance = UnityEngine.Object.Instantiate(prefabObject, prefabTransform.localPosition, prefabTransform.localRotation, parent) as T;

			RectTransform newInstanceTransform = (typeof(T).BDIsOfType<GameObject>()) 
				? (newInstance as GameObject).GetComponent<RectTransform>() 
				: (newInstance as Component).gameObject.GetComponent<RectTransform>();

			newInstanceTransform.localScale = prefabTransform.localScale;
			newInstanceTransform.localPosition = prefabTransform.localPosition;
			newInstanceTransform.localRotation = prefabTransform.localRotation;
			newInstanceTransform.offsetMin = prefabTransform.offsetMin;
			newInstanceTransform.offsetMax = prefabTransform.offsetMax;
			newInstanceTransform.sizeDelta = prefabTransform.sizeDelta;
			return newInstance;
		}

		public static GameObject GetOrInstantiateNamedObject(string name)
		{
			GameObject existingObject = TryAndGetNamedObject(name, true);
			if (existingObject != null)
				return existingObject;
			else

				return new GameObject(name);
		}
		
		public static AnimationClip BDGetAnimationClip(this Animator animator, string name)
		{
			if (animator == null) return null;
		
			foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
				if (clip.name == name)
					return clip;
			
			return null;
		}

		public static BD.Vector2 BDWorldPtToCanvasPt(this Canvas canvas, BD.Vector3 worldPt)
		{
			Camera camera = canvas.worldCamera ?? Camera.main;

			//Vector position (percentage from 0 to 1) considering camera size.
			//For example (0,0) is lower left, middle is (0.5,0.5)
			BD.Vector2 ret = camera.WorldToViewportPoint(worldPt);
	
			//Calculate position considering our percentage, using our canvas size
			//So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
			BD.Vector2 sizeDelta = canvas.GetComponent<RectTransform>().sizeDelta;
			ret.X *= sizeDelta.X;
			ret.Y *= sizeDelta.Y;
	
			return ret;
		}

		public static void DrawColliderHandle(Collider2D collider)
		{
			#if UNITY_EDITOR
				if (collider == null)
					return;

				if (collider.GetType().BDIsOfType(typeof(PolygonCollider2D)))
				{
					PolygonCollider2D polygonCollider = (PolygonCollider2D)collider;
					BD.Vector2 delta = (BD.Vector2)polygonCollider.transform.position + collider.offset;
					for (int i = 0; i < polygonCollider.points.Length; ++i)
					{
						UnityEditor.Handles.DrawLine(polygonCollider.points[i] + delta,
							polygonCollider.points[(i + 1) % polygonCollider.points.Length] + delta);
					}
				}
				else if (collider.GetType().BDIsOfType(typeof(BoxCollider2D)))
				{
					BoxCollider2D boxCollider = (BoxCollider2D)collider;
					BD.Vector2 delta = (BD.Vector2)boxCollider.transform.position + 2 * collider.offset; //dangit unity
					BD.Vector2 halfSize = boxCollider.size; //dangit unity
					UnityEditor.Handles.DrawLine(delta - halfSize, delta - halfSize.NegateX());
					UnityEditor.Handles.DrawLine(delta - halfSize, delta - halfSize.NegateY());
					UnityEditor.Handles.DrawLine(delta + halfSize, delta + halfSize.NegateX());
					UnityEditor.Handles.DrawLine(delta + halfSize, delta + halfSize.NegateY());
				}
			#endif
		}

		public static void GizmosDrawX(BD.Vector2 pos) { GizmosDrawX(pos, 1); }
		public static void GizmosDrawX(BD.Vector2 pos, float scale)
		{
			#if UNITY_EDITOR
				UnityEditor.Handles.color = Gizmos.color;

				float length = HandleUtility.GetHandleSize(pos) * 0.1f * scale;
				BD.Vector2 upRight = BD.NVector2.UpRight * length;
				UnityEditor.Handles.DrawLine(pos + upRight, pos - upRight);
				UnityEditor.Handles.DrawLine(pos + upRight.NegateY(), pos - upRight.NegateY());
			#endif
		}

		public static void GizmosDrawSphereNub(BD.Vector3 pos)
		{
			#if UNITY_EDITOR
				float size = HandleUtility.GetHandleSize(pos) * 0.1f;
				UnityEditor.Handles.SphereHandleCap(0, pos, Quaternion.identity, size, EventType.Repaint);
			#endif
		}

		public static void GizmosDrawRect(BD.Rect rect)
		{
			GizmosDrawRect(rect.TopLeft, rect.BottomRight);
		}

		public static void GizmosDrawRect(BD.Vector2 topLeft, BD.Vector2 bottomRight)
		{
			#if UNITY_EDITOR
				Gizmos.DrawLine(topLeft, new BD.Vector2(bottomRight.X, topLeft.Y));
				Gizmos.DrawLine(topLeft, new BD.Vector2(topLeft.X, bottomRight.Y));
				Gizmos.DrawLine(bottomRight, new BD.Vector2(bottomRight.X, topLeft.Y));
				Gizmos.DrawLine(bottomRight, new BD.Vector2(topLeft.X, bottomRight.Y));
			#endif
		}

		public static void GizmosDrawCircle(BD.Vector2 centerPt, float radius)
		{
			#if UNITY_EDITOR
				Gizmos.DrawWireSphere(centerPt, radius);
			#endif
		}

		public static void GizmosDrawText(BD.Vector2 topLeft, string text)
		{
			#if UNITY_EDITOR
				Handles.Label(topLeft, text, new BD.UStyle(14, BD.Color.Black));
			#endif
		}
		
		public static BD.Rect CalcSpriteWorldLoc(SpriteRenderer spriteRenderer)
        {
            BD.Vector2 centerPt = spriteRenderer.transform.position;
            BD.Vector2 minDelta = spriteRenderer.sprite.bounds.min;
            BD.Vector2 size = spriteRenderer.sprite.bounds.size;
            minDelta *= (BD.Vector2)spriteRenderer.transform.lossyScale;
            size *= (BD.Vector2)spriteRenderer.transform.lossyScale;
            return new BD.Rect(centerPt + minDelta, size);
        }



		public static void SetToCopyOfMaterialIfNeeded(this Renderer renderer, Material sourceMat)
		{
			if (renderer.sharedMaterial == null || renderer.sharedMaterial.shader.name != sourceMat.shader.name)
			{
				renderer.sharedMaterial = new Material(sourceMat);
			}
		}

		public static bool IsInEditMode()
		{
			return Application.isEditor;
		}

		public static bool WasTimePassed(float watchTime) { return WasTimePassed(watchTime, Time.time); }
		public static bool WasTimePassed(float watchTime, float currentTime)
		{
			float oldTime = currentTime - Time.deltaTime;
			return watchTime <= currentTime && watchTime > oldTime;
		}

		public static bool TransformPositionOverTime(Transform transform, float startTime, float duration, BD.Vector3 startPos, BD.Vector3 endPos)
		{
			Debug.Assert(Time.time >= startTime);

			if (Time.time < startTime + duration)
			{
				float normalizedTime = (Time.time - startTime) / duration;
				transform.position = BD.Vector3.Lerp(startPos, endPos, normalizedTime);
				return true;
			}
			
			transform.position = endPos;
			return false;
		}


		public static bool TransformPositionOverTime(Transform transform, float startTime, float duration, BD.Vector3 startPos, BD.Vector3 endPos, AnimationCurve normalizedCurve)
		{
			Debug.Assert(Time.time >= startTime);

			if (Time.time < startTime + duration)
			{
				float normalizedTime = (Time.time - startTime) / duration;
				float normalizedDistance = normalizedCurve.Evaluate(normalizedTime);
				transform.position = BD.Vector3.Lerp(startPos, endPos, normalizedDistance);
				return true;
			}
			
			transform.position = endPos;
			return false;
		}

		public static IEnumerator FadeUICanvas(CanvasGroup canvas, float newAlpha)
		{
			return FadeUICanvas(canvas, newAlpha, 0.25f);
		}

		public static IEnumerator FadeUICanvas(CanvasGroup canvas, float newAlpha, float fadeTime)
		{
			//UI isn't affected by time scale and uses real time
			canvas.gameObject.SetActive(true);
			float startTime = Time.realtimeSinceStartup;
			float startVal = canvas.alpha;
			while (Time.realtimeSinceStartup < startTime + fadeTime)
			{
				float normalizedTime = (Time.realtimeSinceStartup - startTime) / fadeTime;
				canvas.alpha = Maths.Lerp(startVal, newAlpha, normalizedTime);
				yield return null;
			}
			canvas.alpha = newAlpha;
			canvas.interactable = newAlpha > 0;
			canvas.blocksRaycasts = newAlpha > 0;
		}

		public static KeyCode IndexToAlphaKey(int index)
		{
			switch (index)
			{
				case 0:
					return KeyCode.Alpha1;
				case 1:
					return KeyCode.Alpha2;
				case 2:
					return KeyCode.Alpha3;
				case 3:
					return KeyCode.Alpha4;
				case 4:
					return KeyCode.Alpha5;
				case 5:
					return KeyCode.Alpha6;
				case 6:
					return KeyCode.Alpha7;
				case 7:
					return KeyCode.Alpha8;
				case 8:
					return KeyCode.Alpha9;
				default:
					return KeyCode.Alpha0;
			}
		}
	}
}