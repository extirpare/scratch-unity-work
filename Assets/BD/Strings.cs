﻿using System;
using System.Collections.Generic;

namespace BD
{
	public static class Strings

	{
		public static char[] WhitespaceChars = { ' ', '\t', '\n', '\r' };
		public static string InvalidString { get { return "[INVALID]"; } }

		public static string Truncate(string str, int len) { return Truncate(str, len, false); }
		public static string TruncatePr(string str, int len) { return Truncate(str, len, true); }
		public static string Truncate(string str, int len, bool addEllipsis)
		{
			if (str.Length > len)
				return str.Substring(0, len) + (addEllipsis ? "..." : "");
			else
				return str;
		}

		public static bool IsNullOrEmpty(string str)
		{
			return str == null || str.Trim().Length == 0;
		}

		public static string RemoveInstancesOf(string haystack, string needle)
		{
			string[] separators = { needle };
			string[] split = haystack.Split(separators, StringSplitOptions.RemoveEmptyEntries);
			string ret = "";
			foreach (string item in split)
				ret += item;
			return ret;
		}

		public static string ListArray<T>(T[] arr)
		{
			string ret = "";
			for (int i = 0; i < arr.Length; ++i)
			{
				ret += arr[i].ToString();
				if (i != arr.Length - 1)
					ret += ", ";
			}
			return ret;
		}

		public static string GetSharedPrefix(List<string> strings)
		{
			if (strings.Count < 2)
				return "";

			string match = strings[0];
			foreach (string str in strings)
			{
				int matchChar = 0;
				while (str.Length > matchChar && match.Length > matchChar &&
					match[matchChar] == str[matchChar])
				{
					++matchChar;
				}

				match = match.Substring(0, matchChar);
				if (match == "") return match;
			}
			return match;
		}

		public static string GetSharedSuffix(List<string> strings)
		{
			if (strings.Count < 2)
				return "";

			string match = strings[0];
			foreach (string str in strings)
			{
				int matchChar = 0;
				while (str.Length > matchChar && match.Length > matchChar &&
					match[match.Length - matchChar - 1] == str[str.Length - matchChar - 1])
				{
					++matchChar;
				}

				match = match.Substring(match.Length - matchChar);
				if (match == "") return match;
			}
			return match;
		}

		public static void RemoveSharedPrefixAndSuffix(List<string> strings)
		{
			string sharedPrefix = GetSharedPrefix(strings);
			string sharedSuffix = GetSharedSuffix(strings);

			for (int i = 0; i < strings.Count; ++i)
				strings[i] = strings[i].Substring(sharedPrefix.Length, strings[i].Length - sharedPrefix.Length - sharedSuffix.Length);
		}

		public static string[] SplitIntoLines(string input)
		{
			return input.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
		}

		public static string[] SplitByQuotes(string input)
		{
			string[] splits = input.Split('"');
			List<string> ret = new List<string>();
			foreach (string split in splits)
				if (split.Trim().Length > 0)
					ret.Add(split);

			return ret.ToArray();
		}

		public static string GetFirstWord(string input)
		{
			int index = input.IndexOfAny(WhitespaceChars);
			if (index != -1)
				return input.Substring(0, index);
			else
				return input;
		}

		public static BD.Pair<string, string> SplitByFirstWord(string input)
		{
			BD.Pair<string, string> ret = new Pair<string, string>();
			ret.First = GetFirstWord(input).Trim();
			ret.Second = input.Substring(ret.First.Length).Trim();
			return ret;
		}

		public static string[] SplitByCommas(string input)
		{
			string[] ret = input.Split(',');
			for (int i = 0; i < ret.Length; ++i)
				ret[i] = ret[i].Trim();
			return ret;
		}

		public static string[] SplitToWords(string input)
		{
			return input.Split(" \t.".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
		}

		public static string CleanFilename(string filenameStr)
		{
			string ret = RemoveInstancesOf(filenameStr, "\"");
			return ret.Trim();
		}

		public static int[] AllIndexesOf(string str, char toFind) { return AllIndexesOf(str, toFind.ToString()); }
		public static int[] AllIndexesOf(string str, string toFind)
		{
			List<int> indexes = new List<int>();

			int iterIndex = 0;
			while (iterIndex != -1)
			{
				iterIndex = str.IndexOf(toFind, iterIndex);
				if (iterIndex != -1)
				{
					indexes.Add(iterIndex);
					iterIndex += toFind.Length;
				}
			}
			return indexes.ToArray();
		}

		//Returns true if they're pretty much the same
		public static bool EzCompare(string s1, string s2)
		{
			if ( (s1 == null || s2 == null) && !object.ReferenceEquals(s1, s2))
				return false;

			return String.Compare(s1.Trim(), s2.Trim(), true) == 0;
		}

		public static bool EzContains(string haystack, string needle)
		{
			if (haystack == null && needle == null) return true;
			if (haystack == null || needle == null) return false;
			
			string ezHaystack = haystack.ToUpper();
			string ezNeedle = needle.Trim().ToUpper();
			return ezHaystack.Contains(ezNeedle);
		}
	}
}
