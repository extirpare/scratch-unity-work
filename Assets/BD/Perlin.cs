﻿namespace BD
{
	//This is an implementation of Ken Perlin's 2002 Improved Perlin Noise algorithm
	//octaveCount = each octave is double the frequency of the octave before it.
	//amplitudeStrength = each octave is amplitudeStrength amplitude of previous octave.
	//see http://flafla2.github.io/2014/08/09/perlinnoise.html

	public class Perlin
	{
		//int m_octaveCount = 1;
		//float m_amplitudeStrength = 0.5f;	

		const int k_length = 256;
		int[] m_rands;
		
		public Perlin()
		{
			m_rands = Random.ShuffledIndices(k_length);

			float[,] test = new float[32,64];

			for (int y = 0; y < 32; ++y)
			{
				for (int x = 0; x < 64; ++x)
				{
					test[y,x] = PerlinImpl((float)x / 10, (float)y / 10, 0);
				}
			}
		}

		public Perlin(int length, int octaveCount, float amplitudeStrength)
		{
			//m_octaveCount = octaveCount;
			//m_amplitudeStrength = amplitudeStrength;
			m_rands = Random.ShuffledIndices(k_length);
		}

		public float At(float x)
		{
			return At(x, 0, 0);
		}

		public float At(float x, float y)
		{
			return At(x, y, 0);
		}

		public float At(float x, float y, float z)
		{
			return PerlinImpl(x, y, z);
		}

		float PerlinImpl(float x, float y, float z)
		{
			UnityEngine.Debug.Assert(x <= k_length - 1 && y <= k_length - 1 && z <= k_length - 1);
			Pair<int, float> xSplit = Maths.ToIntAndRemainder(x);
			Pair<int, float> ySplit = Maths.ToIntAndRemainder(y);
			Pair<int, float> zSplit = Maths.ToIntAndRemainder(z);

			//hacky
			if (xSplit.First == k_length - 1) { xSplit.First = k_length - 2; xSplit.Second = 1f - BD.Maths.Epsilon; }
			if (ySplit.First == k_length - 1) { ySplit.First = k_length - 2; ySplit.Second = 1f - BD.Maths.Epsilon; }
			if (zSplit.First == k_length - 1) { zSplit.First = k_length - 2; zSplit.Second = 1f - BD.Maths.Epsilon; }

			BD.Vector3 remainders = new BD.Vector3(xSplit.Second, ySplit.Second, zSplit.Second);

			//this hashing function is mega-goofy but it's also one of the core concepts of Perlin noise
			//blah blah philosophically Perlin noise seems like arbitrary decisions without regard to artistic output?? 
			//And I think when folks say "Perlin noise" just mean "octave noise" which isn't even part of the spec??
			//anyhow thanks for reading my code comment rants.

			int hash000, hash001, hash010, hash011, hash100, hash101, hash110, hash111;
			hash000 = m_rands[m_rands[m_rands[xSplit.First    ] + ySplit.First    ] + zSplit.First    ];
			hash001 = m_rands[m_rands[m_rands[xSplit.First    ] + ySplit.First    ] + zSplit.First + 1];
			hash010 = m_rands[m_rands[m_rands[xSplit.First    ] + ySplit.First + 1] + zSplit.First    ];
			hash011 = m_rands[m_rands[m_rands[xSplit.First    ] + ySplit.First + 1] + zSplit.First + 1];
			hash100 = m_rands[m_rands[m_rands[xSplit.First + 1] + ySplit.First    ] + zSplit.First    ];
			hash101 = m_rands[m_rands[m_rands[xSplit.First + 1] + ySplit.First    ] + zSplit.First + 1];
			hash110 = m_rands[m_rands[m_rands[xSplit.First + 1] + ySplit.First + 1] + zSplit.First    ];
			hash111 = m_rands[m_rands[m_rands[xSplit.First + 1] + ySplit.First + 1] + zSplit.First + 1];

			//all these "remainders.SetX(remainders.X-1)" are saying "distance to the +1 part
			//of the unit cube I'm in, not distance from the +0 part of the unit cube" 
			float dot000, dot001, dot010, dot011, dot100, dot101, dot110, dot111;
			dot000 = CalcGradientForHashAndDotAgainstIt(hash000, remainders);
			dot001 = CalcGradientForHashAndDotAgainstIt(hash001, remainders.SetZ(remainders.Z-1));
			dot010 = CalcGradientForHashAndDotAgainstIt(hash010, remainders.SetY(remainders.Y-1));
			dot011 = CalcGradientForHashAndDotAgainstIt(hash011, remainders.SetY(remainders.Y-1).SetZ(remainders.Z-1));
			dot100 = CalcGradientForHashAndDotAgainstIt(hash100, remainders.SetX(remainders.X-1));
			dot101 = CalcGradientForHashAndDotAgainstIt(hash101, remainders.SetX(remainders.X-1).SetZ(remainders.Z-1));
			dot110 = CalcGradientForHashAndDotAgainstIt(hash110, remainders.SetX(remainders.X-1).SetY(remainders.Y-1));
			dot111 = CalcGradientForHashAndDotAgainstIt(hash111, remainders.SetX(remainders.X-1).SetY(remainders.Y-1).SetZ(remainders.Z-1));

			float atY0Z0 = Maths.Lerp(dot000, dot100, xSplit.Second);
			float atY1Z0 = Maths.Lerp(dot010, dot110, xSplit.Second);
			float atZ0 = Maths.Lerp(atY0Z0, atY1Z0, ySplit.Second);

			float atY0Z1 = Maths.Lerp(dot001, dot101, xSplit.Second);
			float atY1Z1 = Maths.Lerp(dot011, dot111, xSplit.Second);
			float atZ1 = Maths.Lerp(atY0Z1, atY1Z1, ySplit.Second);

			float ret = Maths.Lerp(atZ0, atZ1, zSplit.Second);
			return PerlinFade(ret);
		}

		//This is a bit of cuteness by Ken Perlin, I'm just copying it, philosophically I dislike this. But it's cute.
		//hash -> gradient, gradients are always compositions of -1, 1, and 0, with two non-0 parameters.
		//This is just skipping all the steps of explicitly listing and dotting against one of those gradients
		float CalcGradientForHashAndDotAgainstIt(int hash, BD.Vector3 val)
		{
			switch (hash & 0xF)
			{
				case 0x0: return val.X + val.Y;
				case 0x1: return -val.X + val.Y;
				case 0x2: return val.X - val.Y;
				case 0x3: return -val.X - val.Y;
				case 0x4: return val.X + val.Z;
				case 0x5: return -val.X + val.Z;
				case 0x6: return val.X - val.Z;
				case 0x7: return -val.X - val.Z;
				case 0x8: return val.Y + val.Z;
				case 0x9: return -val.Y + val.Z;
				case 0xA: return val.Y - val.Z;
				case 0xB: return -val.Y - val.Z;
				case 0xC: return val.Y + val.X;
				case 0xD: return -val.Y + val.Z;
				case 0xE: return val.Y - val.X;
				case 0xF: return -val.Y - val.Z;
				default: return 0; // never happens
			}
		}

		// 6t^5 - 15t^4 + 10t^3
		// this is an 'S' curve along [0,1] pushing towards 0 and 1 values
		float PerlinFade(float val)
		{
			return val * val * val * (val * (val * 6 - 15) + 10);
		}
	}
}
