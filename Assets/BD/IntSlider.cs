﻿using UnityEngine;
using System;

namespace BD
{
	//Slider can never go above max value, and max value must be > 0
	[Serializable]
	public class IntSlider
	{
		[SerializeField] int m_currentValue = 0;
		[SerializeField] int m_maxValue = 0;

		public IntSlider() { }
		public IntSlider(int val) { m_currentValue = val; m_maxValue = val; }
		public IntSlider(int curr, int max) { m_currentValue = curr; m_maxValue = max; }

		public int CurrentValue { get { return m_currentValue; } set { m_currentValue = BD.Maths.Clamp(value, 0, m_maxValue); } }
		public int MaxValue { get { return m_maxValue; } set { m_maxValue = BD.Maths.Max(0, value); } }

		public float Normalized 
		{ 
			get { return MaxValue > 0 ? (float)CurrentValue / MaxValue : 1; }
			set { CurrentValue = (int)(MaxValue * value); }
		}

		public void ToMax() { CurrentValue = MaxValue; }
		public void ToZero() { CurrentValue = 0; }

		public bool AtZero() { return CurrentValue == 0 && MaxValue != 0; }
		public bool AtMax() { return CurrentValue == MaxValue; }

		public override string ToString()
		{
			return "IntSlider, at " + CurrentValue + " / " + MaxValue + ", norm " + Normalized;
		}
	}
}
