﻿using UnityEngine;

//All this code is taken from http://wiki.unity3d.com/index.php?title=Mathfx 
//Sorry for renaming it to BD.Easing but I already have Mathf and Maths libraries
//I also may have tweaked some of these anyhow so

namespace BD
{
	public static class Easing
	{
		//Real classic Hermite spline, eases in+out, horizontal derivative at 0+1
		//This was made in the 1800s!
		public static float Hermite(float start, float end, float normalizedDist)
		{
			return Mathf.Lerp(start, end, normalizedDist * normalizedDist * (3.0f - 2.0f * normalizedDist));
		}
	
		public static Vector2 Hermite(Vector2 start, Vector2 end, float normalizedDist)
		{
			return new Vector2(Hermite(start.X, end.X, normalizedDist), Hermite(start.Y, end.Y, normalizedDist));
		}
	
		public static Vector3 Hermite(Vector3 start, Vector3 end, float normalizedDist)
		{
			return new Vector3(Hermite(start.X, end.X, normalizedDist), Hermite(start.Y, end.Y, normalizedDist), Hermite(start.Z, end.Z, normalizedDist));
		}
	
		//This just seems like a goofy trick. However, it does ease out. See wiki link at top.
		public static float Sinerp(float start, float end, float normalizedDist)
		{
			return Mathf.Lerp(start, end, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f));
		}
	
		public static Vector2 Sinerp(Vector2 start, Vector2 end, float normalizedDist)
		{
			return new Vector2(Mathf.Lerp(start.X, end.X, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f)), 
				Mathf.Lerp(start.Y, end.Y, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f)));
		}
	
		public static Vector3 Sinerp(Vector3 start, Vector3 end, float normalizedDist)
		{
			return new Vector3(Mathf.Lerp(start.X, end.X, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f)), 
				Mathf.Lerp(start.Y, end.Y, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f)), 
				Mathf.Lerp(start.Z, end.Z, Mathf.Sin(normalizedDist * Mathf.PI * 0.5f)));
		}

		//This just seems like a goofy trick. However, it does ease in. See wiki link at top.
		public static float Coserp(float start, float end, float normalizedDist)
		{
			return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(normalizedDist * Mathf.PI * 0.5f));
		}
	
		public static Vector2 Coserp(Vector2 start, Vector2 end, float normalizedDist)
		{
			return new Vector2(Coserp(start.X,end.X,normalizedDist),
				Coserp(start.Y,end.Y,normalizedDist));
		}
	
		public static Vector3 Coserp(Vector3 start, Vector3 end, float normalizedDist)
		{
			return new Vector3(Coserp(start.X, end.X, normalizedDist), 
				Coserp(start.Y, end.Y, normalizedDist), 
				Coserp(start.Z, end.Z, normalizedDist));
		}
	
		//"Boing-like interpolation". Overshoots then sproings around end value until completing.
		public static float Berp(float start, float end, float normalizedDist)
		{
			normalizedDist = Mathf.Clamp01(normalizedDist);
			normalizedDist = (Mathf.Sin(normalizedDist * Mathf.PI * (0.2f + 2.5f * normalizedDist * normalizedDist * normalizedDist)) 
				* Mathf.Pow(1f - normalizedDist, 2.2f) + normalizedDist) 
				* (1f + (1.2f * (1f - normalizedDist)));
			return start + (end - start) * normalizedDist;
		}
	
		public static Vector2 Berp(Vector2 start, Vector2 end, float normalizedDist)
		{
			return new Vector2(Berp(start.X,end.X,normalizedDist), Berp(start.Y, end.Y, normalizedDist));
		}
	
		public static Vector3 Berp(Vector3 start, Vector3 end, float normalizedDist)
		{
			return new Vector3(Berp(start.X, end.X, normalizedDist), Berp(start.Y, end.Y, normalizedDist), Berp(start.Z, end.Z, normalizedDist));
		}
	
		public static float Lerp(float start, float end, float normalizedDist)
		{
			return ((1.0f - normalizedDist) * start) + (normalizedDist * end);
		}
	
		/*
		* CLerp - Circular Lerp - is like lerp but handles the wraparound from 0 to 360.
		* This is useful when interpolating eulerAngles and the object
		* crosses the 0/360 boundary.  The standard Lerp function causes the object
		* to rotate in the wrong direction and looks stupid. Clerp fixes that.
		*/
		public static float Clerp(float startDeg, float endDeg, float normalizedDist)
		{
			float min = 0.0f;
			float max = 360.0f;
			float half = Mathf.Abs((max - min) / 2.0f);//half the distance between min and max
			float ret = 0.0f;
			float diff = 0.0f;
	
			if ((endDeg - startDeg) < -half)
			{
				diff = ((max - startDeg) + endDeg) * normalizedDist;
				ret = startDeg + diff;
			}
			else if ((endDeg - startDeg) > half)
			{
				diff = -((max - endDeg) + startDeg) * normalizedDist;
				ret = startDeg + diff;
			}
			else ret = startDeg + (endDeg - startDeg) * normalizedDist;
	
			// Debug.Log("Start: "  + start + "   End: " + end + "  Value: " + value + "  Half: " + half + "  Diff: " + diff + "  Retval: " + retval);
			return ret;
		}
	}
}
