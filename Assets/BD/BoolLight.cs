﻿using UnityEngine;
using UnityEngine.UI;

public class BoolLight : MonoBehaviour 
{
	public Image Light;
	public BD.Color OnColor = BD.Color.White;
	public BD.Color OffColor = BD.Color.ZeroAlpha;
	public bool Value;
	
	void Update()
	{
		Light.color = Value ? OnColor : OffColor;
	}
}
