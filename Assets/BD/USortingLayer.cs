﻿using UnityEngine;

[System.Serializable]
public class USortingLayer
{
	public USortingLayer(SortingLayer layer) { LayerID = layer.id; }
	public int LayerID;
	public static implicit operator USortingLayer(SortingLayer val) { return new USortingLayer(val); }

	public static SortingLayer LayerWithID(int id)
	{
		SortingLayer[] sortingLayers = SortingLayer.layers;
		int index = System.Array.FindIndex(sortingLayers, x => x.id == id);
		UnityEngine.Debug.Assert(index != -1);
		return sortingLayers[index];
	}
}
