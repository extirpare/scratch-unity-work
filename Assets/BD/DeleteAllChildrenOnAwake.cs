﻿using UnityEngine;
using BD;

public class DeleteAllChildrenOnAwake : MonoBehaviour 
{
	void Awake() 
	{
		this.transform.BDDeleteAllChildren();
		GameObject.Destroy(this); //only destroys my own DeleteAllChildrenOnAwake monobehaviour
	}
}
