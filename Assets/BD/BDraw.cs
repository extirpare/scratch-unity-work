﻿using System;
using UnityEngine;

namespace BD
{
	public static class BDraw
	{
		static bool k_usingSimpleTextureMat = false;
		//static readonly Shader k_simpleTextureShader = Shader.Find("Editor/Cable/Resources/TextureAlpha");
		//static readonly Material k_simpleTextureMat = GenerateUnlitTextureMaterial();
		public static Material GenerateUnlitTextureMaterial()
		{
			Material ret;
			ret = new Material(Shader.Find("BD/SimpleAlphaTexture"));
			return ret;
		}

		public static Texture2D ColorToTex(BD.Color color)
		{
			Texture2D tex = new Texture2D(1, 1);
			tex.SetPixel(1, 1, color);
			tex.Apply();
			return tex;
		}

		public static void Start(BD.Color color) { Start(color, GL.QUADS); }
		public static void Start(BD.Color color, int glType)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(!k_usingSimpleTextureMat);
			k_usingSimpleTextureMat = true;

			Material mat = GenerateUnlitTextureMaterial();

			mat.SetColor("_Color", color);
			mat.SetTexture("_MainTex", ColorToTex(BD.Color.White.AtAlpha(color.A)));

			GL.Begin(glType);
			mat.SetPass(0);
		}

		public static void Start(Texture2D texture) { Start(texture, GL.QUADS); }
		public static void Start(Texture2D texture, int glType)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(!k_usingSimpleTextureMat);
			k_usingSimpleTextureMat = true;

			Material mat = GenerateUnlitTextureMaterial();

			mat.SetColor("_Color", BD.Color.White);
			mat.SetTexture("_MainTex", texture);

			GL.Begin(glType);
			mat.SetPass(0);
		}


        public static void BatchedLine(BD.Vector2 startPt, BD.Vector2 endPt, float lineWidth)
		{
			if (Event.current.type != EventType.Repaint) return;
			if (startPt == endPt) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);

			BD.Vector2 leftmostPt = (startPt.X > endPt.X) ? endPt : startPt;
			BD.Vector2 rightmostPt = (leftmostPt == startPt) ? endPt : startPt;

			BD.Vector2 scaledTangent = (endPt - startPt).GetPerpendicularNVector() * lineWidth / 2f;
			BD.Vector2 upTangent = (scaledTangent.Y > 0 || (scaledTangent.Y == 0 && scaledTangent.X > 0)) ? -scaledTangent : scaledTangent;

			GL.TexCoord2(0, 1);
			GL.Vertex(leftmostPt + upTangent);
			GL.TexCoord2(1, 1);
			GL.Vertex(rightmostPt + upTangent);
			GL.TexCoord2(1, 0);
			GL.Vertex(rightmostPt - upTangent);
			GL.TexCoord2(0, 0);
			GL.Vertex(leftmostPt - upTangent);
		}

		public static void BatchedRect(BD.Rect location)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);

			GL.TexCoord2(0, 1);
			GL.Vertex(location.TopLeft);
			GL.TexCoord2(1, 1);
			GL.Vertex(location.TopRight);
			GL.TexCoord2(1, 0);
			GL.Vertex(location.BottomRight);
			GL.TexCoord2(0, 0);
			GL.Vertex(location.BottomLeft);
		}

		//Give verts in clockwise order
		public static void BatchedVerts(BD.Vector2[] locations)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);

			//This assumes a lot about our verts and that we're doing non-strip triangles
			for (int i = 1; i < locations.Length - 1; i += 1)
			{
				GL.Vertex(locations[0]);
				GL.Vertex(locations[i]);
				GL.Vertex(locations[i+1]);
			}
		}

		public static void BatchedRect(BD.Rect location, float edgeRadius, eQuarter roundedCorners)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);
			if (edgeRadius == 0)
			{
				BatchedRect(location);
				return;
			}

			BatchedRect(location.ContractHoriz(edgeRadius));
			BatchedRect(location.ContractVert(edgeRadius));
			BD.Vector2 contractAmt = new BD.Vector2(edgeRadius);

			if ((roundedCorners & eQuarter.TopRight) != 0)
				BatchedQuarterCircle(eQuarter.TopRight, location.TopRight + contractAmt.PointDownLeft(), edgeRadius);
			else
				BatchedRect(new BD.Rect(location.TopRight.MoveLeft(edgeRadius), contractAmt));

			if ((roundedCorners & eQuarter.TopLeft) != 0)
				BatchedQuarterCircle(eQuarter.TopLeft, location.TopLeft + contractAmt.PointDownRight(), edgeRadius);
			else
				BatchedRect(new BD.Rect(location.TopLeft, contractAmt));

			if ((roundedCorners & eQuarter.BottomRight) != 0)
				BatchedQuarterCircle(eQuarter.BottomRight, location.BottomRight + contractAmt.PointUpLeft(), edgeRadius);
			else
				BatchedRect(new BD.Rect(location.BottomRight + contractAmt.PointUpLeft(), contractAmt));

			if ((roundedCorners & eQuarter.BottomLeft) != 0)
				BatchedQuarterCircle(eQuarter.BottomLeft, location.BottomLeft + contractAmt.PointUpRight(), edgeRadius);
			else
				BatchedRect(new BD.Rect(location.BottomLeft.MoveUp(edgeRadius), contractAmt));
		}

		[Flags]
		public enum eQuarter
		{
			TopLeft = 1,
			TopRight = 2,
			BottomLeft = 4,
			BottomRight = 8
		}
		public static void BatchedQuarterCircle(eQuarter quarter, BD.Vector2 pt, float radius)
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);

			//everything is in GL.Quads, which I regret here.
			const int k_segments = 12;
			for (int i = 0; i < k_segments; i += 2)
			{
				GL.Vertex(pt);
				for (int j = 0; j < 3; ++j)
				{
					float vertNum = i+j;
					float radians = (vertNum / (4 * k_segments)) * BD.Maths.TwoPi;
					BD.Vector2 circlePt = new BD.Vector2(radius * Mathf.Cos(radians), radius * Mathf.Sin(radians));
					switch (quarter)
					{
						case eQuarter.TopLeft:
							circlePt = circlePt.Negate(); break;
						case eQuarter.TopRight:
							circlePt = circlePt.NegateY(); break;
						case eQuarter.BottomLeft:
							circlePt = circlePt.NegateX(); break;
						case eQuarter.BottomRight:
							break;
					}
					GL.Vertex(pt + circlePt);
				}
			}
		}

		public static void End()
		{
			if (Event.current.type != EventType.Repaint) return;
			UnityEngine.Debug.Assert(k_usingSimpleTextureMat);
			k_usingSimpleTextureMat = false;

			GL.End();
		}

		public static void Text(BD.Vector2 loc, string text) { Text(new BD.Rect(loc, 100, 100), text); }
		public static void Text(BD.Vector2 loc, string text, UnityEngine.GUIStyle style) { Text(new BD.Rect(loc, 100, 100), text, style); }
		public static void Text(BD.Rect loc, string text) { Text(loc, text, BD.UStyle.LabelStyle); }
		public static void Text(BD.Rect loc, string text, UnityEngine.GUIStyle style)
		{
			//UnityEngine.GUI.BeginGroup(loc);
			UnityEngine.GUI.Label(loc, text, style);
			//UnityEngine.GUI.EndGroup();
		}

		public static void Line(BD.Vector2 startPt, BD.Vector2 endPt, BD.Color color) { Line(startPt, endPt, 1f, color); }
		public static void Line(BD.Vector2 startPt, BD.Vector2 endPt, float lineWidth, BD.Color color)
		{
			Start(color);
			BatchedLine(startPt, endPt, lineWidth);
			End();
		}

		public static void Arrow(BD.Vector2 startPt, BD.Vector2 endPt, BD.Color color) { Arrow(startPt, endPt, 1f, color); }
		public static void Arrow(BD.Vector2 startPt, BD.Vector2 endPt, float lineWidth, BD.Color color)
		{
			Start(color);
			BatchedLine(startPt, endPt, lineWidth);
			Vector2 arrowAmt = (endPt - startPt) * 0.2f;
			BatchedLine(endPt, endPt - arrowAmt.Rotate(30), lineWidth);
			BatchedLine(endPt, endPt - arrowAmt.Rotate(-30), lineWidth);
			End();
		}

		public static void Rect(BD.Rect location, BD.Color color)
		{
			Start(color);
			BatchedRect(location);
			End();
		}

		public static void Rect(BD.Rect location, BD.Color color, float edgeRoundAmt)
		{
			Start(color);
			BatchedRect(location, edgeRoundAmt, eQuarter.TopLeft | eQuarter.TopRight | eQuarter.BottomLeft | eQuarter.BottomRight);
			End();
		}

		public static void Rect(BD.Rect location, BD.Color color, float edgeRoundAmt, eQuarter roundedCorners)
		{
			Start(color);
			BatchedRect(location, edgeRoundAmt, roundedCorners);
			End();
		}

		public static void Verts(BD.Vector2[] verts, BD.Color color)
		{
			Start(color, GL.TRIANGLES);
			BatchedVerts(verts);
			End();
		}

		public static void CircleOutline(BD.Vector2 centerPt, float radius, BD.Color color) { CircleOutline(centerPt, radius, 1f, color); }
		public static void CircleOutline(BD.Vector2 centerPt, float radius, float width, BD.Color color)
		{
			Start(color);

			const int k_circleLineSegments = 36;
			BD.Vector2? prevPt = null;
			for (int i = 0; i <= k_circleLineSegments; ++i)
			{
				float radians = (float)i * BD.Maths.TwoPi / k_circleLineSegments;
				float sin = Mathf.Sin(radians);
				float cos = Mathf.Cos(radians);
				BD.Vector2 currPt = centerPt + radius * new BD.Vector2(cos, sin);
				if (prevPt != null) BatchedLine(prevPt.Value, currPt, width);
				prevPt = currPt;
			}

			End();
		}

		//Draws such that inside of line is on rect
		public static void RectOutline(BD.Rect rect, BD.Color color) { RectOutline(rect, 1f, color); }
		public static void RectOutline(BD.Rect rect, float lineWidth, BD.Color color)
		{
			BD.Rect outerEdge = rect.Expand(lineWidth);
			BD.Rect center = rect.Expand(lineWidth / 2);
			Start(color);
			BatchedLine(new BD.Vector2(outerEdge.Left, center.Top), new BD.Vector2(outerEdge.Right, center.Top), lineWidth);
			BatchedLine(new BD.Vector2(outerEdge.Left, center.Bottom), new BD.Vector2(outerEdge.Right, center.Bottom), lineWidth);
			BatchedLine(new BD.Vector2(center.Left, rect.Top), new BD.Vector2(center.Left, rect.Bottom), lineWidth);
			BatchedLine(new BD.Vector2(center.Right, rect.Top), new BD.Vector2(center.Right, rect.Bottom), lineWidth);
			End();
		}

		public static void Lines(BD.Vector2[] segments, float lineWidth, BD.Color color, bool connectLastToFirst)
		{
			Start(color);
			for (int i = 0; i < segments.Length - 1; ++i)
				BatchedLine(segments[i], segments[i+1], lineWidth);

			if (connectLastToFirst)
				BatchedLine(segments[segments.Length - 1], segments[0], lineWidth);

			End();
		}

		public static void Bezier(BD.Bezier2D curve, BD.Color color)
		{
			Start(color);
			const int k_bezierLineSegments = 10;

			BD.Vector2 prevPt = curve.StartPt;
			for (int i = 1; i <= k_bezierLineSegments; ++i)
			{
				BD.Vector2 curPt = curve.PositionAlongCurve((float)i/k_bezierLineSegments);
				BatchedLine(prevPt, curPt, 3f);
				prevPt = curPt;
			}

			End();
		}

		public static void Texture(Texture2D tex, BD.Rect location)
		{
			Start(tex);
			BatchedRect(location);
			End();
		}
	}
}
