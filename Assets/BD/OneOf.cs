﻿using System;
using System.Xml;

namespace BD
{

	// 
	// ___       __     ___      __   ___  __  
	//  |  |  | /  \     |  \ / |__) |__  /__` 
	//  |  |/\| \__/     |   |  |    |___ .__/ 
	// ( http://patorjk.com/software/taag/#p=display&h=0&f=Stick%20Letters&t=ONE%20TYPE )
	//

	[Xmlable("OneOf")]
	public abstract class OneOf : IXmlable
	{
		public OneOf() { }

		public abstract void ReadFromXml(XmlElement parent);
		public abstract void WriteToXml(XmlElement parent);

		public abstract bool CanBeA<T>();
		public abstract bool IsA<T>();
		public abstract bool IsA(Type type);

		public abstract T As<T>() where T : class;
	}

	[OneOf("TwoType")]
	public class OneOf<Type1, Type2> : OneOf
		where Type1 : class, IXmlable, new()
		where Type2 : class, IXmlable, new()
	{
		public OneOf() { }
		public OneOf(Type1 valType1) { m_valType1 = valType1; }
		public OneOf(Type2 valType2) { m_valType2 = valType2; }

		public static implicit operator OneOf<Type1, Type2>(Type1 val) { return new OneOf<Type1, Type2>(val); }
		public static implicit operator OneOf<Type1, Type2>(Type2 val) { return new OneOf<Type1, Type2>(val); }
		public static implicit operator Type1(OneOf<Type1, Type2> val) { UnityEngine.Debug.Assert(val.IsA<Type1>()); return val.As<Type1>(); }
		public static implicit operator Type2(OneOf<Type1, Type2> val) { UnityEngine.Debug.Assert(val.IsA<Type2>()); return val.As<Type2>(); }

		public static bool operator !=(OneOf<Type1, Type2> val, Type1 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2> val, Type2 comp) { return !(val == comp); }
		public static bool operator ==(OneOf<Type1, Type2> val, Type1 comp) { return val.IsA<Type1>() && val.As<Type1>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2> val, Type2 comp) { return val.IsA<Type2>() && val.As<Type2>() == comp; }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool CanBeA<T>() { return typeof(Type1) == typeof(T) || typeof(Type2) == typeof(T); }

		public override bool IsA<T>() { return IsA(typeof(T)); }
		public override bool IsA(Type type) {
			if (m_valType1 != null) { return typeof(Type1).BDIsOfType(type); }
			if (m_valType2 != null) { return typeof(Type2).BDIsOfType(type); }
			return false;
		}

		public override RetType As<RetType>()
		{
			if (IsA<Type1>() && typeof(Type1).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType1, typeof(RetType));
			if (IsA<Type2>() && typeof(Type2).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType2, typeof(RetType));
			return null;
		}

		public override string ToString()
		{
			if (IsA<Type1>()) { return typeof(Type1).ToString() + ": " + As<Type1>().ToString(); }
			if (IsA<Type2>()) { return typeof(Type2).ToString() + ": " + As<Type2>().ToString(); }
			return "NO TYPES; ALL IS LOST; HAIL SATAN"; //what's an exception
		}

		public override void WriteToXml(XmlElement parent)
		{
			if (IsA<Type1>()) { parent.BDWriteAttr("Type", typeof(Type1).ToString()).BDWriteChild(As<Type1>()); }
			else if (IsA<Type2>()) { parent.BDWriteAttr("Type", typeof(Type2).ToString()).BDWriteChild(As<Type2>()); }
		}

		public override void ReadFromXml(XmlElement parent)
		{
			string type = "";
			parent.BDReadAttr("Type", ref type);
			if (type == typeof(Type1).ToString()) { m_valType1 = new Type1(); m_valType1.ReadFromXml(parent); }
			else if (type == typeof(Type2).ToString()) { m_valType2 = new Type2(); m_valType2.ReadFromXml(parent); }
			else UnityEngine.Debug.Assert(false);
		}

		Type1 m_valType1 = null;
		Type2 m_valType2 = null;
	}



	// ___       __   ___  ___    ___      __   ___  __  
	//  |  |__| |__) |__  |__      |  \ / |__) |__  /__` 
	//  |  |  | |  \ |___ |___     |   |  |    |___.__/ 
	//

	[OneOf("ThreeType")]
	public class OneOf<Type1, Type2, Type3> : OneOf
		where Type1 : class, IXmlable, new()
		where Type2 : class, IXmlable, new()
		where Type3 : class, IXmlable, new()
	{
		public OneOf() { }
		public OneOf(Type1 valType1) { m_valType1 = valType1; }
		public OneOf(Type2 valType2) { m_valType2 = valType2; }
		public OneOf(Type3 valType3) { m_valType3 = valType3; }

		public static implicit operator OneOf<Type1, Type2, Type3>(Type1 val) { return new OneOf<Type1, Type2, Type3>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3>(Type2 val) { return new OneOf<Type1, Type2, Type3>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3>(Type3 val) { return new OneOf<Type1, Type2, Type3>(val); }
		public static implicit operator Type1(OneOf<Type1, Type2, Type3> val) { UnityEngine.Debug.Assert(val.IsA<Type1>()); return val.As<Type1>(); }
		public static implicit operator Type2(OneOf<Type1, Type2, Type3> val) { UnityEngine.Debug.Assert(val.IsA<Type2>()); return val.As<Type2>(); }
		public static implicit operator Type3(OneOf<Type1, Type2, Type3> val) { UnityEngine.Debug.Assert(val.IsA<Type3>()); return val.As<Type3>(); }

		public static bool operator !=(OneOf<Type1, Type2, Type3> val, Type1 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3> val, Type2 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3> val, Type3 comp) { return !(val == comp); }
		public static bool operator ==(OneOf<Type1, Type2, Type3> val, Type1 comp) { return val.IsA<Type1>() && val.As<Type1>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3> val, Type2 comp) { return val.IsA<Type2>() && val.As<Type2>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3> val, Type3 comp) { return val.IsA<Type3>() && val.As<Type3>() == comp; }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool CanBeA<T>() { return typeof(Type1) == typeof(T) || typeof(Type2) == typeof(T) || typeof(Type3) == typeof(T); }

		public override bool IsA<T>() { return IsA(typeof(T)); }
		public override bool IsA(Type type)
		{
			if (m_valType1 != null) { return typeof(Type1).BDIsOfType(type); }
			if (m_valType2 != null) { return typeof(Type2).BDIsOfType(type); }
			if (m_valType3 != null) { return typeof(Type3).BDIsOfType(type); }
			return false;
		}

		public override RetType As<RetType>()
		{
			if (IsA<Type1>() && typeof(Type1).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType1, typeof(RetType));
			if (IsA<Type2>() && typeof(Type2).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType2, typeof(RetType));
			if (IsA<Type3>() && typeof(Type3).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType3, typeof(RetType));
			return null;
		}

		public override string ToString()
		{
			if (IsA<Type1>()) { return typeof(Type1).ToString() + ": " + As<Type1>().ToString(); }
			if (IsA<Type2>()) { return typeof(Type2).ToString() + ": " + As<Type2>().ToString(); }
			if (IsA<Type3>()) { return typeof(Type3).ToString() + ": " + As<Type3>().ToString(); }
			return "NO TYPES; ALL IS LOST; HAIL SATAN"; //what's an exception
		}

		public override void WriteToXml(XmlElement parent)
		{
			if (IsA<Type1>()) { parent.BDWriteAttr("Type", typeof(Type1).ToString()).BDWriteChild(As<Type1>()); }
			else if (IsA<Type2>()) { parent.BDWriteAttr("Type", typeof(Type2).ToString()).BDWriteChild(As<Type2>()); }
			else if (IsA<Type3>()) { parent.BDWriteAttr("Type", typeof(Type3).ToString()).BDWriteChild(As<Type3>()); }
		}

		public override void ReadFromXml(XmlElement parent)
		{
			string type = "";
			parent.BDReadAttr("Type", ref type);
			if (type == typeof(Type1).ToString()) { m_valType1 = new Type1(); m_valType1.ReadFromXml(parent); }
			else if (type == typeof(Type2).ToString()) { m_valType2 = new Type2(); m_valType2.ReadFromXml(parent); }
			else if (type == typeof(Type3).ToString()) { m_valType3 = new Type3(); m_valType3.ReadFromXml(parent); }
			else UnityEngine.Debug.Assert(false);
		}

		Type1 m_valType1 = null;
		Type2 m_valType2 = null;
		Type3 m_valType3 = null;
	}



	//  ___  __        __     ___      __   ___  __  
	// |__  /  \ |  | |__)     |  \ / |__) |__  /__` 
	// |    \__/ \__/ |  \     |   |  |    |___.__/ 
	//

	[OneOf("FourType")]
	public class OneOf<Type1, Type2, Type3, Type4> : OneOf
		where Type1 : class, IXmlable, new()
		where Type2 : class, IXmlable, new()
		where Type3 : class, IXmlable, new()
		where Type4 : class, IXmlable, new()
	{
		public OneOf() { }
		public OneOf(Type1 valType1) { m_valType1 = valType1; }
		public OneOf(Type2 valType2) { m_valType2 = valType2; }
		public OneOf(Type3 valType3) { m_valType3 = valType3; }
		public OneOf(Type4 valType4) { m_valType4 = valType4; }

		public static implicit operator OneOf<Type1, Type2, Type3, Type4>(Type1 val) { return new OneOf<Type1, Type2, Type3, Type4>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4>(Type2 val) { return new OneOf<Type1, Type2, Type3, Type4>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4>(Type3 val) { return new OneOf<Type1, Type2, Type3, Type4>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4>(Type4 val) { return new OneOf<Type1, Type2, Type3, Type4>(val); }
		public static implicit operator Type1(OneOf<Type1, Type2, Type3, Type4> val) { UnityEngine.Debug.Assert(val.IsA<Type1>()); return val.As<Type1>(); }
		public static implicit operator Type2(OneOf<Type1, Type2, Type3, Type4> val) { UnityEngine.Debug.Assert(val.IsA<Type2>()); return val.As<Type2>(); }
		public static implicit operator Type3(OneOf<Type1, Type2, Type3, Type4> val) { UnityEngine.Debug.Assert(val.IsA<Type3>()); return val.As<Type3>(); }
		public static implicit operator Type4(OneOf<Type1, Type2, Type3, Type4> val) { UnityEngine.Debug.Assert(val.IsA<Type4>()); return val.As<Type4>(); }

		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4> val, Type1 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4> val, Type2 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4> val, Type3 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4> val, Type4 comp) { return !(val == comp); }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4> val, Type1 comp) { return val.IsA<Type1>() && val.As<Type1>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4> val, Type2 comp) { return val.IsA<Type2>() && val.As<Type2>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4> val, Type3 comp) { return val.IsA<Type3>() && val.As<Type3>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4> val, Type4 comp) { return val.IsA<Type4>() && val.As<Type4>() == comp; }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool CanBeA<T>() { return typeof(Type1) == typeof(T) || typeof(Type2) == typeof(T) || typeof(Type3) == typeof(T) || typeof(Type4) == typeof(T); }

		public override bool IsA<T>() { return IsA(typeof(T)); }
		public override bool IsA(Type type)
		{
			if (m_valType1 != null) { return typeof(Type1).BDIsOfType(type); }
			if (m_valType2 != null) { return typeof(Type2).BDIsOfType(type); }
			if (m_valType3 != null) { return typeof(Type3).BDIsOfType(type); }
			if (m_valType4 != null) { return typeof(Type4).BDIsOfType(type); }
			return false;
		}

		public override RetType As<RetType>()
		{
			if (IsA<Type1>() && typeof(Type1).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType1, typeof(RetType));
			if (IsA<Type2>() && typeof(Type2).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType2, typeof(RetType));
			if (IsA<Type3>() && typeof(Type3).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType3, typeof(RetType));
			if (IsA<Type4>() && typeof(Type4).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType4, typeof(RetType));
			return null;
		}

		public override string ToString()
		{
			if (IsA<Type1>()) { return typeof(Type1).ToString() + ": " + As<Type1>().ToString(); }
			if (IsA<Type2>()) { return typeof(Type2).ToString() + ": " + As<Type2>().ToString(); }
			if (IsA<Type3>()) { return typeof(Type3).ToString() + ": " + As<Type3>().ToString(); }
			if (IsA<Type4>()) { return typeof(Type4).ToString() + ": " + As<Type4>().ToString(); }
			return "NO TYPES; ALL IS LOST; HAIL SATAN"; //what's an exception
		}

		public override void WriteToXml(XmlElement parent)
		{
			if (IsA<Type1>()) { parent.BDWriteAttr("Type", typeof(Type1).ToString()).BDWriteChild(As<Type1>()); }
			else if (IsA<Type2>()) { parent.BDWriteAttr("Type", typeof(Type2).ToString()).BDWriteChild(As<Type2>()); }
			else if (IsA<Type3>()) { parent.BDWriteAttr("Type", typeof(Type3).ToString()).BDWriteChild(As<Type3>()); }
			else if (IsA<Type4>()) { parent.BDWriteAttr("Type", typeof(Type4).ToString()).BDWriteChild(As<Type4>()); }
		}

		public override void ReadFromXml(XmlElement parent)
		{
			string type = "";
			parent.BDReadAttr("Type", ref type);
			if (type == typeof(Type1).ToString()) { m_valType1 = new Type1(); m_valType1.ReadFromXml(parent); }
			else if (type == typeof(Type2).ToString()) { m_valType2 = new Type2(); m_valType2.ReadFromXml(parent); }
			else if (type == typeof(Type3).ToString()) { m_valType3 = new Type3(); m_valType3.ReadFromXml(parent); }
			else if (type == typeof(Type4).ToString()) { m_valType4 = new Type4(); m_valType4.ReadFromXml(parent); }
			else UnityEngine.Debug.Assert(false);
		}

		Type1 m_valType1 = null;
		Type2 m_valType2 = null;
		Type3 m_valType3 = null;
		Type4 m_valType4 = null;
	}



	//
	// ___          ___    ___      __   ___  __
	// |__  | \  / |__      |  \ / |__) |__  /__` 
	// |    |  \/  |___     |   |  |    |___.__/ 
	//

	[OneOf("FiveType")]
	public class OneOf<Type1, Type2, Type3, Type4, Type5> : OneOf
		where Type1 : class, IXmlable, new()
		where Type2 : class, IXmlable, new()
		where Type3 : class, IXmlable, new()
		where Type4 : class, IXmlable, new()
		where Type5 : class, IXmlable, new()
	{
		public OneOf() { }
		public OneOf(Type1 valType1) { m_valType1 = valType1; }
		public OneOf(Type2 valType2) { m_valType2 = valType2; }
		public OneOf(Type3 valType3) { m_valType3 = valType3; }
		public OneOf(Type4 valType4) { m_valType4 = valType4; }
		public OneOf(Type5 valType5) { m_valType5 = valType5; }

		public static implicit operator OneOf<Type1, Type2, Type3, Type4, Type5>(Type1 val) { return new OneOf<Type1, Type2, Type3, Type4, Type5>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4, Type5>(Type2 val) { return new OneOf<Type1, Type2, Type3, Type4, Type5>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4, Type5>(Type3 val) { return new OneOf<Type1, Type2, Type3, Type4, Type5>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4, Type5>(Type4 val) { return new OneOf<Type1, Type2, Type3, Type4, Type5>(val); }
		public static implicit operator OneOf<Type1, Type2, Type3, Type4, Type5>(Type5 val) { return new OneOf<Type1, Type2, Type3, Type4, Type5>(val); }
		public static implicit operator Type1(OneOf<Type1, Type2, Type3, Type4, Type5> val) { UnityEngine.Debug.Assert(val.IsA<Type1>()); return val.As<Type1>(); }
		public static implicit operator Type2(OneOf<Type1, Type2, Type3, Type4, Type5> val) { UnityEngine.Debug.Assert(val.IsA<Type2>()); return val.As<Type2>(); }
		public static implicit operator Type3(OneOf<Type1, Type2, Type3, Type4, Type5> val) { UnityEngine.Debug.Assert(val.IsA<Type3>()); return val.As<Type3>(); }
		public static implicit operator Type4(OneOf<Type1, Type2, Type3, Type4, Type5> val) { UnityEngine.Debug.Assert(val.IsA<Type4>()); return val.As<Type4>(); }
		public static implicit operator Type5(OneOf<Type1, Type2, Type3, Type4, Type5> val) { UnityEngine.Debug.Assert(val.IsA<Type5>()); return val.As<Type5>(); }

		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type1 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type2 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type3 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type4 comp) { return !(val == comp); }
		public static bool operator !=(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type5 comp) { return !(val == comp); }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type1 comp) { return val.IsA<Type1>() && val.As<Type1>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type2 comp) { return val.IsA<Type2>() && val.As<Type2>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type3 comp) { return val.IsA<Type3>() && val.As<Type3>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type4 comp) { return val.IsA<Type4>() && val.As<Type4>() == comp; }
		public static bool operator ==(OneOf<Type1, Type2, Type3, Type4, Type5> val, Type5 comp) { return val.IsA<Type5>() && val.As<Type5>() == comp; }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override bool CanBeA<T>() { return typeof(Type1) == typeof(T) || typeof(Type2) == typeof(T) || typeof(Type3) == typeof(T) 
				|| typeof(Type4) == typeof(T) || typeof(Type5) == typeof(T); }

		public override bool IsA<T>() { return IsA(typeof(T)); }
		public override bool IsA(Type type)
		{
			if (m_valType1 != null) { return typeof(Type1).BDIsOfType(type); }
			if (m_valType2 != null) { return typeof(Type2).BDIsOfType(type); }
			if (m_valType3 != null) { return typeof(Type3).BDIsOfType(type); }
			if (m_valType4 != null) { return typeof(Type4).BDIsOfType(type); }
			if (m_valType5 != null) { return typeof(Type5).BDIsOfType(type); }
			return false;
		}

		public override RetType As<RetType>()
		{
			if (IsA<Type1>() && typeof(Type1).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType1, typeof(RetType));
			if (IsA<Type2>() && typeof(Type2).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType2, typeof(RetType));
			if (IsA<Type3>() && typeof(Type3).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType3, typeof(RetType));
			if (IsA<Type4>() && typeof(Type4).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType4, typeof(RetType));
			if (IsA<Type5>() && typeof(Type5).BDIsOfType(typeof(RetType))) return (RetType)Convert.ChangeType(m_valType5, typeof(RetType));
			return null;
		}

		public override string ToString()
		{
			if (IsA<Type1>()) { return typeof(Type1).ToString() + ": " + As<Type1>().ToString(); }
			if (IsA<Type2>()) { return typeof(Type2).ToString() + ": " + As<Type2>().ToString(); }
			if (IsA<Type3>()) { return typeof(Type3).ToString() + ": " + As<Type3>().ToString(); }
			if (IsA<Type4>()) { return typeof(Type4).ToString() + ": " + As<Type4>().ToString(); }
			if (IsA<Type5>()) { return typeof(Type5).ToString() + ": " + As<Type5>().ToString(); }
			return "NO TYPES; ALL IS LOST; HAIL SATAN"; //what's an exception
		}

		public override void WriteToXml(XmlElement parent)
		{
			if (IsA<Type1>()) { parent.BDWriteAttr("Type", typeof(Type1).ToString()).BDWriteChild(As<Type1>()); }
			else if (IsA<Type2>()) { parent.BDWriteAttr("Type", typeof(Type2).ToString()).BDWriteChild(As<Type2>()); }
			else if (IsA<Type3>()) { parent.BDWriteAttr("Type", typeof(Type3).ToString()).BDWriteChild(As<Type3>()); }
			else if (IsA<Type4>()) { parent.BDWriteAttr("Type", typeof(Type4).ToString()).BDWriteChild(As<Type4>()); }
			else if (IsA<Type5>()) { parent.BDWriteAttr("Type", typeof(Type5).ToString()).BDWriteChild(As<Type5>()); }
		}

		public override void ReadFromXml(XmlElement parent)
		{
			string type = "";
			parent.BDReadAttr("Type", ref type);
			if (type == typeof(Type1).ToString()) { m_valType1 = new Type1(); m_valType1.ReadFromXml(parent); }
			else if (type == typeof(Type2).ToString()) { m_valType2 = new Type2(); m_valType2.ReadFromXml(parent); }
			else if (type == typeof(Type3).ToString()) { m_valType3 = new Type3(); m_valType3.ReadFromXml(parent); }
			else if (type == typeof(Type4).ToString()) { m_valType4 = new Type4(); m_valType4.ReadFromXml(parent); }
			else if (type == typeof(Type5).ToString()) { m_valType5 = new Type5(); m_valType5.ReadFromXml(parent); }
			else UnityEngine.Debug.Assert(false);
		}

		Type1 m_valType1 = null;
		Type2 m_valType2 = null;
		Type3 m_valType3 = null;
		Type4 m_valType4 = null;
		Type5 m_valType5 = null;
	}


	//Necessary to write to/from XML and remember the type
	public class OneOfAttribute : BD.NamedAttribute
	{
		public OneOfAttribute(string name) : base(name) { }
	}
}
