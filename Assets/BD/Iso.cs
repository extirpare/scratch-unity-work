﻿namespace BD
{
	public static class Iso
	{
		public static Vector2 k_TileSize { get { return new Vector2(2f, 1f); } }
		//how far you have to go in world space to reach from the center of [x,y] to center of [x+1,y]
		public static Vector2 k_UnitDistanceX { get { return k_TileSize * 0.5f; } }
		//how far you have to go in world space to reach from the center of [x,y] to center of [x,y+1]
		public static Vector2 k_UnitDistanceY { get { return k_UnitDistanceX.SetX(-1 * k_UnitDistanceX.X); } }

		public static IntCoord WorldToIsoIndex(Vector2 worldSpacePt)
		{
			// With credit to http://clintbellanger.net/articles/isometric_math/
			return new IntCoord(
				Maths.RoundToInt((worldSpacePt.X / k_UnitDistanceX.X + worldSpacePt.Y / k_UnitDistanceY.Y) / 2),
				Maths.RoundToInt((worldSpacePt.Y / k_UnitDistanceY.Y - worldSpacePt.X / k_UnitDistanceX.X) / 2)
			);
		}

		//Returns the center point of the tile at the given IsoIndex
		public static Vector2 IsoIndexToWorld(IntCoord isoTile)
		{
			return new Vector2(
				(isoTile.X - isoTile.Y) * k_UnitDistanceX.X,
				(isoTile.X + isoTile.Y) * k_UnitDistanceY.Y
			);
		}

		//Returns the cornerDir point of the tile at the given IsoIndex
		public static Vector2 IsoIndexToWorld(IntCoord isoTile, eDirection cornerDir)
		{
			Vector2 center = IsoIndexToWorld(isoTile);
			switch (cornerDir)
			{
				case eDirection.Down: 	return center + new Vector2(0, -0.5f * k_TileSize.Y);
				case eDirection.Left: 	return center + new Vector2(-0.5f * k_TileSize.X, 0);
				case eDirection.Up: 		return center + new Vector2(0, 0.5f * k_TileSize.Y);
				case eDirection.Right: 	return center + new Vector2(0.5f * k_TileSize.X, 0);
				default: throw new System.Exception();
			}
		}

		public static Vector2[] IsoIndexToWorldPts(IntCoord isoTile)
		{
			return new Vector2[]
			{
				IsoIndexToWorld(isoTile, eDirection.Left),
				IsoIndexToWorld(isoTile, eDirection.Down),
				IsoIndexToWorld(isoTile, eDirection.Right),
				IsoIndexToWorld(isoTile, eDirection.Up)
			};
		}

	}
}