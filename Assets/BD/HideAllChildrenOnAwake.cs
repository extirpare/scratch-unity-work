﻿using UnityEngine;
using BD;

public class HideAllChildrenOnAwake : MonoBehaviour 
{
	void Awake() 
	{
		this.transform.BDSetActiveAllChildren(false);
	}
}
