﻿//A DirDims is dimensions along each direction, up/left/right/bottom

using System;
using System.Xml;

using UnityEngine;

namespace BD
{
	[Xmlable("DirDims")]
	[Serializable] //Unity might use this to serialize but mostly we use it to expose to inspector
	public struct DirDims : IXmlable
	{
		public float Left;
		public float Top;
		public float Right;
		public float Bottom;
		public float Up { get { return Top; } set { Top = value; } }
		public float Down { get { return Bottom; } set { Bottom = value; } }

		public DirDims(float splat) { Left = splat; Top = splat; Right = splat; Bottom = splat; }
		public DirDims(float left, float up, float right, float down) { Left = left; Top = up; Right = right; Bottom = down; }

		public static DirDims Zero { get { return new DirDims(0, 0, 0, 0); } }

		public static implicit operator UnityEngine.RectOffset(DirDims val) { return new UnityEngine.RectOffset((int)val.Left, (int)val.Right, (int)val.Top, (int)val.Bottom); }
		
		//this is the unary operator, like "x = -myPoint"
		public static DirDims operator -(DirDims val) { return -1 * val; }

		//Arithmetic...
		public static DirDims operator +(DirDims a, DirDims b) { return new DirDims(a.Left + b.Left, a.Up + b.Up, a.Right + b.Right, a.Down + b.Down); }
		public static DirDims operator -(DirDims a, DirDims b) { return new DirDims(a.Left - b.Left, a.Up - b.Up, a.Right - b.Right, a.Down - b.Down); }

		public static DirDims operator *(float val, DirDims a) { return new DirDims(a.Left * val, a.Up * val, a.Right * val, a.Down * val); }
		public static DirDims operator /(float val, DirDims a) { return new DirDims(a.Left / val, a.Up / val, a.Right / val, a.Down / val); }

		public float Height { get { return Top + Bottom; } }
		public float Width { get { return Left + Right; } }

		public Vector2 WidthHeight { get { return new Vector2(Width, Height); } }

		public override string ToString() { return "Left: " + Left.ToString() + " Top: " + Top.ToString() + " Right: " + Right.ToString() + " Bottom: " + Bottom.ToString(); }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("Left", Left).BDWriteAttr("Top", Top).BDWriteAttr("Right", Right).BDWriteAttr("Bottom", Bottom); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("Left", ref Left).BDReadAttr("Top", ref Top).BDReadAttr("Right", ref Right).BDReadAttr("Bottom", ref Bottom); }
	}
}
