﻿using System;
using System.Xml;

using UnityEngine;

//Vector2 can be not-normalized, Vector2 must be normalized
namespace BD
{
	[Xmlable("Vector2")]
	[Serializable] //Unity might use this to serialize but mostly we use it to expose to inspector
	public struct Vector2 : IXmlable
	{
		public float X;
		public float Y;
		public float Width { get { return X; } set { X = value; } }
		public float Height { get { return Y; } set { Y = value; } }

		public Vector2(float splat) { this.X = splat; this.Y = splat; }
		public Vector2(float x, float y) { this.X = x; this.Y = y; }
		public Vector2(Vector2 other) { this.X = other.X; this.Y = other.Y; }
		public Vector2(IntCoord intCoord) { this.X = intCoord.X; this.Y = intCoord.Y; }
		public static implicit operator Vector2(NVector2 v) { return new Vector2(v.X, v.Y); }
		public static implicit operator Vector2(IntCoord i) { return new Vector2(i.X, i.Y); }
		public static implicit operator Vector2(UnityEngine.Vector2 v) { return new Vector2(v.x, v.y); }
		public static implicit operator Vector2(UnityEngine.Vector3 v) { return new Vector2(v.x, v.y); }
		public static implicit operator Vector2(UnityEngine.Vector4 v) { return new Vector2(v.x, v.y); }
		public static implicit operator UnityEngine.Vector2(Vector2 v) { return new UnityEngine.Vector2(v.X, v.Y); }
		public static implicit operator UnityEngine.Vector3(Vector2 v) { return new UnityEngine.Vector3(v.X, v.Y, 0); }
		public static implicit operator UnityEngine.Vector4(Vector2 v) { return new UnityEngine.Vector4(v.X, v.Y, 0, 1); }
		public static implicit operator UnityEngine.Rect(Vector2 v) { return new UnityEngine.Rect(0, 0, v.X, v.Y); }
		public static implicit operator NVector2(Vector2 v) { return new NVector2(v.X, v.Y); }

		public static Vector2 Zero { get { return new Vector2(0, 0); } }
		public static Vector2 One { get { return new Vector2(1, 1); } }
		public static Vector2 None { get { return new Vector2(-1, -1); } }
		public static Vector2 Right { get { return new Vector2(1, 0); } }
		public static Vector2 Left { get { return new Vector2(-1, 0); } }
		public static Vector2 Up { get { return new Vector2(0, 1); } }
		public static Vector2 Down { get { return new Vector2(0, -1); } }
		
		public static Vector2 Splat(float val) { return new Vector2(val, val); }

		public static Vector2 InDirection(eDirection dir)
		{
			switch (dir)
			{
				case eDirection.Right: return Vector2.Right;
				case eDirection.Up: return Vector2.Up;
				case eDirection.Left: return Vector2.Left;
				case eDirection.Down: return Vector2.Down;
				default: throw new Exception();
			}
		}

		//this is the unary operator, like "x = -myPoint"
		public static Vector2 operator -(Vector2 val) { return new Vector2(-val.X, -val.Y); }

		//Arithmetic...
		public static Vector2 operator +(Vector2 a, Vector2 b) { return new Vector2(a.X + b.X, a.Y + b.Y); }
		public static Vector2 operator -(Vector2 a, Vector2 b) { return new Vector2(a.X - b.X, a.Y - b.Y); }
		public static Vector2 operator *(Vector2 a, Vector2 b) { return new Vector2(a.X * b.X, a.Y * b.Y); }
		public static Vector2 operator /(Vector2 a, Vector2 b) { return new Vector2(a.X / b.X, a.Y / b.Y); }
		public static Vector2 operator +(Vector2 a, UnityEngine.Vector2 b) { return new Vector2(a.X + b.x, a.Y + b.y); }
		public static Vector2 operator -(Vector2 a, UnityEngine.Vector2 b) { return new Vector2(a.X - b.x, a.Y - b.y); }
		public static Vector2 operator *(Vector2 a, UnityEngine.Vector2 b) { return new Vector2(a.X * b.x, a.Y * b.y); }
		public static Vector2 operator /(Vector2 a, UnityEngine.Vector2 b) { return new Vector2(a.X / b.x, a.Y / b.y); }
		public static Vector2 operator +(UnityEngine.Vector2 a, Vector2 b) { return new Vector2(a.x + b.X, a.y + b.Y); }
		public static Vector2 operator -(UnityEngine.Vector2 a, Vector2 b) { return new Vector2(a.x - b.X, a.y - b.Y); }
		public static Vector2 operator *(UnityEngine.Vector2 a, Vector2 b) { return new Vector2(a.x * b.X, a.y * b.Y); }
		public static Vector2 operator /(UnityEngine.Vector2 a, Vector2 b) { return new Vector2(a.x / b.X, a.y / b.Y); }

		public static Vector2 operator *(float val, Vector2 a) { return new Vector2(a.X * val, a.Y * val); }
		public static Vector2 operator /(float val, Vector2 a) { return new Vector2(val / a.X, val / a.Y); }
		public static Vector2 operator *(Vector2 a, float val) { return new Vector2(a.X * val, a.Y * val); }
		public static Vector2 operator /(Vector2 a, float val) { return new Vector2(a.X / val, a.Y / val); }

		//We don't need to overload assignemnt operators like *=, they automatically pick up the overloaded *, that is cool C#

		public static bool operator !=(Vector2 lhs, Vector2 rhs) { return !(lhs == rhs); }
		public static bool operator ==(Vector2 lhs, Vector2 rhs) 
		{ 
			if (object.ReferenceEquals(lhs, null) || object.ReferenceEquals(rhs, null)) return object.ReferenceEquals(lhs, rhs); //stupid copy/paste for == null checks 
			return lhs.NearEqual(rhs); 
		}

		public bool NearEqual(Vector2 other) { return Maths.NearEqual(X, other.X) && Maths.NearEqual(Y, other.Y); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString() { return "<<" + X + ", " + Y + ">>"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref X).BDReadAttr("Y", ref Y); }

		public Vector2 SetX(float x) { Vector2 ret = this; ret.X = x; return ret; }
		public Vector2 SetY(float y) { Vector2 ret = this; ret.Y = y; return ret; }

		public float Length { get { return (float)Math.Sqrt(X * X + Y * Y); } }

		public bool IsNormalized()
		{
			return Maths.NearEqual(Length, 1f);
		}

		public void Normalize()
		{
			float length = Length;
			this.X /= length;
			this.Y /= length;
		}
		public NVector2 Normalized { get { return Length < Maths.Epsilon ? NVector2.Zero : (NVector2)(this / Length); } }

		public float DistanceAlong(NVector2 axis) { return Dot(axis); }
		public float Dot(Vector2 other) { return this.X * other.X + this.Y * other.Y; }

		//You: "2D cross products don't exist! And isn't this the determinant of a 2x2 matrix?? And cross products are supposed to be vectors??"
		//This treats the two points as 3D vectors with Z = 0 and returns that cross product.
		//Since cross product is perpendicular, and you're guaranteed to be perpendicular to the Z=0 plane, you'll get a line along [0,0,1] always.
		//So the only interesting thing is the magnitude of that line (== ||a|| ||b|| sin(theta)), which this returns.
		public float Cross(Vector2 other) { return this.X * other.Y - this.Y * other.X; }

		public bool LiesOn(NVector2 axis) { return Maths.NearEqual(this.Dot(axis), Length); }
		public bool IsPerpendicularTo(NVector2 axis) { Debug.Assert(Length > 0); return this.Dot(axis) == 0f; }

		public static Vector2 Lerp(Vector2 zeroPos, Vector2 onePos, float dist)
		{
			return onePos*dist + zeroPos*(1-dist);
		}

		//returns the vector perpendicular counterclockwise by 90 degrees
		public NVector2 GetPerpendicularNVector() { return Normalized.GetPerpendicularVector(); }

		public Vector2 Round() { return RoundToNearest(1f); }
		public Vector2 RoundToNearest(float val)
		{
			return new Vector2(Maths.RoundToNearest(X, val), Maths.RoundToNearest(Y, val));
		}
		public Vector2 RoundToNearest(float val, Maths.eRoundType roundType)
		{
			return new Vector2(Maths.RoundToNearest(X, val, roundType), Maths.RoundToNearest(Y, val, roundType));
		}

		public Vector2 AtLeast(Vector2 min) { return new Vector2(Math.Max(X, min.X), Math.Max(Y, min.Y)); }
		public Vector2 AtMost(Vector2 max) { return new Vector2(Math.Min(X, max.X), Math.Min(Y, max.Y)); }

		public Vector2 MoveLeft(float val) { return MoveRight(-val); }
		public Vector2 MoveRight(float val) { return new Vector2(X + val, Y); }
		public Vector2 MoveUp(float val) { return MoveDown(-val); }
		public Vector2 MoveDown(float val) { return new Vector2(X, Y + val); }

		public Vector2 NegateX() { return new Vector2(-X, Y); }
		public Vector2 NegateY() { return new Vector2(X, -Y); }
		public Vector2 Negate() { return new Vector2(-X, -Y); }

		public Vector2 PointUpRight() { return new Vector2(Mathf.Abs(X), -Mathf.Abs(Y)); }
		public Vector2 PointUpLeft() { return new Vector2(-Mathf.Abs(X), -Mathf.Abs(Y)); }
		public Vector2 PointDownRight() { return new Vector2(Mathf.Abs(X), Mathf.Abs(Y)); }
		public Vector2 PointDownLeft() { return new Vector2(-Mathf.Abs(X), Mathf.Abs(Y)); }

		public Vector2 FitInside(Rect rect)
		{
			return new Vector2( BD.Maths.Clamp(X, rect.Left, rect.Right), BD.Maths.Clamp(Y, rect.Top, rect.Bottom) );
		}

		public Rect ExpandToRect(DirDims dirDims)
		{
			return new Rect(X - dirDims.Left, Y - dirDims.Top, dirDims.Width, dirDims.Height);
		}

		public Vector2 Scale(float val)
		{
			return new Vector2(val * this.X, val * this.Y);
		}

		public Vector2 Scale(Vector2 val)
		{
			return new Vector2(val.X * this.X, val.Y * this.Y);
		}

        //rotates counter-clockwise
        public Vector2 Rotate(float degrees)
        {
            float cos = (float)Math.Cos(degrees * Maths.DegreesToRadians);
            float sin = (float)Math.Sin(degrees * Maths.DegreesToRadians);

            return new Vector2(X*cos - Y*sin, X*sin + Y*cos);
        }

		public float DistanceTo(BD.Vector2 other)
		{
			return (other - this).Length;
		}

		//Pair.First is the longer Direction
		//Pair.Second will be null if we're exactly in one direction (i.e. [3,0] => [right, null])
		//Pair.First will be null if we're [0,0]
		public BD.Pair<eDirection?, eDirection?> CalculateDirections()
		{
			BD.Pair<eDirection?, eDirection?> ret = new BD.Pair<eDirection?, eDirection?>(null, null);

			if (Math.Abs(this.X) > Math.Abs(this.Y))
			{
				ret.First = (this.X > 0) ? eDirection.Right : eDirection.Left;
				if (this.Y > 0) ret.Second = eDirection.Up; else if (this.Y < 0) ret.Second = eDirection.Down;
			}
			else if (Math.Abs(this.Y) > Math.Abs(this.X))
			{
				ret.First = (this.Y > 0) ? eDirection.Up : eDirection.Down;
				if (this.X > 0) ret.Second = eDirection.Right; else if (this.X < 0) ret.Second = eDirection.Left;
			}
			else if (this.X != 0) //same absolute value
			{
				ret.First = (this.X > 0) ? eDirection.Right : eDirection.Left;
				ret.Second = (this.Y > 0) ? eDirection.Up : eDirection.Down;
			}

			return ret;
		}

		//This is much simpler than CalculateDirections()
		public eDirection GetDirection()
		{
			if (Math.Abs(Y) > Math.Abs(X))
				return Y > 0 ? eDirection.Up : eDirection.Down;
			else
				return X > 0 ? eDirection.Right : eDirection.Left;
		}

		public void Invert() { X = 1 / X; Y = 1 / Y; }
		public Vector2 Inverse { get { Vector2 ret = new Vector2(this); ret.Invert(); return ret; } }
	}
}
