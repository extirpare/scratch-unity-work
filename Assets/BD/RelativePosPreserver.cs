﻿using BD;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

//Use this if you don't want an object to be your parent, but you do want your position to act like it is

public class RelativePosPreserver : SerializedMonoBehaviour 
{
	public Transform ParentTransform;
	public FlagsOf<eAxis> AxesToMatch = new FlagsOf<eAxis>();

	[NonSerialized, ShowInInspector, ReadOnly] BD.Vector3 Offset;

	void Start()
	{
		Offset = this.transform.position - ParentTransform.position;
	}

	void LateUpdate()
	{
		BD.Vector3 pos = this.transform.position;
		if (AxesToMatch.Test(eAxis.X)) pos.X = ParentTransform.position.x + Offset.X;
		if (AxesToMatch.Test(eAxis.Y)) pos.Y = ParentTransform.position.y + Offset.Y;
		if (AxesToMatch.Test(eAxis.Z)) pos.Z = ParentTransform.position.z + Offset.Z;
		this.transform.position = pos;
	}
}
