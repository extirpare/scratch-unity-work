﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

//A Grid2D is like a 2D array, but it's resizable and built on lists
namespace BD
{
	[Serializable]
	public class Grid2D<T>
	{
		[UnityEngine.HideInInspector] T[,] m_data;
		[ShowInInspector] public BD.IntCoord Dimensions { get { return m_data != null ? new BD.IntCoord(m_data.GetLength(1), m_data.GetLength(0)) : BD.IntCoord.Zero; } }

		//If you need to create cells in the grid in a specific way, set these to your creation function. Otherwise we'll use default(T).
		[NonSerialized] public Func<IntCoord, T> OverrideCellCreationFunc = null;

		public Grid2D() { SetDims(new BD.IntCoord(1, 1)); }
		public Grid2D(int width, int height) { SetDims(new BD.IntCoord(width, height)); }
		public Grid2D(IntCoord dims) { SetDims(dims); }

		public T this[BD.IntCoord loc]
		{
			get { return m_data[loc.Y, loc.X]; }
			set { m_data[loc.Y, loc.X] = value; }
		}

		public T this[int y, int x]
		{
			get { return m_data[y, x]; }
			set { m_data[y, x] = value; }
		}

		public T[] this[int y]
		{
			get
			{
				T[] ret = new T[m_data.GetLength(1)];
				for (int i = 0; i < ret.Length; ++i)
					ret[i] = this[y,i];
				return ret;
			}
			set
			{
				UnityEngine.Debug.Assert(value.Length == m_data.GetLength(1));
				for (int i = 0; i < value.Length; ++i)
					this[y,i] = value[i];
			}
		}

		public void SetDims(BD.IntCoord newDims)
		{
			if (m_data != null && newDims == Dimensions)
				return;

			m_data = new T[newDims.Y, newDims.X];

			if (OverrideCellCreationFunc != null)
			{
				for (int y = 0; y < Dimensions.Y; ++y)
					for (int x = 0; x < Dimensions.X; ++x)
						this[y,x] = OverrideCellCreationFunc(new BD.IntCoord(x, y));
			}
		}

		//midpoint of a 3x5 grid is [1, 2].
		//midpoint of a 2x4 grid is [1, 2].
		public IntCoord MidPoint { get { return Dimensions / 2; } }

		public override string ToString()
		{
			string ret = Dimensions.ToString();
			return ret; 
		}

		public void Clear()
		{
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
				this[index] = default(T);			
		}

		public void Fill(T fillVal)
		{
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
				this[index] = fillVal;			
		}

		public void Fill(List<IntCoord> toFill, T fillVal)
		{
			foreach (IntCoord index in toFill)
				this[index] = fillVal;			
		}

		public void CopyFrom(Grid2D<T> other)
		{
			UnityEngine.Debug.Assert(other.Dimensions == this.Dimensions);
			
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
				this[index] = other[index];
		}

		//returns true if touches at least one index
		public bool ActOnIndicesWhere(Func<IntCoord, T, bool> predicate, Action<IntCoord, T> action)
		{
			bool ret = false;

			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
			{
				if (predicate(index, this[index]))
				{
					action(index, this[index]);
					ret = true;
				}
			}

			return ret;
		}

		//returns true if touches at least one index
		public bool ActOnIndicesWhere(Func<T, bool> predicate, Action<T> action)
		{
			bool ret = false;
			
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
			{
				if (predicate(this[index]))
				{
					action(this[index]);
					ret = true;
				}
			}

			return ret;
		}

		public bool Contains(Func<IntCoord, T, bool> predicate)
		{
			foreach (IntCoord index in new IntCoord.Enumerable(Dimensions))
				if (predicate(index, this[index]))
					return true;
			return false;
		}

		public List<IntCoord> FindIndices(Func<IntCoord, T, bool> predicate)
		{
			List<IntCoord> ret = new List<IntCoord>();
			ActOnIndicesWhere(predicate, (index, val) => ret.Add(index));
			return ret;
		}

		public List<IntCoord> FindIndices(Func<T, bool> predicate)
		{
			List<IntCoord> ret = new List<IntCoord>();
			ActOnIndicesWhere((index, val) => predicate(val), (index, val) => ret.Add(index));
			return ret;
		}

		public BoolGrid FindIndicesAsGrid(Func<IntCoord, T, bool> predicate)
		{
			BoolGrid ret = new BoolGrid(this.Dimensions);
			ret.Fill(false);
			ActOnIndicesWhere(predicate, (index, val) => ret[index] = true);
			return ret;
		}

		public BoolGrid FindIndicesAsGrid(Func<T, bool> predicate)
		{
			BoolGrid ret = new BoolGrid(this.Dimensions);
			ret.Fill(false);
			ActOnIndicesWhere((index, val) => predicate(val), (index, val) => ret[index] = true);
			return ret;
		}
	}
}
