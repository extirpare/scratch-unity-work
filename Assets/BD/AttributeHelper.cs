﻿using System;
using System.Reflection;

namespace BD
{
	public static class AttributeHelper
	{
		public static string BDDump(this MemberInfo member)
		{
			string str = member.Name + " attributes:\n";
			foreach (object attribute in member.GetCustomAttributes(true))
			{
				str += attribute.ToString() + "\n";
			}
			return str;
		}

		public static T BDTryAndGetAttr<T>(this Type type) where T : Attribute
		{
			MemberInfo member = (MemberInfo)type;
			foreach (object attribute in member.GetCustomAttributes(true))
			{
				if (attribute.GetType() == typeof(T))
				{
					return (T)attribute;
				}
			}
			return null;
		}

		public static T BDGetAttr<T>(this Type type) where T : Attribute
		{
			T ret = type.BDTryAndGetAttr<T>();
			if (ret == null)
				throw new NullReferenceException();
			return ret;
		}

		public static Type TryAndGetTypeWithDescribedAttribute<T>(Func<T, bool> predicate) 
			where T : Attribute
		{
			AttributeDictionary<T> dict = AttributeDictionary<T>.Get();
			foreach (var value in dict.Values)
			{
				if (predicate(value.Key))
					return value.Value;
			}
			return null;
		}

		public static Type TryAndGetTypeWithNamedAttribute<AttributeType>(string typeName)
			where AttributeType : NamedAttribute
		{
			Type ret = BD.AttributeHelper.TryAndGetTypeWithDescribedAttribute<AttributeType>(o => o.Name == typeName);
			return ret;
		}
	}

}
