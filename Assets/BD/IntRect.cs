﻿using System;
using System.Xml;

//
// A rect for IntCoords.
// Note that a 1x1 IntRect at 0,0 contains ONLY 0,0 -- it doesn't contain 1,1 or 1,0 or 0,1
//

namespace BD
{
	[Xmlable("IntRect")]
	[Serializable] //Unity might use this to serialize but mostly we use it to expose to inspector
	public struct IntRect : IXmlable 
	{
		public int X;
		public int Y;
		public int Width;
		public int Height;

		public int Left { get { return X; } set { X = value; } }
		public int Right { get { return X + Width; } set { Width = value - X; } }
		public int Top { get { return Y; } set { Y = value; } }
		public int Bottom { get { return Y + Height; } set { Height = value - Y; } }
		public int Length { get { return Width; } set { Width = value; } }

		public IntCoord XY { get { return TopLeft; } set { TopLeft = value; } }
		public IntCoord WidthHeight { get { return Size; } set { Size = value; } }

		public IntCoord TopLeft { get { return new IntCoord(Left, Top); } set { Left = value.X; Top = value.Y; } }
		public IntCoord TopRight { get { return new IntCoord(Right, Top); } set { Right = value.X; Top = value.Y; } }
		public IntCoord BottomLeft { get { return new IntCoord(Left, Bottom); } set { Left = value.X; Bottom = value.Y; } }
		public IntCoord BottomRight { get { return new IntCoord(Right, Bottom); } set { Right = value.X; Bottom = value.Y; } }
		public IntCoord Size { get { return new IntCoord(Width, Height); } set { Width = value.X; Height = value.Y; } }

		public IntRect(IntRect other) { X = other.X; Y = other.Y; Width = other.Width; Height = other.Height; }
		public IntRect(int width, int height)
		{
			this.X = 0;
			this.Y = 0;
			this.Width = width;
			this.Height = height;
		}

		public IntRect(int x, int y, int width, int height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		public IntRect(IntCoord size)
		{
			this.X = 0;
			this.Y = 0;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public IntRect(IntCoord topLeft, IntCoord size)
		{
			this.X = topLeft.X;
			this.Y = topLeft.Y;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public IntRect(IntCoord topLeft, int width, int height)
		{
			this.X = topLeft.X;
			this.Y = topLeft.Y;
			this.Width = width;
			this.Height = height;
		}

		public IntRect(int x, int y, IntCoord size)
		{
			this.X = x;
			this.Y = y;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		public static bool operator ==(IntRect lhs, IntRect rhs)
		{
			return lhs.X == rhs.X
				&& lhs.Y == rhs.Y
				&& lhs.Width == rhs.Width
				&& lhs.Height == rhs.Height;
		}
		public static bool operator !=(IntRect lhs, IntRect rhs) { return !(lhs == rhs); }

		//These silence the compiler about CS0660 / CS0661
		public override bool Equals(object o) { return base.Equals(o); }
		public override int GetHashCode() { return base.GetHashCode(); }

		public override string ToString() { return "[" + Left + ", " + Top + "] size: [" + Width + ", " + Height + "]"; }
		public void WriteToXml(XmlElement parent) { parent.BDWriteAttr("X", X).BDWriteAttr("Y", Y).BDWriteAttr("Width", Width).BDWriteAttr("Height", Height); }
		public void ReadFromXml(XmlElement parent) { parent.BDReadAttr("X", ref X).BDReadAttr("Y", ref Y).BDReadAttr("Width", ref Width).BDReadAttr("Height", ref Height); }

		public bool Contains(IntCoord point)
		{
			return point.X >= Left
				&& point.Y >= Top
				&& point.X < Right
				&& point.Y < Bottom;
		}
		
		public bool FitsWithin(IntCoord dims) { return FitsWithin(new IntRect(dims)); }
		public bool FitsWithin(IntRect parentRect)
		{
			if (Size == IntCoord.Zero)
				return parentRect.Contains(TopLeft);
	
			//ugh "down" means "-1" here but should mean "+1"
			return parentRect.Contains(TopLeft) 
				&& parentRect.Contains(TopRight.MovedInDirection(eDirection.Left))
				&& parentRect.Contains(BottomLeft.MovedInDirection(eDirection.Down));
		}
		
		enum eRelativeLoc
		{
			SmallerNumber,
			InsideBounds,
			BiggerNumber
		}

		public IntCoord ClosestPoint(IntCoord pt)
		{
			eRelativeLoc horizLoc, vertLoc;
			if (pt.X < this.Left) horizLoc = eRelativeLoc.SmallerNumber; 
			else if (pt.X > this.Right) horizLoc = eRelativeLoc.BiggerNumber; 
			else horizLoc = eRelativeLoc.InsideBounds;

			if (pt.Y < this.Top) vertLoc = eRelativeLoc.SmallerNumber; 
			else if (pt.Y > this.Bottom) vertLoc = eRelativeLoc.BiggerNumber; 
			else vertLoc = eRelativeLoc.InsideBounds;

			switch (horizLoc)
			{
				case eRelativeLoc.SmallerNumber: //to the left
					switch (vertLoc)
					{
						case eRelativeLoc.SmallerNumber:
							return this.TopLeft;
						case eRelativeLoc.InsideBounds:
							return new IntCoord(this.Left, pt.Y);
						case eRelativeLoc.BiggerNumber:
							return this.BottomLeft;
					}
					throw new Exception();
				case eRelativeLoc.InsideBounds:
					switch (vertLoc)
					{
						case eRelativeLoc.SmallerNumber:
							return new IntCoord(pt.X, this.Top);
						case eRelativeLoc.InsideBounds:
							return pt; //it's inside us lol
						case eRelativeLoc.BiggerNumber:
							return new IntCoord(pt.X, this.Bottom);
					}
					throw new Exception();
				case eRelativeLoc.BiggerNumber: //to the right
					switch (vertLoc)
					{
						case eRelativeLoc.SmallerNumber:
							return this.TopRight;
						case eRelativeLoc.InsideBounds:
							return new IntCoord(this.Right, pt.Y);
						case eRelativeLoc.BiggerNumber:
							return this.BottomRight;
					}
					throw new Exception();
				default:
					throw new Exception();
			}
		}
	}
}