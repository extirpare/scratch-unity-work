﻿using UnityEngine;

public class DeleteOnAwake : MonoBehaviour 
{
	void Awake()
	{
		GameObject.DestroyImmediate(this.gameObject);
	}
}
