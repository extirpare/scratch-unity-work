﻿using UnityEngine;
using System.Collections;

//Rotates back and forth using a sine pattern

public class SinRotation : MonoBehaviour 
{
	public float Speed;
	public float MaxAngleDeg;
	
	void Update()
	{
		float val = Mathf.Sin(Speed * Time.time);
		float degreesYaw = val * MaxAngleDeg;
		this.transform.rotation = Quaternion.Euler(0, 0, degreesYaw);
	}
}
