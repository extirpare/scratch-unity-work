﻿using System;
using UnityEngine;

// Lets you nicely define a min value and max value whose limits are [0,1]

namespace BD
{
	[Serializable]
	public class MinMaxSlider 
	{
		public float Min = 0.4f;
		public float Max = 0.6f;
		public float OverrideVal { set { Min = value; Max = value; } }
	}
}
